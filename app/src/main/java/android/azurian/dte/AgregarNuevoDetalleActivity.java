package android.azurian.dte;

import android.azurian.dte.Emision.SeleccionarDetalleListaActivity;
import android.azurian.dte.services.Dconfig;
import android.azurian.dte.services.Detalle;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;


public class AgregarNuevoDetalleActivity extends AppCompatActivity {

    Button btnFinalizarAddDetalle;
    EditText inTxtDetalleDesc;
    EditText inTxtDetalleCod;
    Spinner selectTipoIGV;
    EditText inTxtDetalleVU;
    String selectedIVG;
    Spinner listaTipoUnidad;

    String tipoUnidadSelected;

    final String TAG = getClass().getSimpleName();

    Button btnReadBC;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_nuevo_detalle);



        btnFinalizarAddDetalle      = findViewById(R.id.btnFinalizarAddDetalle);
        inTxtDetalleDesc            = findViewById(R.id.inTxtDetalleDesc);
        inTxtDetalleCod             = findViewById(R.id.inTxtDetalleCod);
        selectTipoIGV               = findViewById(R.id.selectTipoIGV);
        inTxtDetalleVU              = findViewById(R.id.inTxtDetalleVU);

        listaTipoUnidad             = findViewById(R.id.listaTipoUnidad);

        btnReadBC                   = findViewById(R.id.btnReadBC);



        btnReadBC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new IntentIntegrator(AgregarNuevoDetalleActivity.this).initiateScan();
            }
        });


        listaTipoUnidad.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String tipoUnidad[] = getResources().getStringArray(R.array.unidades);

                String tipo = tipoUnidad[position];

                Log.i(TAG,"TU: " +tipo );
                tipoUnidadSelected = tipo;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        final String[] tiposIVG = {"Gravado - Operación Onerosa"};

        selectTipoIGV.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,tiposIVG));
        selectTipoIGV.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedIVG = tiposIVG[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnFinalizarAddDetalle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG,"EOP");


                //Validaciones

                try{



                    AlertDialog.Builder consultaQty = new android.support.v7.app.AlertDialog.Builder(AgregarNuevoDetalleActivity.this);
                    consultaQty.setTitle("Cantidad");
                    consultaQty.setMessage("Ingrese Cantidad");
                    final EditText input = new EditText(getApplicationContext());
                    input.setInputType(InputType.TYPE_CLASS_NUMBER);
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT);
                    input.setLayoutParams(lp);
                    consultaQty.setView(input);


                    consultaQty.setPositiveButton("Listo", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            String cantidad = input.getText().toString();

                            try{

                                Intent intent = new Intent();
                                Detalle detalle = new Detalle();

                                detalle.descripcion = inTxtDetalleDesc.getText().toString();
                                detalle.codigo = inTxtDetalleCod.getText().toString();
                                detalle.tipoIGV = selectedIVG;
                                detalle.valorUnitario = Double.parseDouble(inTxtDetalleVU.getText().toString());// Long.parseLong(inTxtDetalleVU.getText().toString());

                                detalle.tipoUnidad = tipoUnidadSelected;

                                FirebaseDatabase database = FirebaseDatabase.getInstance();
                                final DatabaseReference myRef = database.getReference(Dconfig.ruc+"/items");

                                myRef.push().setValue(detalle);

                                detalle.cantidad = 1;

                                int qty = Integer.parseInt(cantidad);


                                detalle.cantidad = qty;

                                detalle.valorSubTotal = detalle.cantidad * detalle.valorUnitario;

                                intent.putExtra("item",detalle);
                                intent.putExtra("Eep","eep");


                                setResult(101,intent);
                                finish();

                            }catch (Exception e){
                                Log.e(TAG,"cantidad to qty "+e.toString());
                            /*
                            Intent intent = new Intent();
                            setResult(201,intent);
                            finish();
                            */
                                Toast.makeText(getApplicationContext(),"Debe ser numero entero",Toast.LENGTH_SHORT);
                            }
                        }
                    });

                    consultaQty.show();





                }catch (Exception e){
                    Log.e(TAG,e.toString());
                }

            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        Log.i(TAG,"REQC: "+requestCode +" RSLTC: "+resultCode);
        if (requestCode == 49374 && resultCode == -1){
            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            inTxtDetalleCod.setText(result.getContents());
        }
    }
}
