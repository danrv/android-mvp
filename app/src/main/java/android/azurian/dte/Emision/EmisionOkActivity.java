package android.azurian.dte.Emision;

import android.annotation.SuppressLint;
import android.azurian.dte.BlePrinter.BluetoothService;
import android.azurian.dte.BlePrinter.ImpConfig;
import android.azurian.dte.BlePrinter.PrinterService;
import android.azurian.dte.Consulta.WebViewActivity;
import android.azurian.dte.R;
import android.azurian.dte.services.Dconfig;
import android.azurian.dte.services.EmisionBoletaService;
import android.azurian.dte.services.ObtenerInfoDoc;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.azurian.dte.sdk.*;

import org.json.JSONException;
import org.json.JSONObject;

public class EmisionOkActivity extends AppCompatActivity {

    /******************************************************************************************************/
    // Debugging

    private static final boolean DEBUG = true;

    private BluetoothService mService = null;

    final String TAG = getClass().getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_factura_emision_ok);


        String b64Data  = (String)getIntent().getStringExtra("b64Data");
        final String tipo = (String)getIntent().getStringExtra("tipo");
        Log.i(TAG,"TPO~> "+tipo);

        final EmisionBoletaService ebs = new EmisionBoletaService();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<?xml version=\"1.0\"?>");
        stringBuilder.append("<EnvioLoteWebService idEmpresa=\"");
        stringBuilder.append(Dconfig.ruc);
        stringBuilder.append("\">");
        stringBuilder.append("<![CDATA[");
        stringBuilder.append(b64Data);
        stringBuilder.append("]]>");
        stringBuilder.append("</EnvioLoteWebService>");

        final JSONObject object = new JSONObject();
        try {
            object.put("xml",stringBuilder.toString());

            Thread th = new Thread(new Runnable() {
                @Override
                public void run() {

                    try{

                        JSONObject object1 = ebs.emitir(object);
                        String resp = object1.getString("respuesta");
                        Log.i(TAG,"RESP--> "+resp);
                        final String resps[] = resp.split("\\|");

                        if(resp.startsWith("ERROR")){
                            Log.w(TAG,"Error al generar documento "+resps[1]);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    AlertDialog.Builder showErr = new android.support.v7.app.AlertDialog.Builder(EmisionOkActivity.this, R.style.Theme_AppCompat_Dialog_Alert);
                                    showErr.setTitle("Error");
                                    showErr.setMessage(resps[1]);
                                    showErr.setPositiveButton("Volver", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                            finish();

                                        }
                                    });

                                    showErr.show();
                                }
                            });




                        }

                        if (resps.length>3){
                            Log.i(TAG,"RUC EMIT "+resps[2]);
                            Log.i(TAG,"DOC GEN "+resps[3]);
                            String docGen[] = resps[3].split("-");
                            String serieGen = docGen[0];
                            String folioGen = docGen[1];

                            Log.i(TAG,"Serie: "+serieGen);
                            Log.i(TAG,"Folio: "+folioGen);


                            String tipoDoc = "01";
                            if (tipo.equalsIgnoreCase("BOLETA")){
                                tipoDoc = "03";
                            }
                            if(tipo.equalsIgnoreCase("NOTAD_BOLETA")){
                                tipoDoc = "08";
                            }
                            if(tipo.equalsIgnoreCase("NOTAD_FACTURA")){
                                tipoDoc = "08";
                            }

                            if(tipo.equalsIgnoreCase("NOTAC_BOLETA")){
                                tipoDoc = "07";
                            }
                            if(tipo.equalsIgnoreCase("NOTAC_FACTURA")){
                                tipoDoc = "07";
                            }

                            JSONObject consulta = new JSONObject();
                            consulta.put("idEmpresa",resps[2]);
                            consulta.put("numero",folioGen);
                            consulta.put("serie",serieGen);
                            consulta.put("tipoDocumento",tipoDoc);

                            Log.i(TAG,"Consultar por Documento "+consulta.toString());

                            /*
                            ObtenerInfoDoc oid = new ObtenerInfoDoc();
                            oid.consultar(consulta);
                            */
                            String pdfUrl = resps[4];

                            Intent intent = new Intent(EmisionOkActivity.this,WebViewActivity.class);
                            intent.putExtra("url",pdfUrl);
                            intent.putExtra("json",consulta.toString());
                            intent.putExtra("codigo",serieGen+"-"+folioGen);
                            intent.putExtra("tipo",tipo);

                            startActivity(intent);
                            Intent resok = new Intent();
                            setResult(204);
                            finish();
                        }

                    }catch (Exception e){
                        Log.i(TAG,"ERROR: "+e.toString());
                    }

                }
            });
            th.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

        PrinterService service = PrinterService.getInstance(getApplicationContext());
            if (service.isADeviceConnected == false){
                service.initService();

            }

        mService = service.mService;
        Log.i(TAG,"SERV-OK " +mService );



        Button btnImprimir = findViewById(R.id.btnImprimir);
        btnImprimir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.i(TAG,"PRINT");
                try{









                }catch (Exception e){

                    Log.e(TAG,e.toString());
                }


            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();

    }




}
