package android.azurian.dte.Emision;

import android.azurian.dte.R;
import android.azurian.dte.services.NormalTransaction;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ObservacionesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ObservacionesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ObservacionesFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "transaction";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private NormalTransaction mTransaction;

    private OnFragmentInteractionListener mListener;

    public ObservacionesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ObservacionesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ObservacionesFragment newInstance(String param1, String param2, NormalTransaction transaction) {
        ObservacionesFragment fragment = new ObservacionesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putSerializable(ARG_PARAM3,transaction);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            mTransaction = (NormalTransaction) getArguments().getSerializable(ARG_PARAM3);
        }
    }

    final String TAG = getClass().getSimpleName();
    Button btnAgregarObservacion;
    EditText inTxtObservaciones;
    EditText inTxtDescuento;

    EditText inTxtSerieEN;
    EditText inCorrelativoEN;
    EditText inMotivoSusEN;

    Spinner spinnerTipoNt;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_observaciones, container, false);

        //((AppCompatActivity) getActivity()).getSupportActionBar().hide();
        Log.i(TAG," EZ-> "+mParam1);

        inTxtSerieEN            = v.findViewById(R.id.inTxtSerieEN);
        inCorrelativoEN         = v.findViewById(R.id.inCorrelativoEN);
        inMotivoSusEN           = v.findViewById(R.id.inMotivoSusEN);

        inTxtObservaciones      = v.findViewById(R.id.inTxtObservaciones);
        btnAgregarObservacion   = v.findViewById(R.id.btnAgregarObservacion);

        inTxtDescuento          = v.findViewById(R.id.inTxtDescuento);

        spinnerTipoNt           = v.findViewById(R.id.spinnerTipoNt);


        if(mParam1.equalsIgnoreCase("NOTAC_BOLETA") || mParam1.equalsIgnoreCase("NOTAC_FACTURA")){

            final String[] tiposNtStrs = {"Anulación Operación","Anulación Error en RUC","Corrección por Error en Descipción","Descuento Global","Descuento por Item","Dovolución Total","Devolución por Item","Bonificacion","Disminución en el Valor"};
            final String[] tiposNtInts = {"01","02","03","04","05","06","07","08","09"};

            String tipoIntStr = tiposNtInts[0];
            mTransaction.setCodigoNotaStr(tipoIntStr);
            mTransaction.setCodigoNotaInt(Integer.parseInt(tipoIntStr));

            spinnerTipoNt.setAdapter(new ArrayAdapter<String>(getContext(),android.R.layout.simple_spinner_item,tiposNtStrs));

            spinnerTipoNt.setSelection(mTransaction.getCodigoNotaInt(),true);
            spinnerTipoNt.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String tipoIntStr = tiposNtInts[position];
                    mTransaction.setCodigoNotaStr(tipoIntStr);
                    mTransaction.setCodigoNotaInt(Integer.parseInt(tipoIntStr));
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    String tipoIntStr = tiposNtInts[0];
                    mTransaction.setCodigoNotaStr(tipoIntStr);
                    mTransaction.setCodigoNotaInt(Integer.parseInt(tipoIntStr));

                }
            });
        }


        if(mParam1.equalsIgnoreCase("NOTAD_BOLETA") || mParam1.equalsIgnoreCase("NOTAD_FACTURA")){

            final String[] tiposNtStrs = {"Interes por Mora","Aumento en el Valor"};
            final String[] tiposNtInts = {"01","02"};

            String tipoIntStr = tiposNtInts[0];
            mTransaction.setCodigoNotaStr(tipoIntStr);
            mTransaction.setCodigoNotaInt(Integer.parseInt(tipoIntStr));

            spinnerTipoNt.setAdapter(new ArrayAdapter<String>(getContext(),android.R.layout.simple_spinner_item,tiposNtStrs));

            spinnerTipoNt.setSelection(mTransaction.getCodigoNotaInt(),true);
            spinnerTipoNt.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String tipoIntStr = tiposNtInts[position];
                    mTransaction.setCodigoNotaStr(tipoIntStr);
                    mTransaction.setCodigoNotaInt(Integer.parseInt(tipoIntStr));
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    String tipoIntStr = tiposNtInts[0];
                    mTransaction.setCodigoNotaStr(tipoIntStr);
                    mTransaction.setCodigoNotaInt(Integer.parseInt(tipoIntStr));

                }
            });
        }



        if(mParam1.startsWith("NOTA")){

            inTxtSerieEN.setVisibility(View.VISIBLE);
            inCorrelativoEN.setVisibility(View.VISIBLE);
            inMotivoSusEN.setVisibility(View.VISIBLE);
            inTxtDescuento.setVisibility(View.GONE);
            spinnerTipoNt.setVisibility(View.VISIBLE);
        }else{
            inTxtSerieEN.setVisibility(View.GONE);
            inCorrelativoEN.setVisibility(View.GONE);
            inMotivoSusEN.setVisibility(View.GONE);
            inTxtDescuento.setVisibility(View.VISIBLE);
            spinnerTipoNt.setVisibility(View.GONE);
        }



        btnAgregarObservacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    int descuento = 0;
                    if(!inTxtDescuento.getText().toString().isEmpty()){
                        descuento = Integer.parseInt(inTxtDescuento.getText().toString());
                        if(descuento>100){
                            Toast.makeText(getContext(),"Descuento debe ser porcentaje menor o igual a 100%", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        int pdescuento = Math.abs(descuento);
                        mTransaction.setDescuento(pdescuento);
                    }


                    if(mParam1.startsWith("NOTA")){

                        String serie        = inTxtSerieEN.getText().toString();
                        String correlativo  = inCorrelativoEN.getText().toString();
                        String motSustento  = inMotivoSusEN.getText().toString();

                        if(serie.isEmpty()){
                            return;
                        }
                        if (correlativo.isEmpty()){
                            return;
                        }
                        if(motSustento.isEmpty()){
                            return;
                        }
                    }
                    mTransaction.setObservaciones(inTxtObservaciones.getText().toString());
                    mTransaction.setMotivoSustento(inMotivoSusEN.getText().toString());

                    mTransaction.setModSerie(inTxtSerieEN.getText().toString());
                    mTransaction.setModFolio(inCorrelativoEN.getText().toString());


                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.fragmentHolderEmision, ReviewFragment.newInstance(mParam1,"",mTransaction));
                    transaction.addToBackStack("Review");

                    transaction.commit();

                }catch (Exception e){
                    Log.e(TAG,""+e.toString());
                    Toast.makeText(getContext(),"Descuento debe ser porcentaje menor o igual a 100%", Toast.LENGTH_SHORT).show();
                }


            }
        });
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
