package android.azurian.dte.Emision;

import android.azurian.dte.R;
import android.azurian.dte.sdk.Command;
import android.azurian.dte.sdk.PrinterCommand;
import android.azurian.dte.services.DDTEServiceSoapBinding;
import android.azurian.dte.services.DIServiceEvents;
import android.azurian.dte.services.DOperationResult;
import android.azurian.dte.services.Dconfig;
import android.azurian.dte.services.Detalle;
import android.azurian.dte.services.NormalTransaction;
import android.azurian.dte.uitls.MonedaSoles;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ReviewFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ReviewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ReviewFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "transaction";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private NormalTransaction mTransaction;

    private OnFragmentInteractionListener mListener;

    public ReviewFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ReviewFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ReviewFragment newInstance(String param1, String param2, NormalTransaction transaction) {
        ReviewFragment fragment = new ReviewFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putSerializable(ARG_PARAM3,transaction);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            mTransaction = (NormalTransaction) getArguments().getSerializable(ARG_PARAM3);
        }
    }

    DetalleReviewBaseAdapter adapter;
    ListView listaDetallesReview;
    final String TAG = getClass().getSimpleName();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_review, container, false);

        Log.i(TAG,"DOM: Vamos Llegua!");
        // EMISOR
        TextView lblRSEmisor            = v.findViewById(R.id.lblRSEmisor);
        TextView lblRUCEmisor           = v.findViewById(R.id.lblRUCEmisor);

        lblRSEmisor.setText(Dconfig.razonSocial);
        lblRUCEmisor.setText(Dconfig.ruc);

        //RECEPTOR

        TextView lblRSReceptor          = v.findViewById(R.id.lblRSReceptor);
        TextView lblRUCReceptor         = v.findViewById(R.id.lblRUCReceptor);
        TextView lblDireccionReceptor   = v.findViewById(R.id.lblDireccionReceptor);
        TextView lblEmailReceptor       = v.findViewById(R.id.lblEmailReceptor);

        lblRSReceptor.setText(mTransaction.getCliente().getRazonSocial());
        lblRUCReceptor.setText(mTransaction.getCliente().getRuc());
        lblDireccionReceptor.setText(mTransaction.getCliente().getDireccion());
        lblEmailReceptor.setText(mTransaction.getCliente().getContacto());

        //Documento

        TextView lblFechaEmision        = v.findViewById(R.id.lblFechaEmision);
        TextView lblFechaVencimiento    = v.findViewById(R.id.lblFechaVencimiento);
        TextView lblMoneda              = v.findViewById(R.id.lblMoneda);
        TextView lblSerie               = v.findViewById(R.id.lblSerie);
        TextView lblDocGratuito         = v.findViewById(R.id.lblDocGratuito);

        lblFechaEmision.setText(mTransaction.getFechaEmision());
        lblFechaVencimiento.setText(mTransaction.getFechaVencimiento());
        lblSerie.setText(mTransaction.getSerie());
        lblMoneda.setText(mTransaction.getMoneda());
        String showGrat = mTransaction.isDocumentoGratuito()? "Si: Documento Gratuito" : "No: Documento NO gratuito";
        lblDocGratuito.setText(showGrat);


        //Lista

        adapter = new DetalleReviewBaseAdapter();
        listaDetallesReview             = v.findViewById(R.id.listaDetallesReview);

        listaDetallesReview.setAdapter(adapter);
        listaDetallesReview.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        //Total

        TextView lblSubTotal            = v.findViewById(R.id.lblSubTotal);
        TextView lblIVG                 = v.findViewById(R.id.lblIVG);
        TextView lblTotal               = v.findViewById(R.id.lblTotal);

        TextView lblDescuento           = v.findViewById(R.id.lblDescuento);


        Double subtotalx = 0.0;
        for (Detalle dt : mTransaction.getListaDetalles()){
            subtotalx = subtotalx + dt.valorSubTotal;
        }

        final int descuento = mTransaction.getDescuento();

        final Double subtotal = subtotalx;
        final double desc = ((double) descuento/(double) 100) * subtotal;

        Log.i(TAG,"Desc: "+desc);
        Log.i(TAG,"PARAM 1 "+mParam1);

        DecimalFormat df = new DecimalFormat("#.00");
        //final Double igv = Double.parseDouble(df.format((subtotal - desc)* 0.18)); //subtotal * 0.19;
        final Double igv = (subtotal - desc)*0.18;//(subtotal - desc)* 0.18); //subtotal * 0.19;



        DecimalFormat df2 = new DecimalFormat("#,##0.00");
//        final Double mdesc = df.format(subtotal*(mTransaction.getDescuento()/100));

        final Double total = subtotal + igv;
        Log.i(TAG,"~  ~");
        Log.i(TAG,"a Double: " + (total-desc));
        Log.i(TAG,"a Double: " + df.format(total-desc));

        final Double formattedTotal = Double.parseDouble(df.format(total-desc)); //String.format( "%.2f", total ));
        DecimalFormat decimalFormat = new DecimalFormat("#");



        lblSubTotal.setText("S/ "+df2.format(subtotal));
        if(mParam1.equalsIgnoreCase("BOLETA")){
            Log.i(TAG,"ES BOLETA");
             //igv = 0.0;
        }
        lblIVG.setText("S/ "+df2.format(igv));
        lblTotal.setText("S/  "+df2.format(formattedTotal));
        lblDescuento.setText("("+mTransaction.getDescuento()+"%) S/"+df2.format(desc));

        TextView lblTotalStr = v.findViewById(R.id.lblTotalStr);
        MonedaSoles mon = new MonedaSoles();
        try{
            Log.i(TAG,"formattedTotal w/o df2 "+formattedTotal);
            Log.i(TAG,"formattedTotal w df2 "+ df2.format(formattedTotal));
            Log.i(TAG,"");

            lblTotalStr.setText(mon.getMontoEnPalabras(formattedTotal,mTransaction.getMoneda()));
            //lblTotalStr.setText(mon.doConversion(formattedTotal));

        }catch (Exception e){
            //Snackbar snackbar = Snackbar.make(getActivity().findViewById(android.R.id.content),"Monto total excede maximo",Snackbar.LENGTH_SHORT);
            //snackbar.show();
            //FragmentManager fm = getFragmentManager();
            //fm.popBackStack();
            Log.e(TAG,"ERROR X1"+e.toString());
            lblTotalStr.setText("SON: "+formattedTotal.toString());
        }


        //Observaciones

        TextView lblObs                 = v.findViewById(R.id.lblObs);
        lblObs.setText(mTransaction.getObservaciones());

        //Boton

        Button btnConfirmarEmitir = v.findViewById(R.id.btnConfirmarEmitir);

        btnConfirmarEmitir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
















        btnConfirmarEmitir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                StringBuilder dstr = new StringBuilder();

                MonedaSoles mon = new MonedaSoles();
                DecimalFormat df = new DecimalFormat("#.00");
                String posA = df.format(igv); //"101"; // IGV calculado sobre el subtotal
                String posB = "202";
                String posC = formattedTotal+""; //303 // Total con descuento + igv
                String posD = "0";// otros cargos
                String posE = desc+"";//"505"; // total descuento
                String posF = df.format(subtotal-desc)+"";//"606"; // sumatoria subtotal SIN IGV
                String posG = formattedTotal+"";//"707";




                try{


                    String usuario = "admin";
                    String tipoDocumentoCat06 = "6";
                    String tipoDocumentoCa06E = "6";

                    Log.i(TAG,"FechaEmision "+mTransaction.getFechaEmision());
                    String fechaEmisionFmt[] = mTransaction.getFechaEmision().split("/"); //= mTransaction.getFechaEmision().replace("/","-");


                    String template = "FACTURA";
                    String tipoEmision = "01";

                    Log.i(TAG,"mParam 1 ~> "+mParam1);

                    if(mParam1.equalsIgnoreCase("BOLETA")){
                        template = "BOLETA";
                        tipoEmision = "03";
                    }




                    if(mParam1.equalsIgnoreCase("BOLETA") || mParam1.equalsIgnoreCase("FACTURA")){

                        BoletaTextGenerator boletaTextGenerator = new BoletaTextGenerator();
                        String textBoleta = boletaTextGenerator.generarBoleta(mTransaction,mParam1);
                        Log.i(getClass().getName(),"Boleta -> "+textBoleta);

                        /*

                        dstr.append("E|"+mTransaction.getSerie()+""+""+"|"+fechaEmisionFmt[2]+"-"+fechaEmisionFmt[1]+"-"+fechaEmisionFmt[0]+"|"+tipoEmision+"|"+"PEN|"+template+"|ANDROID_APP_GENER|"+usuario+"|||||Facturador App||||\n");
                        dstr.append("A|"+Dconfig.ruc+"|"+tipoDocumentoCat06+"|"+Dconfig.razonSocial+"|"+Dconfig.ubigeo+"|"+Dconfig.direccion+"||||||"+Dconfig.razonSocial+"\n");
                        dstr.append("R|"+mTransaction.getCliente().getRuc()+"|"+tipoDocumentoCa06E+"|"+mTransaction.getCliente().getRazonSocial()+"|"+mTransaction.getCliente().getDireccion()+"\n");

                        int idDet = 1;


                        for (Detalle detalle : mTransaction.getListaDetalles()){
                            //dstr.append("D|"+detalle.codigo+"|"+"?|"+"1|"+detalle.valorUnitario+"|||"+detalle.descripcion+"|"+detalle.valorUnitario+"|\n");

                            Double tax      = detalle.valorUnitario * 0.18;
                            String proTax   =df.format(detalle.valorUnitario + tax);

                            //dstr.append("D|"+idDet+"|NIU|"+detalle.cantidad+"|"+detalle.valorSubTotal+"|"+proTax+"|01|||"+detalle.descripcion+"|"+detalle.codigo+"|"+detalle.valorUnitario+"|"+"FALSE|0.00"+"\n");
                            dstr.append("D|"+idDet+"|NIU|"+detalle.cantidad+"|"+df.format(detalle.valorSubTotal)+"|"+proTax+"|01|||"+detalle.descripcion+"|"+detalle.codigo+"|"+detalle.valorUnitario+"|"+"|0.00"+"\n");
                            idDet = idDet+1;
                        }
                        int pd = 1;
                        for (Detalle detalle : mTransaction.getListaDetalles()){
                            //dstr.append("D|"+detalle.codigo+"|"+"?|"+"1|"+detalle.valorUnitario+"|||"+detalle.descripcion+"|"+detalle.valorUnitario+"|\n");
                            dstr.append("SI|"+"1000|"+"IGV|"+"VAT|"+df.format(detalle.valorUnitario*0.18)+"|"+df.format(detalle.valorUnitario*0.18)+"|"+"10|"+pd+"\n");
                            pd = pd+1;

                        }



                        Log.i(TAG,"MATTER: "+igv);

                        dstr.append("T|"+(posA)+"||1000|IGV|VAT\n");
                        dstr.append("TA|"+posC+"|"+posD+"|"+posE+"||\n");
                        dstr.append("CT|1001|Total valor de venta - operaciones gravadas|0|"+posF+"|"+posG+"|\n");

                        dstr.append("AP|1000|1000|"+mon.getMontoEnPalabras(formattedTotal,mTransaction.getMoneda())+"\n");
                        String fchVenc[] = mTransaction.getFechaVencimiento().split("/");
                        dstr.append("AP|FECVENC|9001|"+fchVenc[2]+"-"+fchVenc[1]+"-"+fchVenc[0]);
                        Log.i(getClass().getName(),dstr.toString());


                        Log.i(getClass().getName(),"T~> "+dstr.toString());

                        String b64Data = Base64.encodeToString(dstr.toString().getBytes(),Base64.NO_WRAP);

*/

                        String b64Data = Base64.encodeToString(textBoleta.getBytes(),Base64.NO_WRAP);

                        Intent intent = new Intent(getActivity(),EmisionOkActivity.class);
                        intent.putExtra("b64Data",b64Data);
                        intent.putExtra("tipo",mParam1);

                        startActivityForResult(intent,102);

                        //FragmentManager fm = getFragmentManager();
                        //fm.popBackStack("ROOT",FragmentManager.POP_BACK_STACK_INCLUSIVE);



                    }

                    if(mParam1.equalsIgnoreCase("NOTAC_BOLETA") || mParam1.equalsIgnoreCase("NOTAC_FACTURA")) {
                        /*Nota Credito*/


                        NotaCreditoTextGenerator notaCreditoTextGenerator = new NotaCreditoTextGenerator();
                        String StringNotaCredito       =   notaCreditoTextGenerator.generarBoleta(mTransaction,mParam1);
                     /*   tipoEmision = "07";

                        template = "NOTACREDITO";
                        String motivo = mTransaction.getMotivoSustento();
                        String cod9 = mTransaction.getCodigoNotaStr();
                        dstr.append("E|" + mTransaction.getSerie() + "" + "" + "|" + fechaEmisionFmt[2] + "-" + fechaEmisionFmt[1] + "-" + fechaEmisionFmt[0] + "|" + tipoEmision + "|" + "PEN|" + template + "|ANDROID_APP_GENER|" + usuario + "|||" + motivo + "|" + cod9 + "|Facturador App|\n");//|||
                        dstr.append("A|" + Dconfig.ruc + "|" + tipoDocumentoCat06 + "|" + Dconfig.razonSocial + "|" + Dconfig.ubigeo + "|" + Dconfig.direccion + "||||||" + Dconfig.razonSocial + "\n");
                        dstr.append("R|" + mTransaction.getCliente().getRuc() + "|" + tipoDocumentoCa06E + "|" + mTransaction.getCliente().getRazonSocial() + "|" + mTransaction.getCliente().getDireccion() + "\n");
                        dstr.append("CT|1001|Total valor de venta - operaciones gravadas|0|" + posF + "|" + posG + "|\n");
                        int idDet = 1;
                        for (Detalle detalle : mTransaction.getListaDetalles()){
                            //dstr.append("D|"+detalle.codigo+"|"+"?|"+"1|"+detalle.valorUnitario+"|||"+detalle.descripcion+"|"+detalle.valorUnitario+"|\n");

                            Double tax = detalle.valorUnitario * 0.18;
                            String proTax = df.format(detalle.valorUnitario + tax);
                            dstr.append("D|"+idDet+"|NIU|"+detalle.cantidad+"|"+df.format(detalle.valorSubTotal)+"|||||"+detalle.descripcion+"|"+detalle.codigo+"|"+(detalle.valorSubTotal)+"|"+detalle.valorUnitario+"|01"+"\n");
                            idDet = idDet+1;
                        }
                        int pd = 1;
                        for (Detalle detalle : mTransaction.getListaDetalles()){
                            //dstr.append("D|"+detalle.codigo+"|"+"?|"+"1|"+detalle.valorUnitario+"|||"+detalle.descripcion+"|"+detalle.valorUnitario+"|\n");
                            dstr.append("SI|"+"1000|"+"IGV|"+"VAT|"+df.format(detalle.valorUnitario*0.18)+"|"+df.format(detalle.valorUnitario*0.18)+"|"+"10|"+pd+"\n");
                            pd = pd+1;

                        }
                        dstr.append("T|"+(posA)+"||1000|IGV|VAT\n");
                        dstr.append("TA|"+posC+"|"+posD+"|"+posE+"||\n");
                        String tipoDoc = "03";
                        if (mParam1.equalsIgnoreCase("NOTAC_BOLETA")){
                            tipoDoc = "03";
                        }else {
                            tipoDoc = "01";
                        }
                        dstr.append("FD|"+mTransaction.getModSerie()+"-"+mTransaction.getModFolio()+"|"+tipoDoc);


                        Log.i(TAG,"~> "+dstr.toString());

                        String b64Data = Base64.encodeToString(dstr.toString().getBytes(),Base64.NO_WRAP);
*/

                        String b64Data = Base64.encodeToString(StringNotaCredito.getBytes(),Base64.NO_WRAP);
                        Intent intent = new Intent(getActivity(),EmisionOkActivity.class);
                        intent.putExtra("b64Data",b64Data);
                        intent.putExtra("tipo",mParam1);


                        startActivityForResult(intent,102);



                    }

                    if(mParam1.equalsIgnoreCase("NOTAD_BOLETA") || mParam1.equalsIgnoreCase("NOTAD_FACTURA")){
                        /*Nota debito*/
                        Log.i(TAG,"EMITIR NOTA DEBITO");
                        NotaDebitoTextGenerator notaDebitoTextGenerator = new NotaDebitoTextGenerator();
                        String StringNotaDebitp = notaDebitoTextGenerator.generarBoleta(mTransaction,mParam1);

                       /*
                        tipoEmision = "08";

                        template = "NOTADEBITO";
                        String motivo = mTransaction.getMotivoSustento();
                        String cod9 = mTransaction.getCodigoNotaStr();
                        dstr.append("E|" + mTransaction.getSerie() + "" + "" + "|" + fechaEmisionFmt[2] + "-" + fechaEmisionFmt[1] + "-" + fechaEmisionFmt[0] + "|" + tipoEmision + "|" + "PEN|" + template + "|ANDROID_APP_GENER|" + usuario + "|||" + motivo + "|" + cod9 + "|Facturador App|\n");//|||
                        dstr.append("A|" + Dconfig.ruc + "|" + tipoDocumentoCat06 + "|" + Dconfig.razonSocial + "|" + Dconfig.ubigeo + "|" + Dconfig.direccion + "||||||" + Dconfig.razonSocial + "\n");
                        dstr.append("R|" + mTransaction.getCliente().getRuc() + "|" + tipoDocumentoCa06E + "|" + mTransaction.getCliente().getRazonSocial() + "|" + mTransaction.getCliente().getDireccion() + "\n");
                        dstr.append("CT|1001|Total valor de venta - operaciones gravadas|0|" + posF + "|" + posG + "|\n");

                        int idDet = 1;
                        for (Detalle detalle : mTransaction.getListaDetalles()){
                            //dstr.append("D|"+detalle.codigo+"|"+"?|"+"1|"+detalle.valorUnitario+"|||"+detalle.descripcion+"|"+detalle.valorUnitario+"|\n");

                            Double tax = detalle.valorUnitario * 0.18;
                            String proTax = df.format(detalle.valorUnitario + tax);
                            //D|1|NIU|1|600.0|||||Te Lipton Caja Roja sabor amargo|7805000313944|600.0|708.0|01
                            dstr.append("D|"+idDet+"|NIU|"+detalle.cantidad+"|"+df.format(detalle.valorSubTotal)+"|||||"+detalle.descripcion+"|"+detalle.codigo+"|"+(detalle.valorSubTotal)+"|"+detalle.valorUnitario                                                                                                                                                                                                                                                   +"|01"+"\n");
                            idDet = idDet+1;
                        }

                        int pd = 1;
                        for (Detalle detalle : mTransaction.getListaDetalles()){
                            //dstr.append("D|"+detalle.codigo+"|"+"?|"+"1|"+detalle.valorUnitario+"|||"+detalle.descripcion+"|"+detalle.valorUnitario+"|\n");
                            dstr.append("SI|"+"1000|"+"IGV|"+"VAT|"+df.format(detalle.valorUnitario*0.18)+"|"+df.format(detalle.valorUnitario*0.18)+"|"+"10|"+pd+"\n");
                            pd = pd+1;

                        }
                        dstr.append("T|"+(posA)+"||1000|IGV|VAT\n");
                        dstr.append("TA|"+posC+"|"+posD+"|"+posE+"||\n");
                        String tipoDoc = "03";
                        if (mParam1.equalsIgnoreCase("NOTAD_BOLETA")){
                            tipoDoc = "03";
                        }else {
                            tipoDoc = "01";
                        }
                        dstr.append("FD|"+mTransaction.getModSerie()+"-"+mTransaction.getModFolio()+"|"+tipoDoc);

                        Log.i(TAG,"~> "+dstr.toString());


                        String b64Data = Base64.encodeToString(dstr.toString().getBytes(),Base64.NO_WRAP);
*/
                       String b64Data = Base64.encodeToString(StringNotaDebitp.getBytes(),Base64.NO_WRAP);
                        Intent intent = new Intent(getActivity(),EmisionOkActivity.class);
                        intent.putExtra("b64Data",b64Data);
                        intent.putExtra("tipo",mParam1);


                        startActivityForResult(intent,102);

                    }




                }catch (Exception e){
                    Log.e(TAG,"ERROR X4 "+e.toString());


                    Log.i(TAG,"DSTR "+dstr.toString());

                    Log.i(TAG," PosA :" + posA);
                    Log.i(TAG," PosC :" + posC);
                    Log.i(TAG," PosE :" + posE);
                    Log.i(TAG," PosF :" + posF);
                    Log.i(TAG," PosG :" + posG);
                }








                /*
                Thread th = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        llamarWS();
                    }
                });

                th.run();*/

            }
        });

        return v;
    }


    public void llamarWS(){

        StringBuilder bodyReq = new StringBuilder();
        //1 armar cabecera String

        //2 armar detalle

        //3 armar otros

        //4 codificar a base 6

        //5 armar request


        String b64BodyReq = Base64.encodeToString(bodyReq.toString().getBytes(),Base64.DEFAULT);


        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("<?xml version=\"1.0\"?\n>");
        stringBuilder.append("  <EnvioLoteWebService>\n");
        stringBuilder.append("      <idEmpresa>"+Dconfig.ruc+"</idEmpresa>\n");
        stringBuilder.append("      <lote>"+ b64BodyReq +"</lote>\n");
        stringBuilder.append("</EnvioLoteWebService>");

        DDTEServiceSoapBinding service = new DDTEServiceSoapBinding(new DIServiceEvents() {
            @Override
            public void Starting() {

                Log.i(getClass().getName(),"STARTING");
            }

            @Override
            public void Completed(DOperationResult result) {

                Log.i(getClass().getName(),"R: "+result);
                String respuesta = (String) result.Result;


                Log.i(getClass().getName(),"Resp: "+respuesta);
            }
        });

        String xmlString = stringBuilder.toString();
        service.procesarLoteAsync("<?xml version=\"1.0\" ?><EnvioLoteWebService idEmpresa=\"20379372741\"><![CDATA[RXxGMDAxfDIwMTYtMDktMTV8MDF8UEVOfEZBQ1RVUkF8MDAzNDA3ODk2MnxTUVVJU1BFTXxMSTE4fCAgICAxfHx8UjNRfERURS1QZXJ1QGluZHVyYS5uZXR8MA0KQXwyMDM3OTM3Mjc0MXw2fElORFVSQVBFUlVTLkEufDMwMTAxfEF2LkVsUGFjaWZpY280MDEtNDIzfHxMaW1heUNhbGxhb3x8SW5kZXBlbmRlbmNpYSxMaW1hfFBFfElORFVSQVBFUlVTLkEuDQpSfDIwNTM2NTU3ODU4fDZ8SE9NRUNFTlRFUlMgUEVSVUFOT1MgUy5BLnwNCkR8MDAxfEtHfDE1MHwxNDU4LjAwfDE3MjAuNDR8MXx8fEVMRUNUUk9ETyA2MDExIChQKSAxLzh8MDAwMDAwMDAwMDAyMDAxMDYwfDkuNzJ8fA0KRHwwMDJ8S0d8NDV8MjU4LjMwfDMwNC43OXwxfHx8QUxBTUJSRSBNSUcgSU5EVVJBIDcwUzYgIDA4WDE1IFNTfDAwMDAwMDAwMDAwMTAxNTMzMnw1Ljc0fHwNCkR8MDAzfEtHfDI1fDI2OC4wMHwzMTYuMjR8MXx8fEVMRUNUUk9ETyA2MDExIChQKSAzLzMyICBFTlZBU0FETyAoIDFLR3wwMDAwMDAwMDAwMDIwMDE4ODB8MTAuNzJ8fA0KRHwwMDR8S0d8NzV8NzUwLjAwfDg4NS4wMHwxfHx8RUxFQ1RST0RPIDYwMTEgKFApIDEvOCAgRU5WQVNBRE8gKCAxS0d8MDAwMDAwMDAwMDAyMDAxODYwfDEwLjAwfHwNCkR8MDA1fFVOfDIwfDE1Ni4wMHwxODQuMDh8MXx8fEVMRUNUUk9ETyBFMzA4TCAzLzMyICAoMyBWQVJJTExBUyl8MDAwMDAwMDAwMDAyMDAyOTE3fDcuODB8fA0KRHwwMDZ8S0d8MjV8MjY4LjAwfDMxNi4yNHwxfHx8RUxFQ1RST0RPIDIzMCAoUCkgMy8zMiAgRU5WQVNBRE8gKCAxIEtHfDAwMDAwMDAwMDAwMjAwMTg4M3wxMC43Mnx8DQpEfDAwN3xLR3wyNXwyNjguMDB8MzE2LjI0fDF8fHxFTEVDVFJPRE8gNjAxMSAoUCkgMy8zMiAgRU5WQVNBRE8gKCAxS0d8MDAwMDAwMDAwMDAyMDAxODgwfDEwLjcyfHwNCkR8MDA4fEtHfDI1fDI1MC4wMHwyOTUuMDB8MXx8fEVMRUNUUk9ETyA2MDExIChQKSAxLzggIEVOVkFTQURPICggMUtHfDAwMDAwMDAwMDAwMjAwMTg2MHwxMC4wMHx8DQpEfDAwOXxLR3wyNXwyNTAuMDB8Mjk1LjAwfDF8fHxFTEVDVFJPRE8gNjAxMSAoUCkgMS84ICBFTlZBU0FETyAoIDFLR3wwMDAwMDAwMDAwMDIwMDE4NjB8MTAuMDB8fA0KVHw3MDYuNzN8NzA2LjczfDEwMDB8SUdWfFZBVA0KU0l8MTAwMHxJR1Z8VkFUfDI2Mi40NHwyNjIuNDR8MTB8MDAxDQpTSXwxMDAwfElHVnxWQVR8NDYuNDl8NDYuNDl8MTB8MDAyDQpTSXwxMDAwfElHVnxWQVR8NDguMjR8NDguMjR8MTB8MDAzDQpTSXwxMDAwfElHVnxWQVR8MTM1LjAwfDEzNS4wMHwxMHwwMDQNClNJfDEwMDB8SUdWfFZBVHwyOC4wOHwyOC4wOHwxMHwwMDUNClNJfDEwMDB8SUdWfFZBVHw0OC4yNHw0OC4yNHwxMHwwMDYNClNJfDEwMDB8SUdWfFZBVHw0OC4yNHw0OC4yNHwxMHwwMDcNClNJfDEwMDB8SUdWfFZBVHw0NS4wMHw0NS4wMHwxMHwwMDgNClNJfDEwMDB8SUdWfFZBVHw0NS4wMHw0NS4wMHwxMHwwMDkNClRBfDQ2MzMuMDN8fA0KQ1R8MTAwMXxUb3RhbHZhbG9yZGV2ZW50YS1vcGVyYWNpb25lc2dyYXZhfHwzOTI2LjMwfHwNCkFQfDEwMDB8MTAwMHxDVUFUUk8gTUlMIFNFSVNDSUVOVE9TIFRSRUlOVEEgWSBUUkVTIFNPTEVTIFkgVFJFUyBDRU5UQVZPUw0KQVB8MDAwMXxBUFZFTkR8QW50b25pbyBNZW5kZXoNCkFQfDAwMDJ8QVBCT0R8MDENCkFQfDAwMDN8QVBESVJFQ0NJT058QVYuIEdVQVJESUEgIENJVklMIENEUkEgMTAgTEEgQ0FNUElOQQ0KQVB8MDAwNHxBUERJUkVDQ0lPTjJ8Q0hPUlJJTExPU0xJTUENCkFQfDAwMDZ8QVBWRU5DTU5UT3wyMDE2LTA3LTI5DQpBUHwwMDA3fEFQT0N8NjE0MjQxDQpBUHwwMDA4fEFQRkNIT0N8MjAxNi0wNy0xOA0KQVB8MDAwOXxBUEZPUk1BUEFHT3w2MCBkaWFzDQpBUHwwMDEwfEFBUFBMQU5UQXxTdWN1cnNhbCAgTGltYQ0KQVB8MDAxMXxBUEZBQ1RVUkF8MDAzNDA3ODk2Mg0KQVB8MDAxMnxBUE9CU0VSVkFDSU9OfDAyMjEzNzA2NzA=]]></EnvioLoteWebService>");

    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onActivityResult(int requestCode,int resultCode,Intent data){
        if(requestCode == 102 && resultCode == 204 ){
            FragmentManager fm = getFragmentManager();
            fm.popBackStack("ROOT",FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }

    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    static class DetalleReviewResultViewHolder{
        TextView lblNombreProdTV;
        TextView lblCodProdTV;
        TextView lblValorProdTV;
        TextView lbltxtQtyTsub;
        LinearLayout prodRevLayout;
    }


    class DetalleReviewBaseAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return mTransaction.getListaDetalles().size();
        }

        @Override
        public Object getItem(int position) {
            return mTransaction.getListaDetalles().get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        DetalleReviewResultViewHolder viewHolder;
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            Detalle detalle = mTransaction.getListaDetalles().get(position);

            if (convertView == null){

                LayoutInflater inflater = getActivity().getLayoutInflater();
                convertView = inflater.inflate(R.layout.row_detalle_review,null,false);
                viewHolder = new DetalleReviewResultViewHolder();
                viewHolder.lblNombreProdTV = convertView.findViewById(R.id.lblNombreProd);
                viewHolder.lblCodProdTV = convertView.findViewById(R.id.lblCodProd);
                viewHolder.lblValorProdTV = convertView.findViewById(R.id.lblValorProd);
                viewHolder.prodRevLayout = convertView.findViewById(R.id.prodRevLayout);
                viewHolder.lbltxtQtyTsub = convertView.findViewById(R.id.txtQtyTsub);
                convertView.setTag(viewHolder);

            }else{
                viewHolder = (DetalleReviewResultViewHolder) convertView.getTag();
            }

            if(position % 2 == 0){

                viewHolder.prodRevLayout.setBackgroundColor(getResources().getColor(R.color.alt_row));
            }else{
                viewHolder.prodRevLayout.setBackgroundColor(getResources().getColor(R.color.info_form));
            }

            DecimalFormat df = new DecimalFormat("#.00");


            viewHolder.lblNombreProdTV.setText(detalle.descripcion);
            viewHolder.lblCodProdTV.setText("Codigo: "+detalle.codigo);
            viewHolder.lbltxtQtyTsub.setText(""+ df.format(detalle.valorUnitario)  + " x "+detalle.cantidad);
            viewHolder.lblValorProdTV.setText("S/ "+ df.format(detalle.valorSubTotal) );

            return convertView;
        }
    }
}
