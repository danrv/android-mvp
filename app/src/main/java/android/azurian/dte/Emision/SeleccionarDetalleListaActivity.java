package android.azurian.dte.Emision;

import android.azurian.dte.Persistencia.DetallesSQLOpenHelper;
import android.azurian.dte.R;
import android.azurian.dte.services.Dconfig;
import android.azurian.dte.services.Detalle;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class SeleccionarDetalleListaActivity extends AppCompatActivity {

    List<Detalle> listaDetalles;
    ListView listaDetallesGuardados;
    DetalleResultAdapter adapter;
    EditText inTxtSrchDetalle;
    final String TAG = getClass().getSimpleName();

    Button btnLeerCodigo;

    ProgressBar loaderSelDetalle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seleccionar_detalle_lista);


        inTxtSrchDetalle = findViewById(R.id.inTxtSrchDetalle);

        btnLeerCodigo           = findViewById(R.id.btnLeerCodigo);
        listaDetallesGuardados  = findViewById(R.id.listaDetallesGuardados);
        loaderSelDetalle        = findViewById(R.id.loaderSelDetalle);

        btnLeerCodigo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new IntentIntegrator(SeleccionarDetalleListaActivity.this).initiateScan();
            }
        });

        listaDetallesGuardados.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.i(TAG,"SELEC");

                final int pos = position;

                Detalle detalle = listaDetalles.get(pos);

                AlertDialog.Builder consultaQty = new android.support.v7.app.AlertDialog.Builder(SeleccionarDetalleListaActivity.this, R.style.ThemeOverlay_AppCompat_Dialog_Alert);
                LinearLayout layout = new LinearLayout(getApplicationContext());
                layout.setOrientation(LinearLayout.VERTICAL);

                consultaQty.setTitle("Cantidad");
                consultaQty.setMessage("Ingrese Cantidad");
                final EditText input = new EditText(getApplicationContext());
                input.setInputType(InputType.TYPE_CLASS_NUMBER);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT);
                input.setLayoutParams(lp);

                final EditText input2 = new EditText(getApplicationContext());
                input2.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL);
                input2.setLayoutParams(lp);


                layout.addView(input);
                layout.addView(input2);
                consultaQty.setView(layout);
                input.setHint("Cantidad");
                input2.setHint("Valor (Blanco valor por lista "+detalle.valorUnitario+")");


                consultaQty.setPositiveButton("Listo", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String cantidad = input.getText().toString();
                        String valor = input2.getText().toString();

                        try{
                            int qty = Integer.parseInt(cantidad);


                            Detalle detalle = listaDetalles.get(pos);

                            if(!valor.isEmpty()){

                                double val = Double.parseDouble(valor);
                                detalle.valorUnitario = val;

                            }
                            detalle.valorSubTotal = detalle.valorUnitario * qty;
                            detalle.cantidad = qty;
                            Intent intent = new Intent();
                            intent.putExtra("item",detalle);
                            intent.putExtra("Eep","eep");
                            setResult(101,intent);
                            finish();


                        }catch (Exception e){
                            Log.e(TAG,"cantidad to qty "+e.toString());
                            /*
                            Intent intent = new Intent();
                            setResult(201,intent);
                            finish();
                            */
                            Toast.makeText(getApplicationContext(),"Deben ser numeros",Toast.LENGTH_SHORT);
                        }
                    }
                });

                consultaQty.show();

                /*


                */
            }
        });

        listaDetalles = new ArrayList<Detalle>();

        adapter = new DetalleResultAdapter();
        listaDetallesGuardados.setAdapter(adapter);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                loadDataItems();
            }
        });

        thread.start();


        inTxtSrchDetalle.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if(actionId == EditorInfo.IME_ACTION_DONE || actionId ==  EditorInfo.IME_ACTION_NEXT){


                    String param = v.getEditableText().toString();
                    buscarContenido(param);

                    return true;

                }

                return false;
            }
        });
    }


    private void buscarContenido(String p){

        String param = p;
        DetallesSQLOpenHelper admin = new DetallesSQLOpenHelper(getApplicationContext(),"detalles",null,Dconfig.dbVersion);

        SQLiteDatabase bd = admin.getReadableDatabase();

        String query = "SELECT * FROM detalles WHERE (descripcion LIKE '%"+param+"%' OR codigo LIKE '%"+param+"%')";
        Log.i(TAG,"Q:"+query);
        Cursor fila = bd.rawQuery(query,null);

        listaDetalles.clear();

        while (fila.moveToNext()){

            Detalle detalle = new Detalle();
            detalle.descripcion = fila.getString(0);
            detalle.codigo = fila.getString(1);
            detalle.tipoIGV = fila.getString(2);
            detalle.valorUnitario = fila.getDouble(3);
            listaDetalles.add(detalle);
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
            }
        });

    }


    private void loadDataItems(){

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loaderSelDetalle.setVisibility(View.VISIBLE);
            }
        });

        final FirebaseDatabase database = FirebaseDatabase.getInstance();

        final DatabaseReference myRef = database.getReference(Dconfig.ruc+"/items");
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                DetallesSQLOpenHelper admin = new DetallesSQLOpenHelper(getApplicationContext(),"detalles",null,Dconfig.dbVersion);

                SQLiteDatabase bd = admin.getWritableDatabase();

                try{
                    bd.execSQL("DELETE FROM detalles");

                    for(DataSnapshot item : dataSnapshot.getChildren()){
                        Detalle detalle = item.getValue(Detalle.class);
                        Log.i(TAG,detalle.descripcion);
                        //listaDetalles.add(detalle);

                        ContentValues bdDetalle = new ContentValues();

                        bdDetalle.put("descripcion",detalle.descripcion);
                        bdDetalle.put("codigo",detalle.codigo);
                        bdDetalle.put("tipoIGV",detalle.tipoIGV);
                        bdDetalle.put("valorUnitario",detalle.valorUnitario);
                        bd.insert("detalles",null,bdDetalle);

                    }
                }catch (Exception e){
                    Log.e(TAG,"ERROR @ "+e.getMessage());
                }
                finally {
                    bd.close();
                }





                try{

                    listaDetalles.clear();

                    bd = admin.getReadableDatabase();

                    String query = "SELECT * FROM detalles";
                    Log.i(TAG,"Q:"+query);
                    Cursor fila = bd.rawQuery(query,null);

                    while (fila.moveToNext()){

                        Detalle detalle = new Detalle();
                        detalle.descripcion = fila.getString(0);
                        detalle.codigo = fila.getString(1);
                        detalle.tipoIGV = fila.getString(2);
                        detalle.valorUnitario = fila.getDouble(3);
                        listaDetalles.add(detalle);
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();

                            loaderSelDetalle.setVisibility(View.GONE);
                        }
                    });

                }catch (Exception e){
                    Log.i(TAG,e.toString());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
    static class DetalleResultViewHolder{
        TextView descripcionTV;
        TextView codigoTV;
        TextView valorTV;
    }

    class DetalleResultAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return listaDetalles.size();
        }

        @Override
        public Object getItem(int position) {
            return listaDetalles.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        DetalleResultViewHolder viewHolder;
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Detalle dh = listaDetalles.get(position);

            if (convertView == null){

                LayoutInflater inflater = getLayoutInflater();
                convertView = inflater.inflate(R.layout.row_detalle,null,false);
                viewHolder = new DetalleResultViewHolder();

                viewHolder.descripcionTV = convertView.findViewById(R.id.descDetalle);
                viewHolder.codigoTV = convertView.findViewById(R.id.codDetalle);
                viewHolder.valorTV = convertView.findViewById(R.id.valorDetalle);
                convertView.setTag(viewHolder);
            }else {
                viewHolder = (DetalleResultViewHolder)convertView.getTag();

            }

            DecimalFormat df = new DecimalFormat("0.00");
            viewHolder.descripcionTV.setText(dh.descripcion);
            viewHolder.codigoTV.setText(dh.codigo);
            viewHolder.valorTV.setText("S/ "+df.format(dh.valorUnitario));

            return convertView;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){

        if(requestCode == 201){
            listaDetalles.clear();
            adapter.notifyDataSetChanged();

            loadDataItems();
        }else{

            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if(result != null) {
                if(result.getContents() == null) {
                    Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(this, "Scanned: " + result.getContents(), Toast.LENGTH_LONG).show();

                    inTxtSrchDetalle.setText(result.getContents());
                    buscarContenido(result.getContents());
                }
            } else {
                super.onActivityResult(requestCode, resultCode, data);
            }

        }




    }
}
