package android.azurian.dte.Emision;

import android.azurian.dte.AgregarClienteActivity;
import android.azurian.dte.Persistencia.ClientesSQLOpenHelper;
import android.azurian.dte.R;
import android.azurian.dte.services.Cliente;
import android.azurian.dte.services.Dconfig;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.test.LoaderTestCase;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MenuClientesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MenuClientesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MenuClientesFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public MenuClientesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MenuClientesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MenuClientesFragment newInstance(String param1, String param2) {
        MenuClientesFragment fragment = new MenuClientesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    Button btnAddCliente;
    ListView listaClientesDisp;
    List<Cliente> shallowList;
    EditText inTxtSearchClient;
    ClienteResultAdapter adapter;
    final String TAG = getClass().getSimpleName();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_menu_clientes, container, false);


        adapter = new ClienteResultAdapter();



        ClientesSQLOpenHelper admin = new ClientesSQLOpenHelper(getContext(),"clientes",null, Dconfig.dbVersion);
        SQLiteDatabase bd = admin.getWritableDatabase();

        //bd.rawQuery("DROP TABLE IF EXISTS clientes",null);

        try{

            bd.execSQL("DELETE FROM clientes");

        }catch (Exception e){
            Log.e(TAG,e.toString());
        }


        shallowList = new ArrayList<Cliente>();
        listaClientesDisp = v.findViewById(R.id.listaClientesDisp);

        listaClientesDisp.setAdapter(adapter);





        inTxtSearchClient = v.findViewById(R.id.inTxtSearchClient);

        btnAddCliente = v.findViewById(R.id.btnAddCliente);
        btnAddCliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG,"nueva empresa");


                Intent i = new Intent(getContext(),AgregarClienteActivity.class);
                startActivityForResult(i,201);

            }
        });

        inTxtSearchClient.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                Log.i(TAG,""+actionId);
                if(actionId == EditorInfo.IME_ACTION_DONE || actionId ==  EditorInfo.IME_ACTION_NEXT){




                    String param = v.getEditableText().toString();
                    ClientesSQLOpenHelper admin = new ClientesSQLOpenHelper(getContext(),"clientes",null,Dconfig.dbVersion);
                    SQLiteDatabase bd = admin.getReadableDatabase();

                    Log.i(TAG,"BUSCAR "+param);
                    bd = admin.getReadableDatabase();

                    String query = "SELECT * FROM clientes WHERE (ruc LIKE '%"+param+"%' OR nombre LIKE '%"+param+"%')";
                    Log.i(TAG,"Q:"+query);
                    Cursor fila = bd.rawQuery(query,null);


                    shallowList = new ArrayList<>();

                    while (fila.moveToNext()){
                        Log.i(TAG,"F: "+fila.getString(0));
                        Cliente cli = new Cliente();
                        cli.setRuc(fila.getString(0));
                        cli.setRazonSocial(fila.getString(1));
                        cli.setDireccion(fila.getString(2));
                        cli.setContacto(fila.getString(3));
                        cli.setOtro(fila.getString(4));
                        cli.setFbkey(fila.getString(5));
                        cli.setTipoDocumento(fila.getString(6));

                        shallowList.add(cli);

                    }
                    bd.close();
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            adapter.notifyDataSetChanged();

                        }
                    });
                    bd.close();
                }
                return false;
            }
        });

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                loadDataClientes();
            }
        });
        thread.start();

        listaClientesDisp.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Cliente c = shallowList.get(position);
                Log.i(TAG,"**** " + c.getFbkey());
                Intent intent = new Intent(getContext(),AgregarClienteActivity.class);
                intent.putExtra("cliente",c);
                startActivity(intent);
            }
        });
        return v;
    }

    private void loadDataClientes(){

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference("clientes/"+Dconfig.user);


        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                try{
                    ClientesSQLOpenHelper admin = new ClientesSQLOpenHelper(getContext(),"clientes",null,Dconfig.dbVersion);
                    SQLiteDatabase bd = admin.getWritableDatabase();

                    bd.execSQL("DELETE FROM clientes");


                    shallowList.clear();
                    Cliente cliente = null;
                    for (DataSnapshot clienteSnapshot : dataSnapshot.getChildren()){
                        cliente = clienteSnapshot.getValue(Cliente.class);
                        Log.i(TAG,cliente.getRazonSocial() + " "+clienteSnapshot.getKey());
                        //shallowList.add(cliente);
                        ContentValues bdCliente = new ContentValues();
                        bdCliente.put("ruc",cliente.getRuc());
                        bdCliente.put("nombre",cliente.getRazonSocial());
                        bdCliente.put("direccion",cliente.getDireccion());
                        bdCliente.put("correo",cliente.getContacto());
                        bdCliente.put("otro",cliente.getOtro());
                        bdCliente.put("fbkey",clienteSnapshot.getKey());
                        bdCliente.put("tipoDocumento",cliente.getTipoDocumento());

                        bd.insert("clientes",null,bdCliente);

                    }
                    bd.close();

                    try{

                        bd = admin.getReadableDatabase();

                        String query = "SELECT * FROM clientes";
                        Log.i(TAG,"Q:"+query);
                        Cursor fila = bd.rawQuery(query,null);


                        while (fila.moveToNext()){
                            Log.i(TAG,"F: "+fila.getString(0));
                            Cliente cli = new Cliente();
                            cli.setRuc(fila.getString(0));
                            cli.setRazonSocial(fila.getString(1));
                            cli.setDireccion(fila.getString(2));
                            cli.setContacto(fila.getString(3));
                            cli.setOtro(fila.getString(4));
                            cli.setFbkey(fila.getString(5));
                            cli.setTipoDocumento(fila.getString(6));

                            shallowList.add(cli);
                        }
                        bd.close();

                    }catch (Exception e){

                        Log.e(TAG,e.toString());
                    }





                    try {

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //sds listaClientesDisp.setAdapter(new ClienteResultAdapter());

                                adapter.notifyDataSetChanged();
                            }
                        });

                    }catch (Exception e){
                        Log.e(TAG,e.toString());
                    }
                }catch (Exception e){

                    loadDataClientes();
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    static class ClienteResultViewHolder{
        TextView razonSocialTV;
        TextView rucTV;
        TextView contactoTV;
        TextView direccionTV;
    }

    class ClienteResultAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return shallowList.size();
        }

        @Override
        public Object getItem(int position) {
            return shallowList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        ClienteResultViewHolder viewHolder;
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            Cliente cliente = shallowList.get(position);

            if(convertView==null){
                LayoutInflater inflater = getActivity().getLayoutInflater();
                convertView = inflater.inflate(R.layout.row_cliente,null,false);
                viewHolder = new ClienteResultViewHolder();
                viewHolder.razonSocialTV = convertView.findViewById(R.id.lblRazonSocial);
                viewHolder.rucTV = convertView.findViewById(R.id.lblRuc);
                viewHolder.direccionTV = convertView.findViewById(R.id.lblDireccion);
                viewHolder.contactoTV = convertView.findViewById(R.id.lblContacto);
                convertView.setTag(viewHolder);
            }else{
                viewHolder = (ClienteResultViewHolder)convertView.getTag();
            }

            viewHolder.razonSocialTV.setText(cliente.getRazonSocial());
            viewHolder.rucTV.setText(cliente.getRuc());
            viewHolder.contactoTV.setText(cliente.getContacto());
            viewHolder.direccionTV.setText(cliente.getDireccion());

            return convertView;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){


        if(requestCode == 201){

            Log.i(TAG,"EEEEE"+requestCode);
            shallowList.clear();
            adapter.notifyDataSetChanged();

            loadDataClientes();

        }
    }

}
