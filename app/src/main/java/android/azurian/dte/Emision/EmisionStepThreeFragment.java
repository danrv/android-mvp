package android.azurian.dte.Emision;

import android.azurian.dte.AgregarClienteActivity;
import android.azurian.dte.AgregarNuevoDetalleActivity;
import android.azurian.dte.R;
import android.azurian.dte.services.Cliente;
import android.azurian.dte.services.Detalle;
import android.azurian.dte.services.NormalTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link EmisionStepThreeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link EmisionStepThreeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EmisionStepThreeFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "transaction";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private NormalTransaction mTransaction;

    private OnFragmentInteractionListener mListener;

    public EmisionStepThreeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EmisionStepThreeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EmisionStepThreeFragment newInstance(String param1, String param2, NormalTransaction transaction) {
        EmisionStepThreeFragment fragment = new EmisionStepThreeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putSerializable(ARG_PARAM3,transaction);
        fragment.setArguments(args);
        return fragment;
    }

    List<Detalle> listaDetalles;
    Button btnAddDetalle;
    Button btnSearchDetalle;
    ListView listaDetalle;
    Button btnFinalizarDetalle;

    DetalleResultAdapter adapter;


    double totalSoFar = 0;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            mTransaction = (NormalTransaction) getArguments().get(ARG_PARAM3);
        }
    }



    final String TAG = getClass().getSimpleName();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_facturas_step_three, container, false);

        Log.i(TAG,"PARAM 1 "+mParam1);
        Log.i(TAG,"b: "+mTransaction.isDocumentoGratuito());
        listaDetalles = new ArrayList<>();

        btnAddDetalle       = v.findViewById(R.id.btnAddDetalle);
        btnSearchDetalle    = v.findViewById(R.id.btnSearchDetalle);
        listaDetalle        = v.findViewById(R.id.listaDetalle);
        btnFinalizarDetalle = v.findViewById(R.id.btnFinalizarDetalle);

        btnFinalizarDetalle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mParam1.equalsIgnoreCase("BOLETA")){

                    if (mTransaction.getListaDetalles().isEmpty()){

                        return;
                    }

                    Log.i(TAG,"TOTAL SOFAR "+totalSoFar);
                    if(totalSoFar > 700){

                        Cliente cl = mTransaction.getCliente();
                        if (cl.getRazonSocial().equalsIgnoreCase("Cliente Generico") || cl.getDireccion() == null || cl.getDireccion().isEmpty()){

                            Snackbar.make(btnFinalizarDetalle,"Debes completar datos del cliente",Snackbar.LENGTH_LONG).show();
                            Intent intent = new Intent(getContext(),AgregarClienteActivity.class);
                            if(cl.getRuc() == null || cl.getRuc().isEmpty()){
                                Log.i(TAG,"NASTY 1");
                                intent.putExtra("REQ","NEW");
                            }

                            Log.i(TAG,"NASTY 2");
                            intent.putExtra("cliente",cl);
                            startActivityForResult(intent,201);
                            return;
                        }

                    }else {


                    }
                }

                if(mParam1.equalsIgnoreCase("NOTA_BOLETA") || mParam1.equalsIgnoreCase("NOTA_FACTURA")){

                    //Es emision nota credito/debito, pasamos a referencia/sustento


                }else{

                    /*
                    if(mParam1.equalsIgnoreCase("FACTURA")){
                        ArrayList<Detalle> ld = mTransaction.getListaDetalles();
                        for(Detalle d : mTransaction.getListaDetalles()){

                            double igv = d.valorUnitario * 0.18;
                            d.valorUnitario = d.valorUnitario - igv;
                            d.valorSubTotal = d.valorUnitario * d.cantidad;
                        }


                    }else{

                    }*/
                    //Es emision Boleta/Factura, pasamos a observaciones
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.fragmentHolderEmision, ObservacionesFragment.newInstance(mParam1,"",mTransaction));
                    transaction.addToBackStack("Observaciones");
                    transaction.commit();
                }

            }
        });

        adapter = new DetalleResultAdapter();
        listaDetalle.setAdapter(adapter);

        listaDetalle.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                Detalle detalle = listaDetalles.get(position);

                AlertDialog.Builder consultaQty = new android.support.v7.app.AlertDialog.Builder(getContext(), R.style.ThemeOverlay_AppCompat_Dialog_Alert);
                LinearLayout layout = new LinearLayout(getContext());
                consultaQty.setTitle("Cantidad");
                consultaQty.setMessage("Ingrese Cantidad");
                final EditText input = new EditText(getContext());
                input.setInputType(InputType.TYPE_CLASS_NUMBER);

                final EditText input2 = new EditText(getContext());
                input2.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL);
                layout.setOrientation(LinearLayout.VERTICAL);

                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT);
                input.setLayoutParams(lp);
                input2.setLayoutParams(lp);

                layout.addView(input);
                layout.addView(input2);
                consultaQty.setView(layout);
                input.setHint("Cantidad");
                input2.setHint("Valor (Blanco valor por lista "+detalle.valorUnitario+")");


                consultaQty.setPositiveButton("Listo", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String cantidad = input.getText().toString();
                        String valor = input2.getText().toString();
                        try{
                            int qty = Integer.parseInt(cantidad);


                            Detalle detalle = listaDetalles.get(position);
                            if(!valor.isEmpty()){

                                double val = Double.parseDouble(valor);
                                detalle.valorUnitario = val;

                            }
                            detalle.valorSubTotal = detalle.valorUnitario * qty;
                            detalle.cantidad = qty;
                            if (qty == 0){

                                listaDetalles.remove(detalle);
                            }
                            /*
                            Intent intent = new Intent();
                            intent.putExtra("item",detalle);
                            intent.putExtra("Eep","eep");
                            setResult(101,intent);
                            finish();
                            */

                        }catch (Exception e){
                            Log.e(TAG,"cantidad to qty "+e.toString());
                            /*
                            Intent intent = new Intent();
                            setResult(201,intent);
                            finish();
                            */
                            Toast.makeText(getContext(),"Debe ser numero entero",Toast.LENGTH_SHORT);
                        }
                    }
                });

                consultaQty.show();
            }
        });
        listaDetalle.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Log.i(TAG,"Borrar");

                listaDetalles.remove(position);
                mTransaction.setListaDetalles((ArrayList<Detalle>) listaDetalles);
                adapter.notifyDataSetChanged();
                Double totalDocumento = 0.0;

                for(Detalle dt: mTransaction.getListaDetalles()){

                    totalDocumento = totalDocumento+dt.valorSubTotal;
                }

                totalSoFar = totalDocumento;
                DecimalFormat df  = new DecimalFormat("#,##0.00");
                Toast.makeText(getActivity().getApplicationContext(),"Total: "+df.format(totalDocumento),Toast.LENGTH_LONG).show();
                return true;
            }
        });



        btnSearchDetalle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(),SeleccionarDetalleListaActivity.class);
                startActivityForResult(intent,101);
            }
        });
        btnAddDetalle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(),AgregarNuevoDetalleActivity.class);
                startActivityForResult(intent,101);
            }
        });

        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
            adapter.notifyDataSetChanged();
        }
    }



    protected void onRestart(){
        super.onResume();

        Log.i(TAG,"onResume");
        adapter.notifyDataSetChanged();
    }
    @Override
    public void onResume(){
        super.onResume();

        Log.i(TAG,"onResume");
        listaDetalles = mTransaction.getListaDetalles();
        adapter.notifyDataSetChanged();
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    static class DetalleResultViewHolder{
        TextView descripcionTV;
        TextView codigoTV;
        TextView valorTV;
        TextView subtotalDetalle;
    }

    class DetalleResultAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return listaDetalles.size();
        }

        @Override
        public Object getItem(int position) {
            return listaDetalles.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        DetalleResultViewHolder viewHolder;
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Detalle dh = listaDetalles.get(position);

            if (convertView == null){

                LayoutInflater inflater = getActivity().getLayoutInflater();
                convertView = inflater.inflate(R.layout.row_detalle,null,false);
                viewHolder = new DetalleResultViewHolder();

                viewHolder.descripcionTV = convertView.findViewById(R.id.descDetalle);
                viewHolder.codigoTV = convertView.findViewById(R.id.codDetalle);
                viewHolder.valorTV = convertView.findViewById(R.id.valorDetalle);
                viewHolder.subtotalDetalle = convertView.findViewById(R.id.subtotalDetalle);
                convertView.setTag(viewHolder);
            }else {
                viewHolder = (DetalleResultViewHolder)convertView.getTag();

            }

            //double sub = dh.cantidad * dh.valorUnitario;
            DecimalFormat df  = new DecimalFormat("#,##0.00");
            Log.d("NUX","Q es valorSubTotal " +dh.valorSubTotal + " "+ dh.valorSubTotal.getClass().toString());
            String subs = ""+df.format(dh.valorSubTotal);
            viewHolder.descripcionTV.setText(dh.descripcion);
            viewHolder.codigoTV.setText(dh.codigo);
            viewHolder.valorTV.setText(df.format(dh.valorUnitario) + " x "+dh.cantidad);
            viewHolder.subtotalDetalle.setText(subs);
            return convertView;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){

        Log.i(TAG,"RESPONSE !? " + requestCode + " "+resultCode);

        if(requestCode == 201){

            if(resultCode == 202){

                Log.i(TAG,"RESPONSE 201!? " + requestCode);
                Cliente cliente = (Cliente)data.getSerializableExtra("CLIENTE_REQ");
                mTransaction.setCliente(cliente);


            }
        }


        if(requestCode == 101){
            try {
                String st = data.getStringExtra("Eep");
                Detalle item = (Detalle) data.getSerializableExtra("item");
                Log.i(TAG,st);
                Log.i(TAG,item.descripcion);

                //revisar si existe

                boolean existe = false;
                Detalle detalleEnLista = null;

                for (Detalle dt : mTransaction.getListaDetalles()){

                    if(item.codigo.equalsIgnoreCase(dt.codigo) && item.descripcion.equalsIgnoreCase(dt.descripcion)){

                        existe = true;
                        detalleEnLista = dt;
                    }

                }
                if(existe){
                    detalleEnLista.cantidad = detalleEnLista.cantidad+item.cantidad;
                    detalleEnLista.valorSubTotal = detalleEnLista.valorUnitario * detalleEnLista.cantidad;
                }else{
                    mTransaction.getListaDetalles().add(item);
                }

                listaDetalles = mTransaction.getListaDetalles();

                Log.i(TAG,"Lista "+listaDetalles.size());
                adapter.notifyDataSetChanged();
                Double totalDocumento = 0.0;

                for(Detalle dt: mTransaction.getListaDetalles()){

                   totalDocumento = totalDocumento+dt.valorSubTotal;
                }

                totalSoFar = totalDocumento;
                DecimalFormat df  = new DecimalFormat("#,##0.00");
                Toast.makeText(getActivity().getApplicationContext(),"Total: "+df.format(totalDocumento),Toast.LENGTH_LONG).show();
            }catch (Exception e){

                Log.e(TAG,e.toString());
            }
        }

    }


}
