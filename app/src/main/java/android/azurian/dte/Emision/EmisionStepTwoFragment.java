package android.azurian.dte.Emision;

import android.app.DatePickerDialog;
import android.azurian.dte.R;
import android.azurian.dte.home.FoliosHelper;
import android.azurian.dte.home.HomeAltActivity;
import android.azurian.dte.services.NormalTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static android.content.Context.INPUT_METHOD_SERVICE;
import static android.content.Context.MODE_PRIVATE;



public class EmisionStepTwoFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "transaccion";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private NormalTransaction mTransaction;


    private OnFragmentInteractionListener mListener;

    public EmisionStepTwoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BlankfFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EmisionStepTwoFragment newInstance(String param1, String param2, NormalTransaction transaction) {
        EmisionStepTwoFragment fragment = new EmisionStepTwoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putSerializable(ARG_PARAM3,transaction);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            mTransaction = (NormalTransaction) getArguments().get(ARG_PARAM3);

        }
    }

    Button btnFacturacionFechaEmision;
    Button btnFacturacionFechaVencimiento;
    EditText inTxtFacturacionSerie;
    Spinner facturacionMoneaSelect;

    Switch facturacionDocGratuitoSwitch;

    Button btnFacturacionPasoTres;

    String fechaEmision="";
    String fechaVencimiento="";

    String selectedMoneda = "SOLES";
    EditText inTxtCorrelativo;
    final String TAG = getClass().getSimpleName();

    Spinner folioSpinner;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_factura_step_two, container, false);

        inTxtFacturacionSerie   = v.findViewById(R.id.inTxtFacturacionSerie);
        inTxtCorrelativo        = v.findViewById(R.id.inTxtCorrelativo);


        FoliosHelper foliosHelper = (FoliosHelper) ((HomeAltActivity)getActivity()).getFoliosHelper();

        Log.i(getClass().getName(),"Existe FoliosHelper " +foliosHelper);
        SharedPreferences sharedPreferences = getContext().getSharedPreferences("EMISION_PREFS",MODE_PRIVATE);

        folioSpinner = v.findViewById(R.id.folioSpinner);
        if(mParam1.equalsIgnoreCase("FACTURA")){

            /*
            String str = sharedPreferences.getString("FACTURA_SERIE_KEY","F001");
            inTxtFacturacionSerie.setText(str);


            */
            inTxtCorrelativo.setVisibility(View.GONE);
            folioSpinner.setAdapter(new ArrayAdapter<String>(getContext(),android.R.layout.simple_spinner_item,foliosHelper.getListaFoliosFacturas()));
        }
        if(mParam1.equalsIgnoreCase("BOLETA")){
           /*
            String str = sharedPreferences.getString("BOLETA_SERIE_KEY","B001");
            inTxtFacturacionSerie.setText(str);
            inTxtCorrelativo.setVisibility(View.GONE);
            */
            inTxtCorrelativo.setVisibility(View.GONE);
            folioSpinner.setAdapter(new ArrayAdapter<String>(getContext(),android.R.layout.simple_spinner_item,foliosHelper.getListaFoliosBoletas()));

        }


        if(mParam1.equalsIgnoreCase("NOTAC_BOLETA")){
            /*
            String str = sharedPreferences.getString("NOTAC_BOLETA_KEY","BC01");
            inTxtFacturacionSerie.setText(str);
            inTxtCorrelativo.setVisibility(View.GONE);
            */

            List<String> listaFiltrada = new ArrayList<String>();
            for(String s : foliosHelper.getListaFoliosNotasCredito()){
                if(s.startsWith("B")){
                    listaFiltrada.add(s);
                }
            }
            inTxtCorrelativo.setVisibility(View.GONE);
            folioSpinner.setAdapter(new ArrayAdapter<String>(getContext(),android.R.layout.simple_spinner_item,listaFiltrada));
        }
        if(mParam1.equalsIgnoreCase("NOTAC_FACTURA")){
            /*
            String str = sharedPreferences.getString("NOTAC_FACTURA_KEY","FC01");
            inTxtFacturacionSerie.setText(str);
            inTxtCorrelativo.setVisibility(View.GONE);
            */
            List<String> listaFiltrada = new ArrayList<String>();
            for(String s : foliosHelper.getListaFoliosNotasCredito()){
                if(s.startsWith("F")){
                    listaFiltrada.add(s);
                }
            }
            inTxtCorrelativo.setVisibility(View.GONE);
            folioSpinner.setAdapter(new ArrayAdapter<String>(getContext(),android.R.layout.simple_spinner_item,listaFiltrada));

        }


        if(mParam1.equalsIgnoreCase("NOTAD_BOLETA")){
            /*
            String str = sharedPreferences.getString("NOTAD_BOLETA_KEY","BD01");
            inTxtFacturacionSerie.setText(str);
            inTxtCorrelativo.setVisibility(View.GONE);
            */

            List<String> listaFiltrada = new ArrayList<String>();
            for(String s : foliosHelper.getListaFoliosNotasCredito()){
                if(s.startsWith("B")){
                    listaFiltrada.add(s);
                }
            }

            inTxtCorrelativo.setVisibility(View.GONE);
            folioSpinner.setAdapter(new ArrayAdapter<String>(getContext(),android.R.layout.simple_spinner_item,listaFiltrada));
        }

        if(mParam1.equalsIgnoreCase("NOTAD_FACTURA")){
            /*
            String str = sharedPreferences.getString("NOTAD_FACTURA_KEY","FD01");
            inTxtFacturacionSerie.setText(str);
            inTxtCorrelativo.setVisibility(View.GONE);
            */

            List<String> listaFiltrada = new ArrayList<String>();
            for(String s : foliosHelper.getListaFoliosNotasCredito()){
                if(s.startsWith("F")){
                    listaFiltrada.add(s);
                }
            }

            inTxtCorrelativo.setVisibility(View.GONE);
            folioSpinner.setAdapter(new ArrayAdapter<String>(getContext(),android.R.layout.simple_spinner_item,listaFiltrada));
        }


        Log.i(TAG,"PARAM1 -> "+mParam1);

        Log.i(TAG,""+mTransaction.getCliente().getRazonSocial());

        btnFacturacionPasoTres = v.findViewById(R.id.btnFacturacionPasoTres);
        btnFacturacionPasoTres.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try{

                    Log.i(TAG,"fe-> "+fechaEmision);
                    Log.i(TAG,"fv-> "+fechaVencimiento);
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    Date emiDate = sdf.parse(fechaEmision);
                    Date expDate = sdf.parse(fechaVencimiento);

                    Log.i(TAG,"fed-> "+emiDate);
                    Log.i(TAG,"fvd-> "+expDate);
                    if(expDate.before(emiDate)){
                        Log.i(TAG,"FECHA ES BEFORE");
                        Toast.makeText(getContext(),"Fecha vencimiento debe ser mayor a fecha emisión",Toast.LENGTH_SHORT).show();
                        return;
                    }else{
                        Log.i(TAG,"FECHA NO ES BEFORE");
                    }


                    mTransaction.setFechaEmision(fechaEmision);
                    mTransaction.setFechaVencimiento(fechaVencimiento);
                    //mTransaction.setSerie(inTxtFacturacionSerie.getText().toString());
                    mTransaction.setSerie(folioSpinner.getSelectedItem().toString());
                    mTransaction.setMoneda(selectedMoneda);
                    mTransaction.setDocumentoGratuito(facturacionDocGratuitoSwitch.isChecked());

                    //SharedPreferences sharedPreferences = getContext().getSharedPreferences("EMISION_PREFS",MODE_PRIVATE);
                    SharedPreferences.Editor editor = getContext().getSharedPreferences("EMISION_PREFS",MODE_PRIVATE).edit();
                    /*
                    * SharedPreferences sharedPreferences = getSharedPreferences("PRINTER_PREFS",MODE_PRIVATE);
                                String idPrinter = "";
                                idPrinter = sharedPreferences.getString("PRINTER_ID","");
                                String bmpUriStr = sharedPreferences.getString("LOGO_URI","");                  werasA
                    * */

                    if (mParam1.equalsIgnoreCase("FACTURA")){

                        String serie = inTxtFacturacionSerie.getText().toString();
                        editor.putString("FACTURA_SERIE_KEY",serie);
                        editor.commit();
                    }

                    if (mParam1.equalsIgnoreCase("BOLETA")){

                        String serie = inTxtFacturacionSerie.getText().toString();
                        editor.putString("BOLETA_SERIE_KEY",serie);
                        editor.commit();
                    }

                    if (mParam1.equalsIgnoreCase("NOTAC_BOLETA")){

                        String serie = inTxtFacturacionSerie.getText().toString();
                        editor.putString("NOTAC_BOLETA_KEY",serie);
                        editor.commit();
                    }

                    if (mParam1.equalsIgnoreCase("NOTAC_FACTURA")){

                        String serie = inTxtFacturacionSerie.getText().toString();
                        editor.putString("NOTAC_FACTURA_KEY",serie);
                        editor.commit();
                    }


                    if(mParam1.equalsIgnoreCase("NOTAD_BOLETA")){

                        String serie = inTxtFacturacionSerie.getText().toString();
                        editor.putString("NOTAD_BOLETA_KEY",serie);
                        editor.commit();
                    }

                    if(mParam1.equalsIgnoreCase("NOTAD_FACTURA")){

                        String serie = inTxtFacturacionSerie.getText().toString();
                        editor.putString("NOTAD_FACTURA_KEY",serie);
                        editor.commit();
                    }




                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.fragmentHolderEmision, EmisionStepThreeFragment.newInstance(mParam1,"",mTransaction));
                    transaction.addToBackStack("StepThree");
                    transaction.commit();


                }catch (Exception e){

                    Log.e(TAG,""+e.toString());
                }


            }
        });

        facturacionDocGratuitoSwitch = v.findViewById(R.id.facturacionDocGratuitoSwitch);




        facturacionMoneaSelect = v.findViewById(R.id.facturacionMoneaSelect);

        //final String[] monedas = {"SOLES","DÓLARES","EUROS"};
        final String[] monedas = {"SOLES"};
        facturacionMoneaSelect.setAdapter(new ArrayAdapter<String>(getContext(),android.R.layout.simple_spinner_item,monedas));
        facturacionMoneaSelect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedMoneda = monedas[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });






        final Calendar todayCalendar = Calendar.getInstance();
        todayCalendar.setTime(new Date());

        final Calendar futureCalendar = Calendar.getInstance();
        futureCalendar.setTime(new Date());
        futureCalendar.add(Calendar.MONTH,1);

        fechaVencimiento = ""+futureCalendar.get(Calendar.DAY_OF_MONTH)+"/"+(futureCalendar.get(Calendar.MONTH)+1)+"/"+futureCalendar.get(Calendar.YEAR);



        fechaEmision = ""+todayCalendar.get(Calendar.DAY_OF_MONTH)+"/"+(todayCalendar.get(Calendar.MONTH)+1)+"/"+todayCalendar.get(Calendar.YEAR);



        btnFacturacionFechaVencimiento = v.findViewById(R.id.btnFacturacionFechaVencimiento);
        btnFacturacionFechaVencimiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DatePickerDialog datePickerDialogInicio = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                        Toast.makeText(getContext(),
                                ""+dayOfMonth+"-"+(++month)+"-"+year, Toast.LENGTH_SHORT).show();
                        btnFacturacionFechaVencimiento.setText(""+dayOfMonth+"/"+(month)+"/"+year);
                        fechaVencimiento = dayOfMonth+"/"+month+"/"+year;
                    }
                }, todayCalendar.get(Calendar.YEAR), todayCalendar.get(Calendar.MONTH), todayCalendar.get(Calendar.DAY_OF_MONTH));

                datePickerDialogInicio.show();
                try{
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                }catch(Exception e){


                }
            }
        });


        btnFacturacionFechaEmision = v.findViewById(R.id.btnFacturacionFechaEmision);
        btnFacturacionFechaEmision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialogInicio = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                        Toast.makeText(getContext(),
                                ""+dayOfMonth+"/"+(++month)+"/"+year, Toast.LENGTH_SHORT).show();
                        btnFacturacionFechaEmision.setText(""+dayOfMonth+"/"+(month)+"/"+year);
                        fechaEmision = dayOfMonth+"/"+month+"/"+year;
                    }
                }, todayCalendar.get(Calendar.YEAR), todayCalendar.get(Calendar.MONTH), todayCalendar.get(Calendar.DAY_OF_MONTH));

                datePickerDialogInicio.show();
                try{
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                }catch(Exception e){


                }
            }
        });


        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
