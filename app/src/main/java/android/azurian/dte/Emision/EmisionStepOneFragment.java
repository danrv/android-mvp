package android.azurian.dte.Emision;

import android.azurian.dte.AgregarClienteActivity;
import android.azurian.dte.Persistencia.ClientesSQLOpenHelper;
import android.azurian.dte.R;
import android.azurian.dte.services.Cliente;
import android.azurian.dte.services.Dconfig;
import android.azurian.dte.services.NormalTransaction;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link EmisionStepOneFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link EmisionStepOneFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EmisionStepOneFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public EmisionStepOneFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EmisionStepOneFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EmisionStepOneFragment newInstance(String param1, String param2) {
        EmisionStepOneFragment fragment = new EmisionStepOneFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    ListView listaClientesParaFactura;

    List<Cliente> shallowList;

    ClienteResultAdapter adapter;
    EditText inTxtParamsEmisionF;

    final String TAG = getClass().getSimpleName();

    Button btnAddClienteEmision;

    Button btnSinCliente;

    ProgressBar loaderStepOne;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_factura_step_one, container, false);

        adapter = new ClienteResultAdapter();

        listaClientesParaFactura = v.findViewById(R.id.listaClientesParaFactura);

        btnSinCliente   = v.findViewById(R.id.btnSinCliente);
        loaderStepOne   = v.findViewById(R.id.loaderStepOne);


        if(mParam1.equalsIgnoreCase("BOLETA")){
            btnSinCliente.setVisibility(View.VISIBLE);
        }else {
            btnSinCliente.setVisibility(View.GONE);
        }

        inTxtParamsEmisionF = v.findViewById(R.id.inTxtParamsEmisionF);

        inTxtParamsEmisionF.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if(actionId == EditorInfo.IME_ACTION_DONE || actionId ==  EditorInfo.IME_ACTION_NEXT){

                    String param = v.getEditableText().toString();
                    ClientesSQLOpenHelper admin = new ClientesSQLOpenHelper(getActivity().getApplicationContext(),"clientes",null,Dconfig.dbVersion);
                    SQLiteDatabase bd = admin.getReadableDatabase();

                    Log.i(TAG,"BUSCAR "+param);
                    bd = admin.getReadableDatabase();

                    String query = "SELECT * FROM clientes WHERE (ruc LIKE '%"+param+"%' OR nombre LIKE '%"+param+"%')";
                    Log.i(TAG,"Q:"+query);
                    Cursor fila = bd.rawQuery(query,null);


                    shallowList = new ArrayList<>();

                    while (fila.moveToNext()){
                        Log.i(TAG,"F: "+fila.getString(0));
                        Cliente cli = new Cliente();
                        cli.setRuc(fila.getString(0));
                        cli.setRazonSocial(fila.getString(1));
                        cli.setDireccion(fila.getString(2));
                        cli.setContacto(fila.getString(3));
                        cli.setOtro(fila.getString(4));
                        cli.setFbkey(fila.getString(5));
                        cli.setTipoDocumento(fila.getString(6));

                        shallowList.add(cli);

                    }
                    bd.close();
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            adapter.notifyDataSetChanged();

                        }
                    });
                    bd.close();

                }

                return false;
            }
        });

        btnSinCliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NormalTransaction normalTransaction = new NormalTransaction();
                Cliente cls = new Cliente();

                cls.setTipoDocumento("1");
                cls.setRazonSocial("Cliente Generico");
                cls.setRuc("00000001");
                cls.setDireccion("-");

                normalTransaction.setCliente(cls);

                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.fragmentHolderEmision, EmisionStepTwoFragment.newInstance(mParam1,"",normalTransaction));
                transaction.addToBackStack("StepTwo");
                transaction.commit();
            }
        });

        listaClientesParaFactura.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                NormalTransaction normalTransaction = new NormalTransaction();

                normalTransaction.setCliente(shallowList.get(position));

                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.fragmentHolderEmision, EmisionStepTwoFragment.newInstance(mParam1,"",normalTransaction));
                transaction.addToBackStack("StepTwo");
                transaction.commit();
            }
        });
        shallowList = new ArrayList<Cliente>();

        listaClientesParaFactura.setAdapter(adapter);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                loadDataClientes();
            }
        });
        thread.run();


        btnAddClienteEmision = v.findViewById(R.id.btnAddClienteEmision);
        btnAddClienteEmision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(),AgregarClienteActivity.class);
                startActivityForResult(i,201);
            }
        });
        return v;
    }

    DatabaseReference myRef;
    ValueEventListener valueEventListener;
    private void loadDataClientes(){

        loaderStepOne.setVisibility(View.VISIBLE);
        final FirebaseDatabase database = FirebaseDatabase.getInstance();

         myRef = database.getReference("clientes/"+Dconfig.user);


        myRef.addValueEventListener(valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                try{

                    ClientesSQLOpenHelper admin = new ClientesSQLOpenHelper(getActivity().getApplicationContext(),"clientes",null, Dconfig.dbVersion);
                    SQLiteDatabase bd = admin.getWritableDatabase();

                    bd.execSQL("DELETE FROM clientes");

                    shallowList.clear();
                    Cliente cliente = null;
                    for (DataSnapshot clienteSnapshot : dataSnapshot.getChildren()){
                        cliente = clienteSnapshot.getValue(Cliente.class);
                        Log.i("Facutas Cliente->",cliente.getRazonSocial());
                        //shallowList.add(cliente);

                        ContentValues bdCliente = new ContentValues();
                        bdCliente.put("ruc",cliente.getRuc());
                        bdCliente.put("nombre",cliente.getRazonSocial());
                        bdCliente.put("direccion",cliente.getDireccion());
                        bdCliente.put("correo",cliente.getContacto());
                        bdCliente.put("otro",cliente.getOtro());
                        bdCliente.put("fbkey",clienteSnapshot.getKey());
                        bdCliente.put("tipoDocumento",cliente.getTipoDocumento());

                        bd.insert("clientes",null,bdCliente);
                    }

                    bd.close();
                    try{

                        bd = admin.getReadableDatabase();

                        String query = "SELECT * FROM clientes";
                        Log.i(TAG,"Q:"+query);
                        Cursor fila = bd.rawQuery(query,null);


                        while (fila.moveToNext()){
                            Log.i(TAG,"F: "+fila.getString(0));
                            Cliente cli = new Cliente();

                            cli.setRuc(fila.getString(0));
                            cli.setRazonSocial(fila.getString(1));
                            cli.setDireccion(fila.getString(2));
                            cli.setContacto(fila.getString(3));
                            cli.setOtro(fila.getString(4));
                            cli.setFbkey(fila.getString(5));
                            cli.setTipoDocumento(fila.getString(6));

                            shallowList.add(cli);
                        }
                        bd.close();

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                adapter.notifyDataSetChanged();
                                loaderStepOne.setVisibility(View.GONE);
                            }
                        });

                    }catch (Exception e){
                        Log.e(TAG,"A------- A"+e.toString());
                    }

                }catch (Exception e){
                    Log.e(TAG,"B------- B"+e.toString());
                    e.printStackTrace();
                    //loadDataClientes();
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                Log.i(TAG,""+databaseError.getMessage());
            }
        });

    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        if(valueEventListener != null){
            myRef.removeEventListener(valueEventListener);
        }

        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    static class ClienteResultViewHolder{
        TextView razonSocialTV;
        TextView rucTV;
        TextView contactoTV;
        TextView direccionTV;
    }

    class ClienteResultAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return shallowList.size();
        }

        @Override
        public Object getItem(int position) {
            return shallowList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        ClienteResultViewHolder viewHolder;
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            Cliente cliente = shallowList.get(position);

            if(convertView==null){
                LayoutInflater inflater = getActivity().getLayoutInflater();
                convertView = inflater.inflate(R.layout.row_cliente,null,false);
                viewHolder = new ClienteResultViewHolder();
                viewHolder.razonSocialTV = convertView.findViewById(R.id.lblRazonSocial);
                viewHolder.rucTV = convertView.findViewById(R.id.lblRuc);
                viewHolder.direccionTV = convertView.findViewById(R.id.lblDireccion);
                viewHolder.contactoTV = convertView.findViewById(R.id.lblContacto);
                convertView.setTag(viewHolder);
            }else{
                viewHolder = (ClienteResultViewHolder)convertView.getTag();
            }

            viewHolder.razonSocialTV.setText(cliente.getRazonSocial());
            viewHolder.rucTV.setText(cliente.getRuc());
            viewHolder.contactoTV.setText(cliente.getContacto());
            viewHolder.direccionTV.setText(cliente.getDireccion());

            return convertView;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){


        if(requestCode == 201){

            Log.i(TAG,"EEEEE"+requestCode);
            shallowList.clear();
            adapter.notifyDataSetChanged();

            loadDataClientes();

        }
    }
}
