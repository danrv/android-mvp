package android.azurian.dte.sdk;

/**
 * Created by danielrodriguez on 06-03-18.
 */

public class Other {

    public static byte[] byteArraysToBytes(byte[][] data) {

        int length = 0;
        for (int i = 0; i < data.length; i++)
            length += data[i].length;
        byte[] send = new byte[length];
        int k = 0;
        for (int i = 0; i < data.length; i++)
            for (int j = 0; j < data[i].length; j++)
                send[k++] = data[i][j];
        return send;
    }


}
