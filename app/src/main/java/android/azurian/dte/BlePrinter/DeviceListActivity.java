package android.azurian.dte.BlePrinter;

import android.azurian.dte.R;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class DeviceListActivity extends AppCompatActivity {



    private static final boolean DEBUG = true;

    // Return Intent extra
    public static String EXTRA_DEVICE_ADDRESS = "device_address";

    // Member fields
    private BluetoothAdapter mBtAdapter;
    private ArrayAdapter<String> mPairedDevicesArrayAdapter;
    private ArrayAdapter<String> mNewDevicesArrayAdapter;

    List<DeviceMatch> listaDevs;

    final String TAG = getClass().getSimpleName();

    ListView listaDevices;

    String selected;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_list);

        listaDevs = new ArrayList<>();

        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        this.registerReceiver(mReceiver, filter);

        // Register for broadcasts when discovery has finished


        // Get a set of currently paired devices

        try{

            mBtAdapter = BluetoothAdapter.getDefaultAdapter();
            Set<BluetoothDevice> pairedDevices = mBtAdapter.getBondedDevices();
            // Register for broadcasts when a device is discovered


            // If there are paired devices, add each one to the ArrayAdapter
            if (pairedDevices.size() > 0) {
                //findViewById(R.id.title_paired_devices).setVisibility(View.VISIBLE);
                for (BluetoothDevice device : pairedDevices) {
                    //mPairedDevicesArrayAdapter.add(device.getName() + "\n" + device.getAddress());
                    Log.i(TAG,device.getAddress());
                    //ImpConfig.printAddress = device.getAddress();
                    DeviceMatch match = new DeviceMatch();
                    match.nombre = device.getName();
                    match.address = device.getAddress();
                    listaDevs.add(match);
                }

            } else {
                // String noDevices = getResources().getText(R.string.none_paired).toString();
                // mPairedDevicesArrayAdapter.add(noDevices);
            }

            listaDevices = findViewById(R.id.listaDevices);
            listaDevices.setAdapter(new DeviceResultAdapter());

            listaDevices.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                    final int pos = position;
                    final String[] s = {"GPrinter","LESHP","BIXOLON","CBX 830B"};

                    final ArrayAdapter<String> adp = new ArrayAdapter<String>(DeviceListActivity.this,android.R.layout.simple_spinner_item,s);

                    final Spinner sp = new Spinner(DeviceListActivity.this);
                    sp.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    sp.setAdapter(adp);


                    sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            selected = s[position];
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                            selected = s[0];
                        }
                    });
                    AlertDialog.Builder builder = new AlertDialog.Builder(DeviceListActivity.this);
                    builder.setTitle("¿Cual es la marca de la impresora?");
                    builder.setView(sp);
                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            String seld = selected;
                            SharedPreferences.Editor editor = getSharedPreferences("PRINTER_PREFS",MODE_PRIVATE).edit();


                            Log.i(TAG,"Seleccion: "+seld);

                            editor.putString("PRINTER_ID",seld);

                            Log.i(TAG,"Debe guardar "+seld);

                            DeviceMatch match = listaDevs.get(pos);
                            //SharedPreferences.Editor editor = getSharedPreferences("PRINTER_PREFS",MODE_PRIVATE).edit();
                            editor.putString("PRINTER_ADDRESS",match.address);
                            editor.apply();
                            unregisterReceiver(mReceiver);
                            PrinterService ps = PrinterService.getInstance(getApplicationContext());
                            ps.initService();
                            finish();

                        }
                    });
                    builder.create().show();


                /*
                DeviceMatch match = listaDevs.get(position);
                SharedPreferences.Editor editor = getSharedPreferences("PRINTER_PREFS",MODE_PRIVATE).edit();
                editor.putString("PRINTER_ADDRESS",match.address);
                editor.apply();
                unregisterReceiver(mReceiver);
                PrinterService ps = PrinterService.getInstance(getApplicationContext());
                ps.initService();
                finish();
                */

                }
            });

        }catch (Exception e){

            Log.e(TAG,"ERROR @ "+e.getMessage());
            Toast.makeText(DeviceListActivity.this,"Error, por favor revisa que Bluetooth este activado y la impresora pareada",Toast.LENGTH_LONG).show();
        }
    }


    // The BroadcastReceiver that listens for discovered devices and
    // changes the title when discovery is finished
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            try{
                String action = intent.getAction();

                // When discovery finds a device
                if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                    // Get the BluetoothDevice object from the Intent
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    // If it's already paired, skip it, because it's been listed already
                    if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
                        mNewDevicesArrayAdapter.add(device.getName() + "\n" + device.getAddress());
                    }
                    // When discovery is finished, change the Activity title
                } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                    //setProgressBarIndeterminateVisibility(false);
                    //setTitle(R.string.select_device);
                    if (mNewDevicesArrayAdapter.getCount() == 0) {
                        //String noDevices = getResources().getText(R.string.none_found).toString();
                        //mNewDevicesArrayAdapter.add(noDevices);
                    }
                }
            }catch (Exception e){
                Log.e(TAG,e.toString());
            }

        }
    };


    static class DevicesViewHolder{
        TextView nombreDevice;
        TextView addressDevice;
    }

    class DeviceResultAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return listaDevs.size();
        }

        @Override
        public Object getItem(int position) {
            return listaDevs.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        DevicesViewHolder viewHolder;
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            DeviceMatch match = listaDevs.get(position);
            if(convertView == null){
                LayoutInflater inflater = getLayoutInflater();
                convertView = inflater.inflate(R.layout.row_device,null,false);
                viewHolder = new DevicesViewHolder();
                viewHolder.nombreDevice = convertView.findViewById(R.id.lblDeviceName);
                viewHolder.addressDevice = convertView.findViewById(R.id.lblDeviceAddress);
                convertView.setTag(viewHolder);
            }else{

                viewHolder = (DevicesViewHolder)convertView.getTag();
            }

            viewHolder.nombreDevice.setText(match.nombre);
            viewHolder.addressDevice.setText(match.address);

            return convertView;
        }
    }

    class DeviceMatch{
        String nombre;
        String address;
    }

}
