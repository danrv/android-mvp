package android.azurian.dte.BlePrinter;

import android.annotation.SuppressLint;
import android.azurian.dte.home.HomeAltActivity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by danielrodriguez on 12-03-18.
 */

public class PrinterService {

    final String TAG = getClass().getSimpleName();
    private static PrinterService instance = null;
    Context appContext;


    /******************************************************************************************************/
    // Message types sent from the BluetoothService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    public static final int MESSAGE_CONNECTION_LOST = 6;
    public static final int MESSAGE_UNABLE_CONNECT = 7;
    /*******************************************************************************************************/

    // Key names received from the BluetoothService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private static final int REQUEST_CHOSE_BMP = 3;
    private static final int REQUEST_CAMER = 4;

    //QRcode
    private static final int QR_WIDTH = 350;
    private static final int QR_HEIGHT = 350;
    /*******************************************************************************************************/
    private static final String CHINESE = "GBK";
    private static final String THAI = "CP874";
    private static final String KOREAN = "EUC-KR";
    private static final String BIG5 = "BIG5";



    // Name of the connected device
    private String mConnectedDeviceName = null;
    // Local Bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = null;
    // Member object for the services
    public BluetoothService mService = null;

    public boolean isADeviceConnected;

    final String[] items = { "复位打印机", "打印并走纸", "标准ASCII字体", "压缩ASCII字体", "正常大小",
            "二倍高倍宽", "三倍高倍宽", "四倍高倍宽", "取消加粗模式", "选择加粗模式", "取消倒置打印", "选择倒置打印", "取消黑白反显", "选择黑白反显",
            "取消顺时针旋转90°", "选择顺时针旋转90°", "走纸到切刀位置并切纸", "蜂鸣指令", "标准钱箱指令",
            "实时弹钱箱指令", "进入字符模式", "进入中文模式", "打印自检页", "禁止按键", "取消禁止按键" ,
            "设置汉字字符下划线", "取消汉字字符下划线", "进入十六进制模式" };
    final String[] itemsen = { "Print Init", "Print and Paper", "Standard ASCII font", "Compressed ASCII font", "Normal size",
            "Double high power wide", "Twice as high power wide", "Three times the high-powered wide", "Off emphasized mode", "Choose bold mode", "Cancel inverted Print", "Invert selection Print", "Cancel black and white reverse display", "Choose black and white reverse display",
            "Cancel rotated clockwise 90 °", "Select the clockwise rotation of 90 °", "Feed paper Cut", "Beep", "Standard CashBox",
            "Open CashBox", "Char Mode", "Chinese Mode", "Print SelfTest", "DisEnable Button", "Enable Button" ,
            "Set Underline", "Cancel Underline", "Hex Mode" };
    final byte[][] byteCommands = {
            { 0x1b, 0x40, 0x0a },// 复位打印机
            { 0x0a }, //打印并走纸
            { 0x1b, 0x4d, 0x00 },// 标准ASCII字体
            { 0x1b, 0x4d, 0x01 },// 压缩ASCII字体
            { 0x1d, 0x21, 0x00 },// 字体不放大
            { 0x1d, 0x21, 0x11 },// 宽高加倍
            { 0x1d, 0x21, 0x22 },// 宽高加倍
            { 0x1d, 0x21, 0x33 },// 宽高加倍
            { 0x1b, 0x45, 0x00 },// 取消加粗模式
            { 0x1b, 0x45, 0x01 },// 选择加粗模式
            { 0x1b, 0x7b, 0x00 },// 取消倒置打印
            { 0x1b, 0x7b, 0x01 },// 选择倒置打印
            { 0x1d, 0x42, 0x00 },// 取消黑白反显
            { 0x1d, 0x42, 0x01 },// 选择黑白反显
            { 0x1b, 0x56, 0x00 },// 取消顺时针旋转90°
            { 0x1b, 0x56, 0x01 },// 选择顺时针旋转90°
            { 0x0a, 0x1d, 0x56, 0x42, 0x01, 0x0a },//切刀指令
            { 0x1b, 0x42, 0x03, 0x03 },//蜂鸣指令
            { 0x1b, 0x70, 0x00, 0x50, 0x50 },//钱箱指令
            { 0x10, 0x14, 0x00, 0x05, 0x05 },//实时弹钱箱指令
            { 0x1c, 0x2e },// 进入字符模式
            { 0x1c, 0x26 }, //进入中文模式
            { 0x1f, 0x11, 0x04 }, //打印自检页
            { 0x1b, 0x63, 0x35, 0x01 }, //禁止按键
            { 0x1b, 0x63, 0x35, 0x00 }, //取消禁止按键
            { 0x1b, 0x2d, 0x02, 0x1c, 0x2d, 0x02 }, //设置下划线
            { 0x1b, 0x2d, 0x00, 0x1c, 0x2d, 0x00 }, //取消下划线
            { 0x1f, 0x11, 0x03 }, //打印机进入16进制模式
    };
    /***************************条                          码***************************************************************/
    final String[] codebar = { "UPC_A", "UPC_E", "JAN13(EAN13)", "JAN8(EAN8)",
            "CODE39", "ITF", "CODABAR", "CODE93", "CODE128", "QR Code" };
    final byte[][] byteCodebar = {
            { 0x1b, 0x40 },// 复位打印机
            { 0x1b, 0x40 },// 复位打印机
            { 0x1b, 0x40 },// 复位打印机
            { 0x1b, 0x40 },// 复位打印机
            { 0x1b, 0x40 },// 复位打印机
            { 0x1b, 0x40 },// 复位打印机
            { 0x1b, 0x40 },// 复位打印机
            { 0x1b, 0x40 },// 复位打印机
            { 0x1b, 0x40 },// 复位打印机
            { 0x1b, 0x40 },// 复位打印机
    };





    protected PrinterService(Context context) {
        appContext = context;
    }

    public static PrinterService getInstance(Context context){

        if (instance == null){
            instance = new PrinterService(context);
            instance.isADeviceConnected = false;
        }
        return instance;

    }

    public static PrinterService getInstance(){


        return instance;

    }


    public void initService(){

        try{

            Log.i(TAG,"InitService");
            SharedPreferences sharedPreferences =  appContext.getSharedPreferences("PRINTER_PREFS",MODE_PRIVATE);
            String address = sharedPreferences.getString("PRINTER_ADDRESS",null);
            //Log.i(TAG,address);


            Thread th = new Thread(new Runnable() {
                @Override
                public void run() {

                    // Get local Bluetooth adapter
                    mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();


                    Log.i(TAG,"onStart");
                    // If Bluetooth is not on, request that it be enabled.
                    // setupChat() will then be called during onActivityResult
                    if (!mBluetoothAdapter.isEnabled()) {
                        Intent enableIntent = new Intent(
                                BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        ((HomeAltActivity) appContext).startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
                        // Otherwise, setup the session
                        Log.i(TAG,"didStartActivityForResult");
                    } else {
                        if (mService == null) {
                            //KeyListenerInit();//监听
                            Log.i(TAG, "mService is nULL, new BluetoothService");

                            mService = new BluetoothService(appContext, mHandler);

                        }
                    }

                    if(1==2){
                        Log.i(TAG,"ImpConfig not setted");
                    }else{
                        SharedPreferences sharedPreferences = appContext.getSharedPreferences("PRINTER_PREFS",MODE_PRIVATE);
                        String address = sharedPreferences.getString("PRINTER_ADDRESS",null);
                        Log.i("PRINTER",address);
                        if(address != null){
                            BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
                            mService.connect(device);
                            Log.i(TAG,"connect to peripheral");
                        }else{
                            Log.w(TAG,"NO ADDRESS");
                        }

                    }



                    if (mBluetoothAdapter == null) {
                        Toast.makeText(appContext, "Bluetooth is not available",
                                Toast.LENGTH_LONG).show();
                        //finish();
                    }else{
                        Log.i(TAG,"OK..now?");
                    }

                }
            });


            th.run();

        }catch (Exception e){

            Log.e(TAG,e.toString());


        }


    }


    /****************************************************************************************************/
    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Log.i(TAG,"handleMessage " +msg.toString() + " " +msg.arg1);
            switch (msg.what) {
                case MESSAGE_STATE_CHANGE:
                    if (true)
                        Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                    switch (msg.arg1) {
                        case BluetoothService.STATE_CONNECTED:
                            Log.i(TAG,"STATE_CONNECTED");
                            break;
                        case BluetoothService.STATE_CONNECTING:
                            // mTitle.setText(R.string.title_connecting);
                            Log.i(TAG,"STATE_CONNECTING");
                            break;
                        case BluetoothService.STATE_LISTEN:
                        case BluetoothService.STATE_NONE:
                            //mTitle.setText(R.string.title_not_connected);
                            break;
                    }
                    break;
                case MESSAGE_WRITE:

                    break;
                case MESSAGE_READ:

                    break;
                case MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                    Toast.makeText(appContext,
                            "Connected to " + mConnectedDeviceName,
                            Toast.LENGTH_SHORT).show();
                            instance.isADeviceConnected = true;
                    break;
                case MESSAGE_TOAST:
                    Toast.makeText(appContext,
                            msg.getData().getString(TOAST), Toast.LENGTH_SHORT)
                            .show();
                    break;
                case MESSAGE_CONNECTION_LOST:    //蓝牙已断开连接
                    Toast.makeText(appContext, "Device connection was lost",
                            Toast.LENGTH_SHORT).show();
                instance.isADeviceConnected = false;

                    break;
                case MESSAGE_UNABLE_CONNECT:     //无法连接设备
                    Toast.makeText(appContext, "Unable to connect device",
                            Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

}
