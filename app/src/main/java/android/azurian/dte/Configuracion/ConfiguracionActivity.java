package android.azurian.dte.Configuracion;

import android.Manifest;
import android.azurian.dte.R;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;

public class ConfiguracionActivity extends AppCompatActivity {

    final String TAG = getClass().getSimpleName();


    Button      btnSelImage;
    ImageView   imgLogoWall;
    Button      btnConfProductos;
    EditText    inTxtLocalAddress;
    Button      btnSaveLocalAddress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracion);


        btnSelImage         = findViewById(R.id.btnSelImage);
        imgLogoWall         = findViewById(R.id.imgLogoWall);
        btnConfProductos    = findViewById(R.id.btnConfProductos);
        inTxtLocalAddress   = findViewById(R.id.inTxtLocalAddress);
        btnSaveLocalAddress = findViewById(R.id.btnSaveLocalAddress);


        final SharedPreferences sharedPreferences = getSharedPreferences("PRINTER_PREFS",MODE_PRIVATE);

        String savedAddress = sharedPreferences.getString("LOCAL_ADDRESS",null);

        if(savedAddress != null){
            inTxtLocalAddress.setText(savedAddress);
        }

        String logoPrinter = "";
        logoPrinter = sharedPreferences.getString("LOGO_URI","empty");
        if (logoPrinter.equalsIgnoreCase("empty")){
            imgLogoWall.setImageResource(R.drawable.dtelogo);
        }else{
            Uri bmpUri = Uri.parse(logoPrinter);
            imgLogoWall.setImageURI(bmpUri);
        }

        btnSelImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                i.setType("image/*");
                startActivityForResult(i,42);
            }
        });

        btnConfProductos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ConfiguracionActivity.this,ConProductosActivity.class);
                startActivity(i);
            }
        });


        btnSaveLocalAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String toSaveLocalAddress = inTxtLocalAddress.getText().toString();

                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("LOCAL_ADDRESS",toSaveLocalAddress);
                editor.commit();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        Log.i(TAG,"onActivityResutl "+ requestCode +" "+resultCode +" ");
        if(requestCode == 42 && resultCode !=0){

            Uri selectedImageUri = data.getData();
            if(Build.VERSION.SDK_INT>22){
                requestPermissions(new String[] {Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            }
            Log.i(TAG,"VIA " +selectedImageUri);

            imgLogoWall.setImageURI(selectedImageUri);

            try{
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri);

                Log.i(TAG,"BITMAP "+bitmap.getWidth() + " "+bitmap.getHeight() +" ");

                if(bitmap.getWidth() != 500 && bitmap.getHeight() !=240){
                    Toast.makeText(getApplicationContext(),"La imagen debe ser de 500 x 240",Toast.LENGTH_LONG).show();
                }else{
                    Log.i(TAG,"Image Ok");
                    SharedPreferences.Editor editor = getSharedPreferences("PRINTER_PREFS",MODE_PRIVATE).edit();
                    editor.putString("LOGO_URI",selectedImageUri.toString());
                    editor.commit();
                }

                //SharedPreferences.Editor editor = getSharedPreferences("PRINTER_PREFS",MODE_PRIVATE).edit();

            }catch (Exception e){

            }

            /*
            Drawable drawable =  imgLogoWall.getDrawable();
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();

            Log.i(TAG,"BMP "+bitmap.getWidth() + " " + bitmap.getHeight());
            if(bitmap.getWidth() != 400 || bitmap.getHeight() != 240){

            }
            */

        }
    }
}
