package android.azurian.dte.Configuracion;

import android.azurian.dte.R;
import android.azurian.dte.services.Dconfig;
import android.azurian.dte.services.Detalle;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class AgregarDetalleConfActivity extends AppCompatActivity {

    EditText inTxtDesc;
    EditText inTxtCodigo;
    Button btnLeerCodigoC;
    Spinner spinnerTipoOperacion;
    EditText inTxtValorUnitario;
    Spinner spinnerTipoValorC;
    Button btnAddEditC;
    boolean editando;


    //variables
    String selectedIVG; //tipo de operacion
    String tipoUnidadSelected; //tipo de unidad

    final String TAG = getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_detalle_conf);

        editando = false;

        inTxtDesc               = findViewById(R.id.inTxtDesc);
        inTxtCodigo             = findViewById(R.id.inTxtCodigo);
        btnLeerCodigoC          = findViewById(R.id.btnLeerCodigoC);
        spinnerTipoOperacion    = findViewById(R.id.spinnerTipoOperacion);
        inTxtValorUnitario      = findViewById(R.id.inTxtValorUnitario);
        spinnerTipoValorC       = findViewById(R.id.spinnerTipoValorC);
        btnAddEditC             = findViewById(R.id.btnAddEditC);

        final String fid = getIntent().getStringExtra("fbkey");
        if(fid !=null){
            editando = true;
        }
        Log.i(TAG,"Editando Dealle? " +editando);


        // setear spinner con tipo de operacion
        final String[] tiposIVG = {"Gravado - Operación Onerosa"};

        spinnerTipoOperacion.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,tiposIVG));
        spinnerTipoOperacion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedIVG = tiposIVG[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // setear spinner con tipo unidades
        //spinnerTipoValorC.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,R.array));
        spinnerTipoValorC.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String tipoUnidad[] = getResources().getStringArray(R.array.unidades);

                String tipo = tipoUnidad[position];

                Log.i(TAG,"TU: " +tipo );
                tipoUnidadSelected = tipo;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        //Accion para leer codigo
        btnLeerCodigoC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new IntentIntegrator(AgregarDetalleConfActivity.this).initiateScan();
            }
        });


        if(editando){

            try{
                Detalle detalle = (Detalle) getIntent().getSerializableExtra("detalle");
                inTxtDesc.setText(detalle.descripcion);
                inTxtCodigo.setText(detalle.codigo);
                inTxtValorUnitario.setText(""+detalle.valorUnitario);

                Log.i(TAG,"ED TU "+detalle.tipoUnidad);

                spinnerTipoOperacion.setSelection(0,true);

                if (detalle.tipoUnidad.equalsIgnoreCase("Unidades")){
                    spinnerTipoValorC.setSelection(0,true);
                }

                if (detalle.tipoUnidad.equalsIgnoreCase("Kilogramos")){
                    spinnerTipoValorC.setSelection(1,true);
                }

                if (detalle.tipoUnidad.equalsIgnoreCase("Metros")){
                    spinnerTipoValorC.setSelection(2,true);
                }

                if (detalle.tipoUnidad.equalsIgnoreCase("centimetros cuadrados")){
                    spinnerTipoValorC.setSelection(3,true);
                }

                if (detalle.tipoUnidad.equalsIgnoreCase("centimetros cubicos")){
                    spinnerTipoValorC.setSelection(4,true);
                }
            }catch (Exception e){
                Log.e(TAG,"ERROR "+e.toString());

            }


        }
        //Accion boton guardar detalle

        btnAddEditC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (editando){

                    Detalle detalle = new Detalle();

                    detalle.descripcion = inTxtDesc.getText().toString();
                    detalle.codigo = inTxtCodigo.getText().toString();
                    detalle.tipoIGV = selectedIVG;
                    detalle.valorUnitario = Double.parseDouble(inTxtValorUnitario.getText().toString());// Long.parseLong(inTxtDetalleVU.getText().toString());

                    detalle.tipoUnidad = tipoUnidadSelected;

                    FirebaseDatabase database = FirebaseDatabase.getInstance();
                    final DatabaseReference myRef = database.getReference(Dconfig.ruc+"/items/"+fid);
                    myRef.setValue(detalle);
                    setResult(302);

                    finish();

                }else {
                    Detalle detalle = new Detalle();

                    detalle.descripcion = inTxtDesc.getText().toString();
                    detalle.codigo = inTxtCodigo.getText().toString();
                    detalle.tipoIGV = selectedIVG;
                    detalle.valorUnitario = Double.parseDouble(inTxtValorUnitario.getText().toString());// Long.parseLong(inTxtDetalleVU.getText().toString());

                    detalle.tipoUnidad = tipoUnidadSelected;

                    FirebaseDatabase database = FirebaseDatabase.getInstance();
                    final DatabaseReference myRef = database.getReference(Dconfig.ruc+"/items");

                    myRef.push().setValue(detalle);

                    setResult(201);
                    finish();
                }

            }
        });

        //si editando cambiar texto boton
        btnAddEditC.setText("GUARDAR");
    }



    //OVERRIDE PARA RESULT
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        Log.i(TAG,"REQC: "+requestCode +" RSLTC: "+resultCode);
        if (requestCode == 49374 && resultCode == -1){
            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            inTxtCodigo.setText(result.getContents());
        }
    }
}
