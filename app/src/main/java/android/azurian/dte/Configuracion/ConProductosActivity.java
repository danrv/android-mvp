package android.azurian.dte.Configuracion;

import android.azurian.dte.Persistencia.DetallesSQLOpenHelper;
import android.azurian.dte.services.Dconfig;
import android.azurian.dte.services.Detalle;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.azurian.dte.R;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class ConProductosActivity extends AppCompatActivity {

    final String TAG = getClass().getSimpleName();

    Button btnNuevoProductoCnf;
    Button btnLeerCodSrch;
    EditText inTxtCodSrchC;
    ListView listaProdsC;
    List<Detalle> listaDetalles;
    DetalleResultAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_con_productos);

        btnNuevoProductoCnf         = findViewById(R.id.btnNuevoProductoCnf);
        inTxtCodSrchC               = findViewById(R.id.inTxtCodSrchC);
        btnLeerCodSrch              = findViewById(R.id.btnLeerCodSrch);
        listaProdsC                 = findViewById(R.id.listaProdsC);

        // boton k/done/check para el teclado
        inTxtCodSrchC.setImeOptions(EditorInfo.IME_ACTION_DONE);

        // accion boton agregar nuevo
        btnNuevoProductoCnf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(ConProductosActivity.this,AgregarDetalleConfActivity.class);
                startActivityForResult(i,200);
            }
        });

        //accion teclado buscar
        inTxtCodSrchC.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_DONE){
                    Log.i(TAG,"Buscar "+inTxtCodSrchC.getText().toString());
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(inTxtCodSrchC.getWindowToken(), 0);
                    buscarContenido(inTxtCodSrchC.getText().toString());
                    return true;
                }
                return false;
            }
        });

        //accion boton leer codigo
        btnLeerCodSrch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new IntentIntegrator(ConProductosActivity.this).initiateScan();
            }
        });

        //set adapter para lista productos
        listaDetalles = new ArrayList<Detalle>();
        adapter = new DetalleResultAdapter();

        listaProdsC.setAdapter(adapter);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                loadDataItems();
            }
        });

        thread.start();

        listaProdsC.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Detalle detalle = listaDetalles.get(position);

                Log.i(TAG,"ClickOn " +detalle.fbKey + " TU "+detalle.tipoUnidad);
                Intent intent = new Intent(ConProductosActivity.this,AgregarDetalleConfActivity.class);
                intent.putExtra("fbkey",detalle.fbKey);
                intent.putExtra("detalle",detalle);

                startActivityForResult(intent,301);
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        Log.i(TAG,"REQC: "+requestCode +" RSLTC: "+resultCode);
        if(requestCode == 200 && resultCode  == 201){
            //agredado nuevo, debe recargar
        }

        if (requestCode == 49374 && resultCode == -1){
            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            inTxtCodSrchC.setText(result.getContents());
        }

    }

    //metodo para buscar

    private void buscarContenido(String p){

        String param = p;
        DetallesSQLOpenHelper admin = new DetallesSQLOpenHelper(getApplicationContext(),"detalles",null,Dconfig.dbVersion);

        SQLiteDatabase bd = admin.getReadableDatabase();

        String query = "SELECT * FROM detalles WHERE (descripcion LIKE '%"+param+"%' OR codigo LIKE '%"+param+"%')";
        Log.i(TAG,"Q:"+query);
        Cursor fila = bd.rawQuery(query,null);

        listaDetalles.clear();

        while (fila.moveToNext()){

            Detalle detalle = new Detalle();
            detalle.descripcion = fila.getString(0);
            detalle.codigo = fila.getString(1);
            detalle.tipoIGV = fila.getString(2);
            detalle.valorUnitario = fila.getDouble(3);
            detalle.tipoUnidad = fila.getString(6);
            detalle.fbKey = fila.getString(7);
            listaDetalles.add(detalle);
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
            }
        });

    }

    // metodo para obtener de FireBase
    private void loadDataItems(){
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference(Dconfig.ruc+"/items");
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                DetallesSQLOpenHelper admin = new DetallesSQLOpenHelper(getApplicationContext(),"detalles",null,Dconfig.dbVersion);

                SQLiteDatabase bd = admin.getWritableDatabase();

                try{
                    bd.execSQL("DELETE FROM detalles");

                    for(DataSnapshot item : dataSnapshot.getChildren()){
                        Detalle detalle = item.getValue(Detalle.class);
                        Log.i(TAG,detalle.descripcion);
                        //listaDetalles.add(detalle);
                        String fbKey = item.getKey();
                        ContentValues bdDetalle = new ContentValues();

                        bdDetalle.put("descripcion",detalle.descripcion);
                        bdDetalle.put("codigo",detalle.codigo);
                        bdDetalle.put("tipoIGV",detalle.tipoIGV);
                        bdDetalle.put("valorUnitario",detalle.valorUnitario);
                        bdDetalle.put("fbKey",item.getKey());
                        bdDetalle.put("tipoUnidad",detalle.tipoUnidad);
                        bd.insert("detalles",null,bdDetalle);

                    }
                }catch (Exception e){
                    Log.e(TAG,"ERROR @ "+e.getMessage());
                }
                finally {
                    bd.close();
                }





                try{

                    listaDetalles.clear();

                    bd = admin.getReadableDatabase();

                    String query = "SELECT * FROM detalles";
                    Log.i(TAG,"Q:"+query);
                    Cursor fila = bd.rawQuery(query,null);

                    while (fila.moveToNext()){

                        Detalle detalle = new Detalle();
                        detalle.descripcion = fila.getString(0);
                        detalle.codigo = fila.getString(1);
                        detalle.tipoIGV = fila.getString(2);
                        detalle.valorUnitario = fila.getDouble(3);
                        detalle.tipoUnidad = fila.getString(6);
                        detalle.fbKey = fila.getString(7);
                        listaDetalles.add(detalle);
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                        }
                    });

                }catch (Exception e){
                    Log.i(TAG,e.toString());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                Log.e(TAG,"ERROR: "+databaseError.getDetails());
            }
        });

    }

    //clases para listView

    static class DetalleResultViewHolder{
        TextView descripcionTV;
        TextView codigoTV;
        TextView valorTV;
    }

    class DetalleResultAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return listaDetalles.size();
        }

        @Override
        public Object getItem(int position) {
            return listaDetalles.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        DetalleResultViewHolder viewHolder;
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Detalle dh = listaDetalles.get(position);

            if (convertView == null){

                LayoutInflater inflater = getLayoutInflater();
                convertView = inflater.inflate(R.layout.row_detalle,null,false);
                viewHolder = new DetalleResultViewHolder();

                viewHolder.descripcionTV = convertView.findViewById(R.id.descDetalle);
                viewHolder.codigoTV = convertView.findViewById(R.id.codDetalle);
                viewHolder.valorTV = convertView.findViewById(R.id.valorDetalle);
                convertView.setTag(viewHolder);
            }else {
                viewHolder = (DetalleResultViewHolder)convertView.getTag();

            }

            DecimalFormat df = new DecimalFormat("0.00");
            viewHolder.descripcionTV.setText(dh.descripcion);
            viewHolder.codigoTV.setText(dh.codigo);
            viewHolder.valorTV.setText("S/ "+df.format(dh.valorUnitario));

            return convertView;
        }
    }


}
