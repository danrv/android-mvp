package android.azurian.dte.services;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by danielrodriguez on 13-04-18.
 */

public class ObtenerInfoDoc {

    final String TAG = getClass().getSimpleName();

    public JSONObject consultar(JSONObject params){

        try{

            String surl = Dconfig.Resturl+"obtenerInfoDoc";
            Log.i(TAG,"consultar -> "+surl);
            Log.i(TAG,"REST-> " + params.toString());

            System.setProperty("http.keepAlive", "false");
            HttpURLConnection connection;
            URL url = new URL(surl);
            connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type","application/json; charset=UTF-8");
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setConnectTimeout(10000);
            connection.setChunkedStreamingMode(0);

            byte[] bytes = params.toString().getBytes("UTF-8");
            OutputStream os = connection.getOutputStream();

            os.write(bytes);
            connection.connect();

            Log.i(TAG,"Get Response Code: "+connection.getResponseCode());

            if(connection.getResponseCode() == HttpURLConnection.HTTP_OK){
                StringBuilder result = new StringBuilder();
                Log.i(TAG,"Go with JSON");
                InputStream in = new BufferedInputStream(connection.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }

                connection.disconnect();
                Log.i(TAG,"-> "+result.toString());

                JSONObject jsonRespone = new JSONObject(result.toString());

                return jsonRespone;
            }else{
                Log.i(TAG,"NO! Nope! Niet!");
                return null;

            }


        }catch (Exception e){

            Log.e(TAG,"ERROR "+e.toString());
            return null;
        }

    }
}
