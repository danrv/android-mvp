package android.azurian.dte.services;

import java.io.Serializable;

/**
 * Created by danielrodriguez on 26-02-18.
 */

public class Cliente implements Serializable{
    private String razonSocial;
    private String ruc;
    private String direccion;
    private String contacto;
    private String otro;
    private String tipoDocumento;
    private String fbkey;

    public String getFbkey() {
        return fbkey;
    }

    public void setFbkey(String fbKey) {
        this.fbkey = fbKey;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public Cliente() {
    }

    public Cliente(String razonSocial, String ruc, String direccion, String contacto, String otro,String tipoDocumento) {
        this.razonSocial = razonSocial;
        this.ruc = ruc;
        this.direccion = direccion;
        this.contacto = contacto;
        this.otro = otro;
        this.tipoDocumento = tipoDocumento;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public String getOtro() {
        return otro;
    }

    public void setOtro(String otro) {
        this.otro = otro;
    }
}
