package android.azurian.dte.services;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by danielrodriguez on 12-04-18.
 */

public class RestConsltarLoteDoc {

    final String TAG = getClass().getSimpleName();

    public class ReqParams{

        public ReqParams(){

        }

        String  apiKey;
        Long    correlativoFinal;
        Long    correlativoInicial;
        int     estadoSunat;
        String  fechaEmisionFinal;
        String  fechaEmisionInicial;
        String  rucEmisor;
        String  serie;
        String  tipoDocumento;

        public String getApiKey() {
            return apiKey;
        }

        public void setApiKey(String apiKey) {
            this.apiKey = apiKey;
        }

        public Long getCorrelativoFinal() {
            return correlativoFinal;
        }

        public void setCorrelativoFinal(Long correlativoFinal) {
            this.correlativoFinal = correlativoFinal;
        }

        public Long getCorrelativoInicial() {
            return correlativoInicial;
        }

        public void setCorrelativoInicial(Long correlativoInicial) {
            this.correlativoInicial = correlativoInicial;
        }

        public int getEstadoSunat() {
            return estadoSunat;
        }

        public void setEstadoSunat(int estadoSunat) {
            this.estadoSunat = estadoSunat;
        }

        public String getFechaEmisionFinal() {
            return fechaEmisionFinal;
        }

        public void setFechaEmisionFinal(String fechaEmisionFinal) {
            this.fechaEmisionFinal = fechaEmisionFinal;
        }

        public String getFechaEmisionInicial() {
            return fechaEmisionInicial;
        }

        public void setFechaEmisionInicial(String fechaEmisionInicial) {
            this.fechaEmisionInicial = fechaEmisionInicial;
        }

        public String getRucEmisor() {
            return rucEmisor;
        }

        public void setRucEmisor(String rucEmisor) {
            this.rucEmisor = rucEmisor;
        }

        public String getSerie() {
            return serie;
        }

        public void setSerie(String serie) {
            this.serie = serie;
        }

        public String getTipoDocumento() {
            return tipoDocumento;
        }

        public void setTipoDocumento(String tipoDocumento) {
            this.tipoDocumento = tipoDocumento;
        }
    }


    public ArrayList<DocumentHelper> consultar(ReqParams params){

        try{

            String surl = Dconfig.Resturl+"consultarLoteDoc";

            Log.i(TAG,"consultar -> "+surl);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("apiKey",params.apiKey);
            jsonObject.put("correlativoFinal",params.correlativoFinal);
            jsonObject.put("correlativoInicial",params.correlativoInicial);
            jsonObject.put("estadoSunat",params.estadoSunat);
            jsonObject.put("fechaEmisionFinal",params.fechaEmisionFinal);
            jsonObject.put("fechaEmisionInicial",params.fechaEmisionInicial);
            jsonObject.put("rucEmisor",params.rucEmisor);
            jsonObject.put("serie",params.serie);
            jsonObject.put("tipoDocumento",params.tipoDocumento);


            Log.i(TAG,"REST-> " + jsonObject.toString());

            System.setProperty("http.keepAlive", "false");
            HttpURLConnection connection;
            URL url = new URL(surl);
            connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type","application/json; charset=UTF-8");
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setConnectTimeout(10000);
            connection.setChunkedStreamingMode(0);

            byte[] bytes = jsonObject.toString().getBytes("UTF-8");
            OutputStream os = connection.getOutputStream();

            os.write(bytes);
            connection.connect();


            Log.i(TAG,"Get Response Code: "+connection.getResponseCode());
            if(connection.getResponseCode() == HttpURLConnection.HTTP_OK){
                StringBuilder result = new StringBuilder();
                Log.i(TAG,"Go with JSON");
                InputStream in = new BufferedInputStream(connection.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }

                connection.disconnect();
                Log.i(TAG,"-> "+result.toString());

                JSONObject jsonRespone = new JSONObject(result.toString());
                JSONArray listaDocumentos = jsonRespone.getJSONArray("item");
                Log.i(TAG,"listaJson: "+listaDocumentos.length());

                final ArrayList<DocumentHelper> listaDocumentosHelper = new ArrayList<>();

                for(int i=0;i<listaDocumentos.length();i++){
                    JSONObject objc = (JSONObject) listaDocumentos.get(i);
                    Log.i(TAG,"--->"+objc.getString("tipoDocumento"));
                    DocumentHelper dh = new DocumentHelper();
                    dh.rucEmisor = objc.getString("rucEmisor");
                    dh.tipoDocumento = objc.getString("tipoDocumento");
                    dh.serie = objc.getString("serie");
                    dh.correlativo = objc.getLong("correlativo");
                    dh.linkPDF = objc.getString("linkPDF");
                    dh.linkXML = objc.getString("linkXML");
                    dh.estadoDocAzurian = objc.getString("estadoDocAzurian");
                    dh.descripcionSunat = objc.getString("descripcionSunat");

                    dh.tipoDocumentoStr = ""+dh.tipoDocumentoStr;
                    dh.correlativoStr = ""+dh.correlativo;
                    listaDocumentosHelper.add(dh);
                }
                return listaDocumentosHelper;
            }else{
                Log.i(TAG,"NO! Nope! Niet!");
                return null;

            }


        }catch (Exception e){
            Log.e(TAG,"Error "+e.toString());
            return null;
        }


    }
}
