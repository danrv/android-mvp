package android.azurian.dte.services;

//----------------------------------------------------
//
// Generated by www.easywsdl.com
// Version: 5.2.8.3
//
// Created by Quasar Development 
//
//---------------------------------------------------


import java.util.Hashtable;
import org.ksoap2.serialization.*;

public class DConsultarLoteDocRequest extends AttributeContainer implements KvmSerializable
{

    
    public String apiKey;
    
    public Long correlativoFinal=0L;
    
    public Long correlativoInicial=0L;
    
    public Integer estadoSunat=0;
    
    public String fechaEmisionFinal;
    
    public String fechaEmisionInicial;
    
    public String rucEmisor;
    
    public String serie;
    
    public String tipoDocumento;
    private transient java.lang.Object __source;    
    

    
    
    
    public void loadFromSoap(java.lang.Object paramObj,DExtendedSoapSerializationEnvelope __envelope)
    {
        if (paramObj == null)
            return;
        AttributeContainer inObj=(AttributeContainer)paramObj;
        __source=inObj; 
        if(inObj instanceof SoapObject)
        {
            SoapObject soapObject=(SoapObject)inObj;
            int size = soapObject.getPropertyCount();
            for (int i0=0;i0< size;i0++)
            {
                PropertyInfo info=soapObject.getPropertyInfo(i0);
                if(!loadProperty(info,soapObject,__envelope))
                {
                }
            } 
        }

    }

    
    protected boolean loadProperty(PropertyInfo info,SoapObject soapObject,DExtendedSoapSerializationEnvelope __envelope)
    {
        java.lang.Object obj = info.getValue();
        if (info.name.equals("apiKey"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.apiKey = j.toString();
                    }
                }
                else if (obj instanceof String){
                    this.apiKey = (String)obj;
                }
            }
            return true;
        }
        if (info.name.equals("correlativoFinal"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.correlativoFinal = new Long(j.toString());
                    }
                }
                else if (obj instanceof Long){
                    this.correlativoFinal = (Long)obj;
                }
            }
            return true;
        }
        if (info.name.equals("correlativoInicial"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.correlativoInicial = new Long(j.toString());
                    }
                }
                else if (obj instanceof Long){
                    this.correlativoInicial = (Long)obj;
                }
            }
            return true;
        }
        if (info.name.equals("estadoSunat"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.estadoSunat = Integer.parseInt(j.toString());
                    }
                }
                else if (obj instanceof Integer){
                    this.estadoSunat = (Integer)obj;
                }
            }
            return true;
        }
        if (info.name.equals("fechaEmisionFinal"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.fechaEmisionFinal = j.toString();
                    }
                }
                else if (obj instanceof String){
                    this.fechaEmisionFinal = (String)obj;
                }
            }
            return true;
        }
        if (info.name.equals("fechaEmisionInicial"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.fechaEmisionInicial = j.toString();
                    }
                }
                else if (obj instanceof String){
                    this.fechaEmisionInicial = (String)obj;
                }
            }
            return true;
        }
        if (info.name.equals("rucEmisor"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.rucEmisor = j.toString();
                    }
                }
                else if (obj instanceof String){
                    this.rucEmisor = (String)obj;
                }
            }
            return true;
        }
        if (info.name.equals("serie"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.serie = j.toString();
                    }
                }
                else if (obj instanceof String){
                    this.serie = (String)obj;
                }
            }
            return true;
        }
        if (info.name.equals("tipoDocumento"))
        {
            if(obj!=null)
            {
                if (obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    if(j.toString()!=null)
                    {
                        this.tipoDocumento = j.toString();
                    }
                }
                else if (obj instanceof String){
                    this.tipoDocumento = (String)obj;
                }
            }
            return true;
        }
        return false;
    }
    
    public java.lang.Object getOriginalXmlSource()
    {
        return __source;
    }    
    

    @Override
    public java.lang.Object getProperty(int propertyIndex) {
        //!!!!! If you have a compilation error here then you are using old version of ksoap2 library. Please upgrade to the latest version.
        //!!!!! You can find a correct version in Lib folder from generated zip file!!!!!
        if(propertyIndex==0)
        {
            return this.apiKey!=null?this.apiKey:SoapPrimitive.NullNilElement;
        }
        if(propertyIndex==1)
        {
            return correlativoFinal;
        }
        if(propertyIndex==2)
        {
            return correlativoInicial;
        }
        if(propertyIndex==3)
        {
            return estadoSunat;
        }
        if(propertyIndex==4)
        {
            return this.fechaEmisionFinal!=null?this.fechaEmisionFinal:SoapPrimitive.NullNilElement;
        }
        if(propertyIndex==5)
        {
            return this.fechaEmisionInicial!=null?this.fechaEmisionInicial:SoapPrimitive.NullNilElement;
        }
        if(propertyIndex==6)
        {
            return this.rucEmisor!=null?this.rucEmisor:SoapPrimitive.NullNilElement;
        }
        if(propertyIndex==7)
        {
            return this.serie!=null?this.serie:SoapPrimitive.NullNilElement;
        }
        if(propertyIndex==8)
        {
            return this.tipoDocumento!=null?this.tipoDocumento:SoapPrimitive.NullNilElement;
        }
        return null;
    }


    @Override
    public int getPropertyCount() {
        return 9;
    }

    @Override
    public void getPropertyInfo(int propertyIndex, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info)
    {
        if(propertyIndex==0)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "apiKey";
            info.namespace= "http://servicioWeb.azurian.com";
        }
        if(propertyIndex==1)
        {
            info.type = PropertyInfo.LONG_CLASS;
            info.name = "correlativoFinal";
            info.namespace= "http://servicioWeb.azurian.com";
        }
        if(propertyIndex==2)
        {
            info.type = PropertyInfo.LONG_CLASS;
            info.name = "correlativoInicial";
            info.namespace= "http://servicioWeb.azurian.com";
        }
        if(propertyIndex==3)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "estadoSunat";
            info.namespace= "http://servicioWeb.azurian.com";
        }
        if(propertyIndex==4)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "fechaEmisionFinal";
            info.namespace= "http://servicioWeb.azurian.com";
        }
        if(propertyIndex==5)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "fechaEmisionInicial";
            info.namespace= "http://servicioWeb.azurian.com";
        }
        if(propertyIndex==6)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "rucEmisor";
            info.namespace= "http://servicioWeb.azurian.com";
        }
        if(propertyIndex==7)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "serie";
            info.namespace= "http://servicioWeb.azurian.com";
        }
        if(propertyIndex==8)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "tipoDocumento";
            info.namespace= "http://servicioWeb.azurian.com";
        }
    }
    
    @Override
    public void setProperty(int arg0, java.lang.Object arg1)
    {
    }

    /*
    *

     public String apiKey;

    public Long correlativoFinal=0L;

    public Long correlativoInicial=0L;

    public Integer estadoSunat=0;

    public String fechaEmisionFinal;

    public String fechaEmisionInicial;

    public String rucEmisor;

    public String serie;

    public String tipoDocumento;

    */
    public String toString(){

        StringBuilder builder = new StringBuilder();
        builder.append("apiKey: "+apiKey +"\n");
        builder.append("correlativoFinal: "+correlativoFinal +"\n");
        builder.append("correlativoInicial: "+correlativoInicial +"\n");
        builder.append("rucEmisor: "+rucEmisor +"\n");
        builder.append("serie: "+serie +"\n");
        builder.append("tipoDocumento: "+tipoDocumento +"\n");




        return builder.toString();
    }
    
}

