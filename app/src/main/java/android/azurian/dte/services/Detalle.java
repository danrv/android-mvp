package android.azurian.dte.services;

import java.io.Serializable;

/**
 * Created by danielrodriguez on 27-02-18.
 */

public class Detalle implements Serializable{

    public String descripcion;
    public String codigo;
    public String tipoIGV;
    public Double valorUnitario;
    public int cantidad;
    public Double valorSubTotal;
    public String tipoUnidad;
    public String fbKey;

}
