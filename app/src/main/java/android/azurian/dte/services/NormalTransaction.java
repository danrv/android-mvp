package android.azurian.dte.services;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by danielrodriguez on 27-02-18.
 */

public class NormalTransaction implements Serializable {

    Cliente cliente;
    String fechaEmision;
    String fechaVencimiento;
    String serie;
    String moneda;
    boolean documentoGratuito;
    ArrayList<Detalle> listaDetalles;
    String observaciones;
    int descuento;
    String tipoDocumento;
    String motivoSustento;
    int codigoNotaInt;
    String codigoNotaStr;

    String modSerie;
    String modFolio;

    public String getModSerie() {
        return modSerie;
    }

    public void setModSerie(String modSerie) {
        this.modSerie = modSerie;
    }

    public String getModFolio() {
        return modFolio;
    }

    public void setModFolio(String modFolio) {
        this.modFolio = modFolio;
    }

    public String getMotivoSustento() {
        return motivoSustento;
    }

    public void setMotivoSustento(String motivoSustento) {
        this.motivoSustento = motivoSustento;
    }

    public int getCodigoNotaInt() {
        return codigoNotaInt;
    }

    public void setCodigoNotaInt(int codigoNotaInt) {
        this.codigoNotaInt = codigoNotaInt;
    }

    public String getCodigoNotaStr() {
        return codigoNotaStr;
    }

    public void setCodigoNotaStr(String codigoNotaStr) {
        this.codigoNotaStr = codigoNotaStr;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public int getDescuento() {
        return descuento;
    }

    public void setDescuento(int descuento) {
        this.descuento = descuento;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(String fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public boolean isDocumentoGratuito() {
        return documentoGratuito;
    }

    public void setDocumentoGratuito(boolean documentoGratuito) {
        this.documentoGratuito = documentoGratuito;
    }

    public ArrayList<Detalle> getListaDetalles() {
        return listaDetalles;
    }

    public void setListaDetalles(ArrayList<Detalle> listaDetalles) {
        this.listaDetalles = listaDetalles;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }



    public NormalTransaction(){
        listaDetalles = new ArrayList<Detalle>();
    }

}
