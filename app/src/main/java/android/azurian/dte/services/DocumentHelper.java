package android.azurian.dte.services;

import java.io.Serializable;

/**
 * Created by danielrodriguez on 21-02-18.
 */

public class DocumentHelper implements Serializable{

    public Long correlativo;
    public String correlativoStr;
    public String descripcionSunat;
    public String estadoDocAzurian;
    public String estadoDocSunat;
    public String linkPDF;
    public String linkXML;
    public String rucEmisor;
    public String serie;
    public String tipoDocumento;
    public String tipoDocumentoStr;
    public String nombreReceptor;
    public String rucReceptor;
    public Double valor;


    @Override
    public String toString(){
        return " Doc:"+correlativoStr+" | "+linkXML+" | "+linkPDF;
    }
}
