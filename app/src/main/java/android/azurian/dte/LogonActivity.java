package android.azurian.dte;

import android.azurian.dte.Emision.EmisionOkActivity;
import android.azurian.dte.home.ESelectActivity;
import android.azurian.dte.home.EmpresaHelper;
import android.azurian.dte.home.HomeActivity;
import android.azurian.dte.home.HomeAltActivity;
import android.azurian.dte.home.JsonObjectHelper;
import android.azurian.dte.services.Dconfig;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Serializable;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;

import android.view.inputmethod.InputMethodManager;

import org.json.JSONArray;
import org.json.JSONObject;

public class LogonActivity extends AppCompatActivity {

    /*
    * Variables UI
    * */
    Button      btnLogon;
    EditText    inTxtUser;
    EditText    inTxtPasswd;
    ProgressBar progressBar;

    final String TAG = getClass().getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logon);

        btnLogon    = findViewById(R.id.btnLogon);
        inTxtUser   = findViewById(R.id.inTxtUser);
        inTxtPasswd = findViewById(R.id.inTxtPassw);
        progressBar = findViewById(R.id.actividad);

        try{

            /*
            * Setear Texto de Version en Pantalla Login
            * */

            TextView lblVersionInfo = findViewById(R.id.lblVersionInfo);
            lblVersionInfo.setText("Azurian DTE Android App version "+BuildConfig.VERSION_NAME);
            Log.i(TAG,"VERSION "+BuildConfig.VERSION_NAME);

        }catch (Exception e){

        }


        btnLogon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String user = inTxtUser.getText().toString();
                final String passwd = inTxtPasswd.getText().toString();

                if(user.isEmpty() || passwd.isEmpty()){
                    Toast.makeText(getApplicationContext(),R.string.texto_login_incorrecto,Toast.LENGTH_SHORT).show();

                    return;
                }


                try{
                    InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                }catch(Exception e){


                }


                Thread th = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        doLogin(user,passwd);
                    }
                });
                th.start();
            }
        });
    }


    private void doLogin(final String user, final String passwd){
        Log.d( TAG,"doLogin");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                progressBar.animate();
                progressBar.setVisibility(View.VISIBLE);
                //Log.i("info","User: "+ user +" passwd: "+passwd);
                btnLogon.setVisibility(View.INVISIBLE);
            }
        });

        try{


            /*
            * Realizar la consulta a Servicio
            * */
            HttpURLConnection connection;
            URL url = new URL(Dconfig.logonUrl);
            connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setConnectTimeout(10000);


            StringBuilder result = new StringBuilder();

            String body = "user="+user+"&password="+passwd;
            byte[] bytes = body.getBytes("UTF-8");
            OutputStream os = connection.getOutputStream();

            os.write(bytes);
            connection.connect();




            if(connection.getResponseCode() == HttpURLConnection.HTTP_OK){
                Dconfig.user = user;
                InputStream in = new BufferedInputStream(connection.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }
                JSONArray jsonArray = new JSONArray(result.toString());
                Log.i(TAG,"Ok");
                Log.i(TAG,"JSON "+ jsonArray);
                Intent intent = new Intent(LogonActivity.this, ESelectActivity.class);


                ArrayList<EmpresaHelper> listaEmpresas = new ArrayList<EmpresaHelper>();

                // Por cada empresa resultado:

                for (int i=0;i<jsonArray.length();i++){
                    JSONObject objc = (JSONObject) jsonArray.get(i);
                    String ruc = objc.getString("ruc");
                    String nombre = objc.getString("nombreComercial");
                    String hash = objc.getString("hash");
                    String direccion = objc.getString("direccion");
                    String ubigeo = objc.getString("ubigeo");

                    String tipoDocumento = objc.getString("tipoDocumento");
                    String nombreCalle = objc.getString("nombreCalle");
                    String urbanizacion = objc.getString("urbanizacion");
                    String provincia = objc.getString("provincia");
                    String departamento = objc.getString("departamento");
                    String distrito = objc.getString("distrito");
                    String codigoPais = objc.getString("codigoPais");
                    String razonSocial = objc.getString("razonSocial");


                    //Log.i(TAG,"HASH: "+hash);

                    EmpresaHelper eh = new EmpresaHelper();
                    eh.nombre = nombre;

                    //Log.i(TAG,"EEXP "+nombre + "-> "+ruc+"-"+hash);
                    eh.hash = MD5(ruc+"-"+hash);
                    //Log.i(TAG,"EEXP "+MD5(ruc+"-"+hash));
                    eh.rut = ruc;
                    eh.direccion = direccion;
                    eh.ubigeo = ubigeo;

                    eh.tipoDocumento = tipoDocumento;
                    eh.nombreCalle = nombreCalle;
                    eh.urbanizcion = urbanizacion;
                    eh.provincia = provincia;
                    eh.departamento = departamento;
                    eh.distrito = distrito;
                    eh.codigoPais = codigoPais;
                    eh.razonSocial = razonSocial;

                    listaEmpresas.add(eh);
                }


                intent.putExtra("lista",listaEmpresas);
                startActivity(intent);
            }else{
                Log.i(TAG,"NOK");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        btnLogon.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.INVISIBLE);
                        Toast toast = Toast.makeText(getApplicationContext(),R.string.info_login_incorrecta,Toast.LENGTH_LONG);//new Toast(getApplicationContext());
                        toast.show();


                    }
                });

            }

            connection.disconnect();

        }catch (Exception e){

            Log.e(TAG,e.toString());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    btnLogon.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.INVISIBLE);
                    Toast toast = Toast.makeText(getApplicationContext(),R.string.error_conexion,Toast.LENGTH_LONG);//new Toast(getApplicationContext());
                    toast.show();


                }
            });
        }


    }

    public String MD5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }

    public static String cifrarPassword(String password) {
        String ret = null;
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            //pasar string a byte[]
            char [] pass = password.toCharArray();
            byte [] buffer = new byte[pass.length];
            for (int i = 0; i < buffer.length; i++) {
                buffer[i] = (byte)pass[i];
            }
            //pasar por md5
            md5.update(buffer);
            byte [] salida = md5.digest();

            //generar string nuevamente
            BigInteger bi = new BigInteger(salida);
            ret = bi.toString(16);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

}
