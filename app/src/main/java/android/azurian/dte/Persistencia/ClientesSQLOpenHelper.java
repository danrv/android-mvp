package android.azurian.dte.Persistencia;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by danielrodriguez on 19-03-18.
 */

public class ClientesSQLOpenHelper extends SQLiteOpenHelper {
    public ClientesSQLOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String crearClientes = "CREATE TABLE \"clientes\" (\n" +
                "\t \"ruc\" TEXT,\n" +
                "\t \"nombre\" TEXT,\n" +
                "\t \"direccion\" TEXT,\n" +
                "\t \"correo\" TEXT,\n" +
                "\t \"otro\" TEXT,\n" +
                "\t \"fbkey\" TEXT,\n" +
                "\t \"tipoDocumento\" TEXT\n" +
                ")";
        db.execSQL(crearClientes);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS clientes");
        String crearClientes = "CREATE TABLE \"clientes\" (\n" +
                "\t \"ruc\" TEXT,\n" +
                "\t \"nombre\" TEXT,\n" +
                "\t \"direccion\" TEXT,\n" +
                "\t \"correo\" TEXT,\n" +
                "\t \"otro\" TEXT,\n" +
                "\t \"fbkey\" TEXT,\n" +
                "\t \"tipoDocumento\" TEXT\n" +
                ")";
        db.execSQL(crearClientes);
    }
}
