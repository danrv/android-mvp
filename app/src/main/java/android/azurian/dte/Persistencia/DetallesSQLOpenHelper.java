package android.azurian.dte.Persistencia;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by danielrodriguez on 20-03-18.
 */

public class DetallesSQLOpenHelper extends SQLiteOpenHelper {

    public DetallesSQLOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String crearDetalles = "CREATE TABLE \"detalles\" (\n" +
                "\t \"descripcion\" TEXT,\n" +
                "\t \"codigo\" TEXT,\n" +
                "\t \"tipoIGV\" TEXT,\n" +
                "\t \"valorUnitario\" real,\n" +
                "\t \"cantidad\" integer,\n" +
                "\t \"valorSubtotal\" real,\n" +
                "\t \"tipoUnidad\" TEXT,\n" +
                "\t \"fbKey\" TEXT\n" +
                ")";

        db.execSQL(crearDetalles);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS detalles");
        String crearDetalles = "CREATE TABLE \"detalles\" (\n" +
                "\t \"descripcion\" TEXT,\n" +
                "\t \"codigo\" TEXT,\n" +
                "\t \"tipoIGV\" TEXT,\n" +
                "\t \"valorUnitario\" real,\n" +
                "\t \"cantidad\" integer,\n" +
                "\t \"valorSubtotal\" real,\n" +
                "\t \"tipoUnidad\" TEXT,\n" +
                "\t \"fbKey\" TEXT\n" +
                ")";

        db.execSQL(crearDetalles);


    }
}
