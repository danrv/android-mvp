package android.azurian.dte.uitls;

import android.util.Log;

import java.text.DecimalFormat;

/**
 * Created by danielrodriguez on 28-02-18.
 */

public class MonedaSoles {

    private static String unidades(int num, boolean valor) {
        String res = "";
        switch (num)
        {
            case 1:
                res = valor ? "un" : "uno"; break;
            case 2:
                res = "dos"; break;
            case 3:
                res = "tres"; break;
            case 4:
                res = "cuatro"; break;
            case 5:
                res = "cinco"; break;
            case 6:
                res = "seis"; break;
            case 7:
                res = "siete"; break;
            case 8:
                res = "ocho"; break;
            case 9:
                res = "nueve";
        }
        return res;
    }

    private static String decenas(int num, boolean valor) {
        String res = "";
        if (num < 10) {
            res = unidades(num, false);
        } else if (num == 10) {
            res = "diez";
        } else if (num == 11) {
            res = "once";
        } else if (num == 12) {
            res = "doce";
        } else if (num == 13) {
            res = "trece";
        } else if (num == 14) {
            res = "catorce";
        } else if (num == 15) {
            res = "quince";
        } else if ((num >= 16) && (num < 20)) {
            res = "dieci" + unidades(num - 10, valor);
        } else if ((num >= 20) && (num < 30)) {
            res = "veint" + (num - 20 == 0 ? "e" : new StringBuilder().append("i").append(unidades(num - 20, valor)).toString());
        } else if ((num >= 30) && (num < 40)) {
            res = "treinta" + (num - 30 == 0 ? "" : new StringBuilder().append(" y ").append(unidades(num - 30, valor)).toString());
        } else if ((num >= 40) && (num < 50)) {
            res = "cuarenta" + (num - 40 == 0 ? "" : new StringBuilder().append(" y ").append(unidades(num - 40, valor)).toString());
        } else if ((num >= 50) && (num < 60)) {
            res = "cincuenta" + (num - 50 == 0 ? "" : new StringBuilder().append(" y ").append(unidades(num - 50, valor)).toString());
        } else if ((num >= 60) && (num < 70)) {
            res = "sesenta" + (num - 60 == 0 ? "" : new StringBuilder().append(" y ").append(unidades(num - 60, valor)).toString());
        } else if ((num >= 70) && (num < 80)) {
            res = "setenta" + (num - 70 == 0 ? "" : new StringBuilder().append(" y ").append(unidades(num - 70, valor)).toString());
        } else if ((num >= 80) && (num < 90)) {
            res = "ochenta" + (num - 80 == 0 ? "" : new StringBuilder().append(" y ").append(unidades(num - 80, valor)).toString());
        } else if ((num >= 90) && (num < 100)) {
            res = "noventa" + (num - 90 == 0 ? "" : new StringBuilder().append(" y ").append(unidades(num - 90, valor)).toString());
        }
        return res;
    }

    private static String centenas(int num, boolean valor) {
        String res = "";
        if (num < 100) {
            res = decenas(num, valor);
        } else if ((num >= 100) && (num < 200)) {
            res = "cien" + (num - 100 == 0 ? "" : new StringBuilder().append("to ").append(decenas(num - 100, valor)).toString());
        } else if ((num >= 200) && (num < 300)) {
            res = "doscientos" + (num - 200 == 0 ? "" : new StringBuilder().append(" ").append(decenas(num - 200, valor)).toString());
        } else if ((num >= 300) && (num < 400)) {
            res = "trescientos" + (num - 300 == 0 ? "" : new StringBuilder().append(" ").append(decenas(num - 300, valor)).toString());
        } else if ((num >= 400) && (num < 500)) {
            res = "cuatrocientos" + (num - 400 == 0 ? "" : new StringBuilder().append(" ").append(decenas(num - 400, valor)).toString());
        } else if ((num >= 500) && (num < 600)) {
            res = "quinientos" + (num - 500 == 0 ? "" : new StringBuilder().append(" ").append(decenas(num - 500, valor)).toString());
        } else if ((num >= 600) && (num < 700)) {
            res = "seiscientos" + (num - 600 == 0 ? "" : new StringBuilder().append(" ").append(decenas(num - 600, valor)).toString());
        } else if ((num >= 700) && (num < 800)) {
            res = "setecientos" + (num - 700 == 0 ? "" : new StringBuilder().append(" ").append(decenas(num - 700, valor)).toString());
        } else if ((num >= 800) && (num < 900)) {
            res = "ochocientos" + (num - 800 == 0 ? "" : new StringBuilder().append(" ").append(decenas(num - 800, valor)).toString());
        } else if ((num >= 900) && (num < 1000)) {
            res = "novecientos" + (num - 900 == 0 ? "" : new StringBuilder().append(" ").append(decenas(num - 900, valor)).toString());
        }
        return res;
    }

    private static String unidadMiles(int num) {
        String res = "";
        if (num < 1000) {
            res = centenas(num, false);
        } else if ((num >= 1000) && (num < 2000)) {
            res = "mil " + centenas(num % 1000, false);
        } else {
            res = centenas(num / 1000, true) + " mil " + centenas(num % 1000, false);
        }
        return res;
    }

    private static String millon(int num) {
        String res = "";
        if (num < 1000000)
        {
            res = unidadMiles(num);
        }
        else if (num == 1000000)
        {
            res = "Un millon ";
        }
        else
        {
            String unidad = unidades(num / 1000000, true);
            res = unidad + (unidad.equals("un") ? " millon " : " millones ") + unidadMiles(num % 1000000);
        }
        return res;
    }

    private static String convertir(int num) {
        if (num == 0) {
            return "cero";
        }
        //if ((num > 0) && (num <= Integer.MAX_VALUE))// 9999999))
        if ((num > 0) && (num <=  9999999))
        {
            String res = millon(num);
            char primero = res.charAt(0);
            return String.valueOf(primero).toUpperCase() + res.substring(1);
        }
        return "FUERA DEL RANGO!!";
    }

    public String getMontoEnPalabras(double valor, String moneda) {

        Log.i("MS"," -> "+valor);
        //DecimalFormat df = new DecimalFormat("#.00");
        //df.setMaximumFractionDigits(8);

        String numero = String.valueOf(valor);
        //String numero = df.format(valor);
        Log.i("MS"," -> "+numero);
        int punto = numero.indexOf(".");
        String entero = convertir(Integer.parseInt(numero.substring(0, punto)));
        String decimal = numero.substring(punto + 1);

        //agregando 0 a la iz si es entre 0 y 9


        if(decimal.length() == 1) {
            decimal = String.format("%2d0", Integer.parseInt(decimal));
        }else{
            decimal = String.format("%2d", Integer.parseInt(decimal));
        }
        return entero + " con " + decimal + "/100 "+moneda;
    }

    // mayor capacidad?

    private static final String[] tensNames = {
            "",
            " diez",
            " veinte",
            " treinta",
            " cuarenta",
            " cincuenta",
            " sesenta",
            " setenta",
            " ochenta",
            " noventa"
    };

    private static final String[] numNames = {
            "",
            " uno",
            " dos",
            " tres",
            " cuantro",
            " cinco",
            " seis",
            " siete",
            " ocho",
            " nueve",
            " diez",
            " once",
            " doce",
            " trece",
            " catorce",
            " quince",
            " dieciseis",
            " diecisiete",
            " dieciocho",
            " diecinueve"
    };

    private static String convertLessThanOneThousand(int number) {
        String soFar;

        if (number % 100 < 20){
            soFar = numNames[number % 100];
            number /= 100;
        }
        else {
            soFar = numNames[number % 10];
            number /= 10;

            soFar = tensNames[number % 10] + soFar;
            number /= 10;
        }
        if (number == 0) return soFar;
        return numNames[number] + " cientos" + soFar;
    }


    public static String doConversion(Double monto){

        DecimalFormat df2 = new DecimalFormat("#,##0.00");
        String numString = df2.format(monto);
        Log.i("MonedaSoles","doConv "+numString);
        long comp0 = monto.longValue();

        String strFraccion = numString.substring(numString.indexOf('.')+1);
        int centimosLong = Integer.parseInt(strFraccion);
        String strCentimos = convertLessThanOneThousand(centimosLong);


        return convert(comp0)+" con "+strCentimos+"/100 SOLES";

       // return "";
    }
    public static String convert(long number) {
        // 0 to 999 999 999 999
        if (number == 0) { return "cero"; }

        String snumber = Long.toString(number);

        // pad with "0"
        String mask = "000000000000";
        DecimalFormat df = new DecimalFormat(mask);
        snumber = df.format(number);

        // XXXnnnnnnnnn
        int billions = Integer.parseInt(snumber.substring(0,3));
        // nnnXXXnnnnnn
        int millions  = Integer.parseInt(snumber.substring(3,6));
        // nnnnnnXXXnnn
        int hundredThousands = Integer.parseInt(snumber.substring(6,9));
        // nnnnnnnnnXXX
        int thousands = Integer.parseInt(snumber.substring(9,12));

        String tradBillions;
        switch (billions) {
            case 0:
                tradBillions = "";
                break;
            case 1 :
                tradBillions = convertLessThanOneThousand(billions)
                        + " billon ";
                break;
            default :
                tradBillions = convertLessThanOneThousand(billions)
                        + " billones ";
        }
        String result =  tradBillions;

        String tradMillions;
        switch (millions) {
            case 0:
                tradMillions = "";
                break;
            case 1 :
                tradMillions = convertLessThanOneThousand(millions)
                        + " millon ";
                break;
            default :
                tradMillions = convertLessThanOneThousand(millions)
                        + " millones ";
        }
        result =  result + tradMillions;

        String tradHundredThousands;
        switch (hundredThousands) {
            case 0:
                tradHundredThousands = "";
                break;
            case 1 :
                tradHundredThousands = "mil ";
                break;
            default :
                tradHundredThousands = convertLessThanOneThousand(hundredThousands)
                        + " mil ";
        }
        result =  result + tradHundredThousands;

        String tradThousand;
        tradThousand = convertLessThanOneThousand(thousands);
        result =  result + tradThousand;

        // remove extra spaces!
        return result.replaceAll("^\\s+", "").replaceAll("\\b\\s{2,}\\b", " ");
    }


}
