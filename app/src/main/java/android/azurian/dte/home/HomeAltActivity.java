package android.azurian.dte.home;

import android.azurian.dte.BlePrinter.DeviceListActivity;
import android.azurian.dte.BlePrinter.PrinterService;
import android.azurian.dte.ClientesContainerFragment;
import android.azurian.dte.Configuracion.ConfiguracionActivity;
import android.azurian.dte.Consulta.ConsultaContainerFragment;
import android.azurian.dte.Consulta.ConsultaHomeFragment;
import android.azurian.dte.Emision.EmisionStepOneFragment;
import android.azurian.dte.Emision.MenuEmisionFragment;
import android.azurian.dte.Emision.EmisionContainerFragment;
import android.azurian.dte.Emision.EmisionHomeFragment;
import android.azurian.dte.Emision.NotasStepOneFragment;
import android.azurian.dte.EmptyFragment;
import android.azurian.dte.Emision.EmisionStepThreeFragment;
import android.azurian.dte.Emision.EmisionStepTwoFragment;
import android.azurian.dte.Emision.MenuClientesFragment;
import android.azurian.dte.Emision.ObservacionesFragment;
import android.azurian.dte.Consulta.Resultados;
import android.azurian.dte.Consulta.FormularioFragment;
import android.azurian.dte.Emision.ReviewFragment;
import android.azurian.dte.services.Dconfig;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;
import android.azurian.dte.R;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class HomeAltActivity extends AppCompatActivity implements ReviewFragment.OnFragmentInteractionListener, ObservacionesFragment.OnFragmentInteractionListener, EmisionStepThreeFragment.OnFragmentInteractionListener,  EmisionStepTwoFragment.OnFragmentInteractionListener, EmisionStepOneFragment.OnFragmentInteractionListener, MenuClientesFragment.OnFragmentInteractionListener, ClientesContainerFragment.OnFragmentInteractionListener,EmisionContainerFragment.OnFragmentInteractionListener, MenuEmisionFragment.OnFragmentInteractionListener, EmptyFragment.OnFragmentInteractionListener, ConsultaContainerFragment.OnFragmentInteractionListener, FormularioFragment.OnFragmentInteractionListener, Resultados.OnFragmentInteractionListener, ConsultaHomeFragment.OnFragmentInteractionListener, EmisionHomeFragment.OnFragmentInteractionListener, NotasStepOneFragment.OnFragmentInteractionListener{

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private FirebaseAuth mAuth;
    EmpresaHelper selected;
    FoliosHelper foliosHelper;

    final String TAG = getClass().getSimpleName();

    public FoliosHelper getFoliosHelper() {
        return foliosHelper;
    }

    public void setFoliosHelper(FoliosHelper foliosHelper) {
        this.foliosHelper = foliosHelper;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_alt);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        SharedPreferences sharedPreferences = getSharedPreferences("PRINTER_PREFS",MODE_PRIVATE);
        String idPrinter = "";
        idPrinter = sharedPreferences.getString("PRINTER_ID","");

        if(idPrinter.isEmpty()){

            Snackbar printBar = Snackbar.make(toolbar,"Debes configurar una impresora",Snackbar.LENGTH_LONG);
            printBar.show();

        }

        try {
            PrinterService service = PrinterService.getInstance(getApplicationContext());
            service.initService();
        }catch (Exception prE){
            Log.e(TAG,prE.toString());
        }
        mAuth = FirebaseAuth.getInstance();

        mAuth.signInAnonymously().addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                Log.i(TAG,"Done");
            }
        });
        selected        = (EmpresaHelper)getIntent().getSerializableExtra("empresa");
        foliosHelper    = (FoliosHelper)getIntent().getSerializableExtra("foliosHelper");


        Dconfig.ruc = selected.rut;
        Dconfig.hash = selected.hash;
        Dconfig.razonSocial = selected.nombre;
        Dconfig.direccion = selected.direccion;
        Dconfig.ubigeo = selected.ubigeo;
        Log.i(TAG,selected.toString());

        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());


        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setOffscreenPageLimit(6);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home_alt, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            Log.i("MENU","action_settings");
            return true;
        }
        if(id == R.id.action_settings_conf){
            Intent intent = new Intent(this, ConfiguracionActivity.class);
            startActivity(intent);
        }
        if(id == R.id.action_settings_print){

            Intent intent = new Intent(this, DeviceListActivity.class);
            startActivity(intent);

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_home_alt, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }

    @Override
    public void onBackPressed(){

        Log.d(TAG,"onBackPressed " +getFragmentManager().getBackStackEntryCount());
        if(getFragmentManager().getBackStackEntryCount() > 0){

        }else{

            super.onBackPressed();
        }

    }
    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */

    
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).

            if(position == 0){


                return EmisionContainerFragment.newInstance("","");

            }
            if(position == 1){
                return ConsultaContainerFragment.newInstance("","");
            }
            if(position == 2){
                return ClientesContainerFragment.newInstance("","");
            }
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }
    }
}
