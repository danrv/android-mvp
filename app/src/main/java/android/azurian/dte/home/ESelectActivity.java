package android.azurian.dte.home;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.azurian.dte.R;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class ESelectActivity extends AppCompatActivity {

    Spinner empSelect;
    Button btnSelEmp;
    EmpresaHelper selected;
    final String TAG = getClass().getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eselect);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Empresas");
        setSupportActionBar(toolbar);
        final ArrayList<EmpresaHelper> listaEmpresas = (ArrayList<EmpresaHelper>) getIntent().getSerializableExtra("lista");

        empSelect = findViewById(R.id.empSelect);
        btnSelEmp = findViewById(R.id.btnSelEmp);



        List<String> nombreEmpresas = new ArrayList<String>();
        for(EmpresaHelper eh : listaEmpresas){
            Log.i(TAG,"-> "+eh.rut +" "+eh.nombre + " "+eh.hash);
            nombreEmpresas.add(eh.nombre);
        }
        if (listaEmpresas.size()==1){
            selected = listaEmpresas.get(0);
            Intent intent = new Intent(getApplicationContext(),HomeAltActivity.class);
            intent.putExtra("empresa",selected);
            startActivity(intent);
        }



        Log.i(TAG,"lista "+ listaEmpresas.size());
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,nombreEmpresas);

        empSelect.setAdapter(dataAdapter);
        empSelect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final EmpresaHelper ehs = listaEmpresas.get(position);
                if (ehs.hash.equalsIgnoreCase("NAK")){
                    btnSelEmp.setVisibility(View.INVISIBLE);
                    Toast.makeText(getApplicationContext(),R.string.no_api_key,Toast.LENGTH_SHORT).show();
                }else {





                    btnSelEmp.setVisibility(View.VISIBLE);
                    selected = ehs;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

                btnSelEmp.setVisibility(View.INVISIBLE);
            }
        });

        btnSelEmp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /**
                 *
                 * Obtener  Folios Disponibles para los 4 tipos de Documentosyuh
                 */

                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {


                        ObtenerFolios obtenerFolios = new ObtenerFolios();
                        final List<String> listaFoliosFacturas    =  obtenerFolios.obtenerListaParaDocumento(selected.rut, "01");
                        List<String> listaFoliosBoletas     =  obtenerFolios.obtenerListaParaDocumento(selected.rut, "03");
                        List<String> listaFoliosNotaCredito =  obtenerFolios.obtenerListaParaDocumento(selected.rut, "07");
                        List<String> listaFoliosNotaDebito  =  obtenerFolios.obtenerListaParaDocumento(selected.rut, "08");

                        final FoliosHelper foliosHelper = new FoliosHelper();
                        foliosHelper.setListaFoliosFacturas(listaFoliosFacturas);
                        foliosHelper.setListaFoliosBoletas(listaFoliosBoletas);
                        foliosHelper.setListaFoliosNotasCredito(listaFoliosNotaCredito);
                        foliosHelper.setListaFoliosNotasDebito(listaFoliosNotaDebito);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                Intent intent = new Intent(getApplicationContext(),HomeAltActivity.class);
                                intent.putExtra("empresa",selected);
                                intent.putExtra("foliosHelper",foliosHelper);
                                startActivity(intent);
                            }
                        });
                    }
                });


                thread.start();


            }
        });
    }

}
