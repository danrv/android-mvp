package android.azurian.dte.home;

import java.io.Serializable;

/**
 * Created by danielrodriguez on 21-02-18.
 */

public class EmpresaHelper implements Serializable{
    public String nombre;
    public String rut;
    public String hash;
    public String direccion;
    public String ubigeo;
    public String tipoDocumento;
    public String nombreCalle;
    public String urbanizcion;
    public String provincia;
    public String departamento;
    public String distrito;
    public String codigoPais;
    public String razonSocial;


    @Override
    public String toString(){
        return "-->"+this.nombre + " | " +this.rut +" | " +this.hash;
    }
}
