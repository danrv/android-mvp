package android.azurian.dte.Consulta;

import android.Manifest;
import android.annotation.SuppressLint;

import android.app.DownloadManager;
import android.azurian.dte.BlePrinter.BluetoothService;
import android.azurian.dte.BlePrinter.ImpConfig;
import android.azurian.dte.BlePrinter.PrinterService;
import android.azurian.dte.BuildConfig;
import android.azurian.dte.sdk.Command;
import android.azurian.dte.sdk.PrinterCommand;
import android.azurian.dte.services.Dconfig;
import android.azurian.dte.services.ObtenerInfoDoc;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.azurian.dte.R;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;


/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class WebViewActivity extends AppCompatActivity {



    /******************************************************************************************************/
    // Debugging

    private static final boolean DEBUG = true;


    Button btnPrintFindedDodcument;
    Button btnReload;
    Button btnSave;

    InfoDocumentoFactura infoDocumentoFactura;
    String codigo;
    String tipo;
    final String TAG = getClass().getSimpleName();
    String url;
    WebView webView;


    String QrCode = "";

    boolean cargo = false;
    boolean cargoJson = false;
    int intentosJson = 0;


    public  boolean haveStoragePermission() {
        if (Build.VERSION.SDK_INT >= 23) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            return true;
           /*
            if (checkSelfPermission(WebViewActivity.this,android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.e("Permission error","You have permission");
                return true;
            } else {

                Log.e("Permission error","You have asked for permission");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }

            */
        }
        else { //you dont need to worry about these stuff below api level 23
            Log.e("Permission error","You already have the permission");
            return true;
        }
    }

    void cargarJson(){
        try{

            Thread th = new Thread(new Runnable() {
                @Override
                public void run() {
                    try{

                        JSONObject jsonObject = new JSONObject(getIntent().getStringExtra("json"));

                        Log.i(TAG,"JSON to send-> "+jsonObject.toString());

                        ObtenerInfoDoc oid = new ObtenerInfoDoc();

                        JSONObject response = oid.consultar(jsonObject);

                        Log.i(TAG,"JSON RESP "+response.toString());
                        if(response != null){

                            Log.i(TAG,"NOMBRE Emisor: "+infoDocumentoFactura.nombreEmisor);


                            JSONObject infoDocumento = response.getJSONObject("infoDocumento");

                            infoDocumentoFactura.nombreEmisor           = infoDocumento.getString("nombreEmisor");

                            infoDocumentoFactura.rucEmisor              = infoDocumento.getString("rucEmisor");
                            infoDocumentoFactura.documentoEmisor        = infoDocumento.getString("tipoDocumentoEmisor");
                            infoDocumentoFactura.direccionEmisor        = infoDocumento.getString("direccionEmisor");

                            infoDocumentoFactura.nombreReceptor         = infoDocumento.getString("razonSocialReceptor");
                            infoDocumentoFactura.rucReceptor            = infoDocumento.getString("numeroDocumentoReceptor");

                            infoDocumentoFactura.direccionReceptor      = infoDocumento.getString("direccionReceptor");
                            infoDocumentoFactura.documentoReceptor      = infoDocumento.getString("numeroDocumentoReceptor");

                            //infoDocumentoFactura.tipoComprobante  = infoDocumento.getString("tipoComprobante");

                            infoDocumentoFactura.igv                    = infoDocumento.getString("totalIGV");
                            infoDocumentoFactura.totalVenta             = infoDocumento.getString("importeTotal");
                            infoDocumentoFactura.fechaEmision           = infoDocumento.getString("fechaEmision");
                            //infoDocumentoFactura.resume = infoDocumento.getString("resume");
                            infoDocumentoFactura.strMonto               = infoDocumento.getString("montoTotal");

                            infoDocumentoFactura.descuento              = infoDocumento.getString("descuentosGlobales");
                            infoDocumentoFactura.tipoMoneda             = infoDocumento.getString("tipoMoneda");

                            infoDocumentoFactura.ordenCompra            = infoDocumento.getString("ordenCompra");

                            infoDocumentoFactura.serie                  = codigo;

                            infoDocumentoFactura.tipoDocumentoReceptor  = infoDocumento.getString("tipoDocumentoReceptor");

                            JSONArray listaItms = infoDocumento.getJSONArray("servicios");

                            infoDocumentoFactura.listaItems = new ArrayList<>();
                            for(int i=0;i<listaItms.length();i++){
                                JSONObject jitem = listaItms.getJSONObject(i);
                                InfoItemFactura iif = new InfoItemFactura();
                                iif.cantidad = jitem.getString("cantidadItem");
                                iif.descripcion = jitem.getString("descripcionItem");
                                iif.precio = jitem.getString("valorUnitarioItem");
                                iif.importe = jitem.getString("totalItem");
                                iif.codigoItem = jitem.getString("codigoItem");

                                infoDocumentoFactura.listaItems.add(iif);
                            }


                            QrCode = infoDocumento.getString("codigoQR");//sb.toString();

                            Log.i(TAG,"------------------");
                            Log.i(TAG,">> "+infoDocumento.toString());

                            cargoJson = true;
                            Log.i(TAG,"Cargo JSON");
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    btnPrintFindedDodcument.setVisibility(View.VISIBLE);
                                }
                            });
                        }
                    }catch (Exception e){

                        cargoJson = false;
                        Log.e(TAG,"ERROR "+e.toString());
                        Log.i(TAG,"Error Cargar JSON");
                        if(intentosJson < 5){
                            cargarJson();
                        }
                        intentosJson = intentosJson+1;

                    }

                }
            });
            th.start();

        }catch (Exception e){

        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_web_view);


        infoDocumentoFactura = new InfoDocumentoFactura();



        tipo = (String) getIntent().getStringExtra("tipo");


        Date date = new Date();
        Date daysAgo = new DateTime(date).minusDays(10).toDate();

        webView = findViewById(R.id.docWebView);

        final int intentos = 3;
        final int intentoActual = 0;


        url = getIntent().getStringExtra("url");
        //final String urlXml = getIntent().getStringExtra("urlXml");
        codigo = getIntent().getStringExtra("codigo");
        Log.i(TAG,""+url);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Log.i(TAG,"PAGE READY ?!? ");
                cargo = true;
                Log.i(TAG,"Cargo es true");
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);

            }
            @Override
            public void onPageStarted(final WebView webView, String url, Bitmap favicon){
                super.onPageStarted(webView,url,favicon);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        if(cargo == false) {
                            // intentar otra vez
                            if(intentoActual< intentos){

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        webView.reload();
                                        Log.i(TAG,"Reintando");
                                    }
                                });

                            }else{

                                Log.w(TAG,"Falla al cargar");
                            }

                        }else{

                            Thread.currentThread().interrupt();
                            Log.i(TAG,"Cargo, detener tarea");
                        }
                    }
                }).start();

            }

        });
        webView.loadUrl("http://docs.google.com/gview?embedded=true&url="+url);

        cargarJson();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if(cargoJson == false) {
                    // intentar otra vez
                    if(intentosJson< 5){

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                cargarJson();
                                Log.i(TAG,"Reintando JSON");
                            }
                        });

                    }else{

                        Log.w(TAG,"Falla al cargar JSON");
                    }

                }else{

                    Thread.currentThread().interrupt();
                    Log.i(TAG,"Cargo JSOn, detener tarea");
                }
            }
        }).start();



        btnSave     = findViewById(R.id.btnSaveFile);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                haveStoragePermission();
                Log.i(TAG,"Descargar y Guardar");

                DownloadManager downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                Uri uri = Uri.parse(url);

                DownloadManager.Request request = new DownloadManager.Request(uri);
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE|DownloadManager.Request.NETWORK_WIFI);
                request.setTitle("Documento DTE "+infoDocumentoFactura.serie);
                request.setDescription("Descargando");
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE);
                request.setVisibleInDownloadsUi(true);
                String finalPath = Environment.getExternalStorageDirectory()+"/DTE_DOCS/"+infoDocumentoFactura.serie+"_"+infoDocumentoFactura.tipoDocumentoReceptor+".pdf";
                //request.setDestinationUri(Uri.parse("file://doc_dte/"+infoDocumentoFactura.serie+"_"+infoDocumentoFactura.tipoDocumentoReceptor+".pdf"));
                File file = new File(finalPath);
                final Uri urix =  Uri.fromFile(file);
                request.setDestinationUri(urix);

                downloadManager.enqueue(request);
            }
        });

        btnReload = findViewById(R.id.btnReload);
        btnReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webView.loadUrl("http://docs.google.com/gview?embedded=true&url="+url);
            }
        });


        final PrinterService ps = PrinterService.getInstance(getApplicationContext());

        /*
        if (ps.isADeviceConnected == false){
            ps.initService();
        }
        */
        btnPrintFindedDodcument = findViewById(R.id.btnPrintFindedDodcument);
        btnPrintFindedDodcument.setVisibility(View.INVISIBLE);
        btnPrintFindedDodcument.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                final BluetoothService mService = ps.mService;

                try{

                    Thread th = new Thread(new Runnable() {
                        @Override
                        public void run() {


                            try{

                                //Drawable drawable = getApplicationContext().getResources().getDrawable(R.drawable.eaop);


                                SharedPreferences sharedPreferences = getSharedPreferences("PRINTER_PREFS",MODE_PRIVATE);
                                String idPrinter = "";
                                idPrinter = sharedPreferences.getString("PRINTER_ID","");
                                String bmpUriStr = sharedPreferences.getString("LOGO_URI","");

                                Uri p = Uri.parse(bmpUriStr);
                                //grantUriPermission("Ni.Idea.com",p,Intent.FLAG_GRANT_READ_URI_PERMISSION);

                                if(Build.VERSION.SDK_INT>22){
                                    requestPermissions(new String[] {Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                                }
                                Log.i(TAG,"SP Printer "+idPrinter);
                                if (idPrinter.equalsIgnoreCase("LESHP")){
                                    BitmapFactory.Options o = new BitmapFactory.Options();
                                    o.inScaled = false;
                                    Bitmap bmp = BitmapFactory.decodeResource(getApplicationContext().getResources(),R.drawable.dtebig,o);
                                    if(!bmpUriStr.isEmpty()){

                                        Uri bmpUri = Uri.parse(bmpUriStr);
                                        Log.i(TAG,"URI "+bmpUriStr + " "+bmpUri);
                                        bmp = MediaStore.Images.Media.getBitmap(getApplication().getContentResolver(), bmpUri);
                                    }

                                    Log.i(TAG,"TENGO BMP");
                                    Log.i(TAG,"BMP "+bmp.getWidth()+ " x "+bmp.getHeight());
                                    Log.i(TAG,"BMP " + bmp.getDensity());
                                    Log.i(TAG,"BMP "+ bmp.toString());

                                    int ancho = bmp.getWidth();
                                    int alto = bmp.getHeight();

                                    List<Byte> toPrintBytes = new ArrayList<>();



                                    for(int linea=0;linea<alto;linea++){

                                        int bitCounter = 0;
                                        byte byt = (byte)0x00;

                                        for(int col=0;col<ancho;col++){


                                            int pixel = bmp.getPixel(col,linea);
                                            if(pixel == Color.BLACK){
                                                //Log.i(TAG,"pixel["+linea+"]["+col+"]=> Negro");
                                                byt |= (1 << 7-bitCounter);
                                            }else{
                                                //Log.i(TAG,"pixel["+linea+"]["+col+"]=> Blanco");
                                                byt |= (0 << 7-bitCounter);
                                            }

                                            //Log.i(TAG,"pixel["+linea+"]["+col+"]=> "+bitCounter + " ~ "+String.format("%X",byt));
                                            bitCounter++;
                                            if(bitCounter==8){
                                                //Log.i(TAG,"Byte "+String.format("%x",byt));
                                                toPrintBytes.add(byt);
                                                byt = (byte)0x00;
                                                bitCounter = 0;
                                            }
                                        }

                                    }



                                    Command.ESC_Align[2] = 0x01;
                                    mService.write(Command.ESC_Align);

                                    byte arr[] = {(byte)0x1D,(byte)0x76,(byte)0x30,(byte)0x00,(byte)0x3e,(byte)0x00,(byte)0xf0,(byte)0x00};


                                    // byte arr[] = {(byte)0x1C,(byte)0x71,(byte)0x01,(byte)0x32,(byte)0x00,(byte)0xF0,(byte)0x00};

                                    mService.write(arr);
                                    byte[] bts = new byte[toPrintBytes.size()];
                                    int j = 0;
                                    for(byte bt : toPrintBytes){
                                        bts[j] = bt;
                                        j++;
                                    }
                                    mService.write(bts);
                                }else if(idPrinter.equalsIgnoreCase("BIXOLON")){
                                    BitmapFactory.Options o = new BitmapFactory.Options();
                                    o.inScaled = false;
                                    Bitmap bmp = BitmapFactory.decodeResource(getApplicationContext().getResources(),R.drawable.dtebig,o);
                                    if(!bmpUriStr.isEmpty()){

                                        Uri bmpUri = Uri.parse(bmpUriStr);
                                        Log.i(TAG,"URI "+bmpUriStr + " "+bmpUri);
                                        bmp = MediaStore.Images.Media.getBitmap(getApplication().getContentResolver(), bmpUri);
                                    }

                                    Log.i(TAG,"TENGO BMP");
                                    Log.i(TAG,"BMP "+bmp.getWidth()+ " x "+bmp.getHeight());
                                    Log.i(TAG,"BMP " + bmp.getDensity());
                                    Log.i(TAG,"BMP "+ bmp.toString());

                                    int ancho = bmp.getWidth();
                                    int alto = bmp.getHeight();

                                    List<Byte> toPrintBytes = new ArrayList<>();



                                    for(int linea=0;linea<alto;linea++){

                                        int bitCounter = 0;
                                        byte byt = (byte)0x00;

                                        for(int col=0;col<ancho;col++){


                                            int pixel = bmp.getPixel(col,linea);
                                            if(pixel == Color.BLACK){
                                                //Log.i(TAG,"pixel["+linea+"]["+col+"]=> Negro");
                                                byt |= (1 << 7-bitCounter);
                                            }else{
                                                //Log.i(TAG,"pixel["+linea+"]["+col+"]=> Blanco");
                                                byt |= (0 << 7-bitCounter);
                                            }

                                            //Log.i(TAG,"pixel["+linea+"]["+col+"]=> "+bitCounter + " ~ "+String.format("%X",byt));
                                            bitCounter++;
                                            if(bitCounter==8){
                                                //Log.i(TAG,"Byte "+String.format("%x",byt));
                                                toPrintBytes.add(byt);
                                                byt = (byte)0x00;
                                                bitCounter = 0;
                                            }
                                        }

                                    }



                                    Command.ESC_Align[2] = 0x01;
                                    mService.write(Command.ESC_Align);

                                    byte arr[] = {(byte)0x1D,(byte)0x76,(byte)0x30,(byte)0x00,(byte)0x3e,(byte)0x00,(byte)0xf0,(byte)0x00};


                                    // byte arr[] = {(byte)0x1C,(byte)0x71,(byte)0x01,(byte)0x32,(byte)0x00,(byte)0xF0,(byte)0x00};

                                    mService.write(arr);
                                    byte[] bts = new byte[toPrintBytes.size()];
                                    int j = 0;
                                    for(byte bt : toPrintBytes){
                                        bts[j] = bt;
                                        j++;
                                    }
                                    mService.write(bts);


                                }else{

                                    BitmapFactory.Options o = new BitmapFactory.Options();
                                    o.inScaled = false;
                                    Bitmap bmp = BitmapFactory.decodeResource(getApplicationContext().getResources(),R.drawable.dtesmall,o);

                                    if(!bmpUriStr.isEmpty()){

                                        Uri bmpUri = Uri.parse(bmpUriStr);
                                        Log.i(TAG,"URI "+bmpUriStr + " "+bmpUri);
                                        bmp = MediaStore.Images.Media.getBitmap(getApplication().getContentResolver(), bmpUri);
                                        bmp = Bitmap.createScaledBitmap(bmp,250,120,false);
                                    }

                                    Log.i(TAG,"TENGO BMP");
                                    Log.i(TAG,"BMP "+bmp.getWidth()+ " x "+bmp.getHeight());
                                    Log.i(TAG,"BMP " + bmp.getDensity());
                                    Log.i(TAG,"BMP "+ bmp.toString());

                                    int ancho = bmp.getWidth();
                                    int alto = bmp.getHeight();

                                    List<Byte> toPrintBytes = new ArrayList<>();



                                    for(int linea=0;linea<alto;linea++){

                                        int bitCounter = 0;
                                        byte byt = (byte)0x00;

                                        for(int col=0;col<ancho;col++){


                                            int pixel = bmp.getPixel(col,linea);
                                            if(pixel == Color.BLACK){
                                                //Log.i(TAG,"pixel["+linea+"]["+col+"]=> Negro");
                                                byt |= (1 << 7-bitCounter);
                                            }else{
                                                //Log.i(TAG,"pixel["+linea+"]["+col+"]=> Blanco");
                                                byt |= (0 << 7-bitCounter);
                                            }

                                            //Log.i(TAG,"pixel["+linea+"]["+col+"]=> "+bitCounter + " ~ "+String.format("%X",byt));
                                            bitCounter++;
                                            if(bitCounter==8){
                                                Log.i(TAG,"Byte "+String.format("%x",byt));
                                                toPrintBytes.add(byt);
                                                byt = (byte)0x00;
                                                bitCounter = 0;
                                            }
                                        }

                                    }



                                    Command.ESC_Align[2] = 0x01;
                                    mService.write(Command.ESC_Align);

                                    byte arr[] = {(byte)0x1D,(byte)0x76,(byte)0x30,(byte)0x00,(byte)0x1F,(byte)0x00,(byte)0x78,(byte)0x00};


                                    // byte arr[] = {(byte)0x1C,(byte)0x71,(byte)0x01,(byte)0x32,(byte)0x00,(byte)0xF0,(byte)0x00};

                                    mService.write(arr);
                                    byte[] bts = new byte[toPrintBytes.size()];
                                    int j = 0;
                                    for(byte bt : toPrintBytes){
                                        bts[j] = bt;
                                        j++;
                                    }
                                    mService.write(bts);
                                }




                                String symMoneda = "S/.";
                                if(infoDocumentoFactura.tipoMoneda.equalsIgnoreCase("PEN")){
                                    symMoneda = "S/.";
                                }else if(infoDocumentoFactura.tipoMoneda.equalsIgnoreCase("USD")){
                                    symMoneda = "$";
                                }else{
                                    symMoneda = "€";
                                }

                                //se solicito sacar el simbolo de moneda
                                // 25 Jun.
                                symMoneda = "";




                                if(1==2){
                                    return;//para debug
                                }

                                byte comm[] = Command.ESC_t;
                                comm[2] = (byte)16;
                                mService.write(comm);

                                Log.i(TAG,"btnPrintFindedDodcument");
                                Command.ESC_Align[2] = 0x01;
                                mService.write(Command.ESC_Align);
                                Command.GS_ExclamationMark[2] = 0x10;
                                mService.write(Command.GS_ExclamationMark);

                                StringBuilder sep = new StringBuilder();
                                for(int i=0;i<10;i++){
                                    sep.append("*");
                                }
                                sep.append("\n");

                                StringBuilder sep2 = new StringBuilder();
                                for(int i=0;i<40;i++){
                                    sep2.append("-");
                                }
                                sep2.append("\n");

                                byte[] sepBuf  = sep.toString().getBytes();
                                //mService.write(sepBuf);




                                String toPrintName = infoDocumentoFactura.nombreEmisor+"\n";
                                byte[] buf = toPrintName.getBytes("ISO-8859-1");
                                mService.write(buf);

                                Command.GS_ExclamationMark[2] = 0x00;
                                mService.write(Command.GS_ExclamationMark);
                                // imprimir direccion aca

                                String toPrintDireccionEmisor = infoDocumentoFactura.direccionEmisor+"\n";
                                byte[] direccionbuf = toPrintDireccionEmisor.getBytes("ISO-8859-1");
                                mService.write(direccionbuf);

                                //fin imprimir direccion


                                Command.GS_ExclamationMark[2] = 0x00;
                                mService.write(Command.GS_ExclamationMark);
                                Command.ESC_Align[2] = 0x01;
                                mService.write(Command.ESC_Align);

                                String toPrintEmisorRuc = "RUC: "+infoDocumentoFactura.rucEmisor+"\n";
                                mService.write(toPrintEmisorRuc.getBytes("ISO-8859-1"));

                                if(tipo !=null){

                                    //String toPrintTipoDocumento = tipo+"\n";
                                    //mService.write(toPrintTipoDocumento.getBytes());

                                }

                                Command.GS_ExclamationMark[2] = 0x00;
                                mService.write(Command.GS_ExclamationMark);

                                String tipoDocumentoEmit = "";
                                Log.i(TAG,"SERIE ES "+infoDocumentoFactura.serie);
                                String pSerie[] = infoDocumentoFactura.serie.split("-");



                                Log.i(TAG,"SERIE LNGT == 1 "+pSerie[0]);
                                if(pSerie[0].startsWith("B0")){
                                    Command.ESC_Align[2] = 0x01;
                                    mService.write(Command.ESC_Align);
                                    String toPrintTipo = "Boleta de Venta Electronica\n";
                                    tipoDocumentoEmit = "BOLETA";

                                    mService.write(toPrintTipo.getBytes());
                                }
                                if(pSerie[0].startsWith("F0")){
                                    Command.ESC_Align[2] = 0x01;
                                    mService.write(Command.ESC_Align);
                                    String toPrintTipo = "Factura Electronica\n";
                                    mService.write(toPrintTipo.getBytes("ISO-8859-1"));
                                    tipoDocumentoEmit = "FACTURA";
                                }


                                Log.i(TAG,"SERIE LNGT > 1 "+pSerie[0]);
                                if (pSerie[0].startsWith("BC") || pSerie[0].startsWith("FC")){
                                    Log.i(TAG,"Seria Nota Credito");
                                    Command.ESC_Align[2] = 0x01;
                                    mService.write(Command.ESC_Align);
                                    String toPrintTipo = "- NOTA CREDITO -\n";
                                    mService.write(toPrintTipo.getBytes("ISO-8859-1"));
                                }

                                if (pSerie[0].startsWith("BD") || pSerie[0].startsWith("FD")){
                                    Log.i(TAG,"Seria Nota DEbito");
                                    Command.ESC_Align[2] = 0x01;
                                    mService.write(Command.ESC_Align);
                                    String toPrintTipo = "- NOTA DEBITO -\n";
                                    mService.write(toPrintTipo.getBytes("ISO-8859-1"));
                                }

                                String toPrintFolioSerie = codigo +"\n" ;
                                mService.write(toPrintFolioSerie.getBytes("ISO-8859-1"));
                                String separacion = "\n";
                                mService.write(separacion.getBytes("ISO-8859-1"));












                                Command.GS_ExclamationMark[2] = 0x00;
                                mService.write(Command.GS_ExclamationMark);



                                mService.write(separacion.getBytes("ISO-8859-1"));

                                //imprmir direccion local

                                String localAddress     = sharedPreferences.getString("LOCAL_ADDRESS","")+"\n";
                                mService.write(localAddress.getBytes("ISO-8859-1"));


                                //fin impresion local


                                Command.GS_ExclamationMark[2] = 0x00;
                                mService.write(Command.GS_ExclamationMark);
                                Command.ESC_Align[2] = 0x00;
                                mService.write(Command.ESC_Align);



                                mService.write(separacion.getBytes("ISO-8859-1"));


                                if (infoDocumentoFactura.serie.startsWith("BND") || infoDocumentoFactura.serie.startsWith("BNC") ||infoDocumentoFactura.serie.startsWith("FNC") ||infoDocumentoFactura.serie.startsWith("FND")){
                                    String toPrintDocMod = fixedSizeString("Documento",15,0)+":"+infoDocumentoFactura.ordenCompra+"\n\n";
                                    mService.write(toPrintDocMod.getBytes("ISO-8859-1"));
                                }
                                String toPrintFechaEmision = fixedSizeString("Fecha Emisión",15,0)+":"+infoDocumentoFactura.fechaEmision+"\n\n";
                                mService.write(toPrintFechaEmision.getBytes("ISO-8859-1"));

                                String toPrintRazonSocial = fixedSizeString("Razón Social",15,0)+":"+infoDocumentoFactura.nombreReceptor+"\n";
                                mService.write(toPrintRazonSocial.getBytes("ISO-8859-1"));

                                String toPrintRUC = fixedSizeString("RUC Cliente",15,0)+":"+infoDocumentoFactura.rucReceptor+"\n";
                                mService.write(toPrintRUC.getBytes("ISO-8859-1"));


                                String toPrintSep = "...............................................\n";
                                mService.write(toPrintSep.getBytes());


                                /**** impresion por item ******/

                                //imprimir cabecera

                                /*
                                ------SKU------- ------Q---- -----PU---- ------T----
                                * */

                                int skuSpaces   = 16;
                                int otherSpaces = 9;
                                String itemsHeaderP1 = doCenter("SKU",skuSpaces) + doCenter(" CANT",otherSpaces) + doCenter("PRECIO",otherSpaces) + doCenter(" TOTAL",otherSpaces)+"\n";
                                String itemsHeaderP2 = doCenter("DESCRIPCION",skuSpaces) + doCenter(" UNIT",otherSpaces) + doCenter(" UNIT",otherSpaces)+"\n\n";

                                mService.write(itemsHeaderP1.getBytes("ISO-8859-1"));
                                mService.write(itemsHeaderP2.getBytes("ISO-8859-1"));

                                for(InfoItemFactura iif : infoDocumentoFactura.listaItems){
                                    String itemDescP1 = doCenter(iif.codigoItem,skuSpaces) + doCenter(iif.cantidad,otherSpaces) + doCenter(iif.precio,otherSpaces)+ doCenter(iif.importe,otherSpaces)+"\n";
                                    String itemDescP2 = iif.descripcion+"\n";
                                    mService.write(itemDescP1.getBytes("ISO-8859-1"));
                                    mService.write(itemDescP2.getBytes("ISO-8859-1"));
                                }

                                /*
                                for(InfoItemFactura iif : infoDocumentoFactura.listaItems){
                                    String lineaUno = "Código: "+iif.codigoItem +"\n";
                                    mService.write(lineaUno.getBytes("ISO-8859-1"));

                                    String lineaDos = "Descripción:\n"+iif.descripcion+"\n";
                                    mService.write(lineaDos.getBytes("ISO-8859-1"));

                                    String lineaTres = "Cantidad: "+iif.cantidad + " Precio Unitario: "+symMoneda+iif.precio +"\n";
                                    mService.write(lineaTres.getBytes("ISO-8859-1"));
                                    Command.ESC_Align[2] = 0x02;
                                    mService.write(Command.ESC_Align);
                                    String lineaCuatro = "Importe: "+fixedSizeString(symMoneda+iif.importe,15,1)+"\n";
                                    mService.write(lineaCuatro.getBytes("ISO-8859-1"));
                                    Command.ESC_Align[2] = 0x01;
                                    mService.write(Command.ESC_Align);
                                    String lineaCinco= "____________________________\n";
                                    mService.write(lineaCinco.getBytes("ISO-8859-1"));
                                    Command.ESC_Align[2] = 0x00;
                                    mService.write(Command.ESC_Align);
                                }
                                */

                                mService.write(toPrintSep.getBytes());

                                Thread.sleep(500);
                                String toPrintNewLine = "\n";
                                mService.write(toPrintNewLine.getBytes());

                                String toPrintsTt = "SON: "+infoDocumentoFactura.strMonto+"\n";
                                mService.write(toPrintsTt.getBytes("ISO-8859-1"));

                                mService.write(toPrintNewLine.getBytes());

                                Command.ESC_Align[2] = 0x02;
                                mService.write(Command.ESC_Align);



                                String toPrintOpEx = fixedSizeString("Op. Exonerada",15,0)+":"+fixedSizeString("",15,1)+"\n";
                                mService.write(toPrintOpEx.getBytes("ISO-8859-1"));

                                String toPrintOpIna = fixedSizeString("Op. Inafecta",15,0)+":"+fixedSizeString("",15,1)+"\n";
                                mService.write(toPrintOpIna.getBytes("ISO-8859-1"));

                                String toPrintOpGrat = fixedSizeString("Op. Gratuita",15,0)+":"+fixedSizeString("",15,1)+"\n";
                                mService.write(toPrintOpGrat.getBytes("ISO-8859-1"));

                                String toPrintOpGrav = fixedSizeString("Op. Gravada",15,0)+":"+fixedSizeString("",15,1)+"\n";
                                mService.write(toPrintOpGrav.getBytes("ISO-8859-1"));



                                String toPrintISC = fixedSizeString("I.S.C.",15,0)+":"+fixedSizeString(symMoneda+"0.00",15,1)+"\n";
                                mService.write(toPrintISC.getBytes("ISO-8859-1"));

                                String toPrintIGV = fixedSizeString("I.G.V. 18%",15,0)+":"+fixedSizeString(symMoneda+infoDocumentoFactura.igv,15,1)+"\n";
                                mService.write(toPrintIGV.getBytes("ISO-8859-1"));



                                String toPrintDesc = fixedSizeString("Desc. Globales",15,0)+":"+fixedSizeString(symMoneda+infoDocumentoFactura.descuento,15,1)+"\n";
                                mService.write(toPrintDesc.getBytes("ISO-8859-1"));

                                String toPrintTt = fixedSizeString("Importe Total",15,0)+":"+fixedSizeString(symMoneda+infoDocumentoFactura.totalVenta,15,1)+"\n";
                                mService.write(toPrintTt.getBytes("ISO-8859-1"));



                                if(infoDocumentoFactura.tipoMoneda.equalsIgnoreCase("PEN")){
                                    Log.i(TAG,"Moneda Es Soles");
                                    String toPrintTipoMoneda = fixedSizeString("Tipo Moneda",15,0)+":"+fixedSizeString("Sol",15,1)+"\n";
                                    mService.write(toPrintTipoMoneda.getBytes("ISO-8859-1"));

                                }
                                if(infoDocumentoFactura.tipoMoneda.equalsIgnoreCase("USD")){
                                    Log.i(TAG,"Moneda Es USD");
                                    String toPrintTipoMoneda = fixedSizeString("Tipo Moneda",15,0)+":"+fixedSizeString("Dólar USA",15,1)+"\n";
                                    mService.write(toPrintTipoMoneda.getBytes("ISO-8859-1"));

                                }
                                if(infoDocumentoFactura.tipoMoneda.equalsIgnoreCase("EUR")){
                                    Log.i(TAG,"Moneda Es EURO");
                                    String toPrintTipoMoneda = fixedSizeString("Tipo Moneda",15,0)+":"+fixedSizeString("EURO",15,1)+"\n";
                                    mService.write(toPrintTipoMoneda.getBytes("ISO-8859-1"));

                                }

                                String toPrintMoneda = fixedSizeString("Codigo Moneda",15,0)+":"+fixedSizeString(infoDocumentoFactura.tipoMoneda,15,1)+"\n";
                                mService.write(toPrintMoneda.getBytes("ISO-8859-1"));

                                mService.write(toPrintNewLine.getBytes());
                                mService.write(toPrintSep.getBytes());
                                mService.write(toPrintNewLine.getBytes());


                                mService.write(Command.GS_V_m_n);

                                Command.ESC_Align[2] = 0x01;
                                mService.write(Command.ESC_Align);
                                mService.write(Command.ESC_Align);
                                //mService.write(sepBuf);




                                if (idPrinter.equalsIgnoreCase("LESHP")){

                                    Log.i(TAG,"Print in LESHP");
                                    byte[] qrcode3 = PrinterCommand.getBarCommand(QrCode, 0, 2, 7);//


                                    mService.write(qrcode3);


                                    mService.write(PrinterCommand.POS_Set_PrtAndFeedPaper(25));
                                    mService.write(Command.GS_V_m_n);


                                }else if(idPrinter.equalsIgnoreCase("BIXOLON")||idPrinter.equalsIgnoreCase("CBX 830B")){

                                    byte[] errorSelector = {0x1d,0x28,0x6b,0x03,0x00,0x31,0x45,0x31};
                                    mService.write(errorSelector);
                                    byte[] moduleSelector = {0x1d,0x28,0x6b,0x03,0x00,0x31,0x43,0x06};
                                    mService.write(moduleSelector);

                                    byte commandStr[]={0x1d,0x28,0x6b,0,0,0x31,0x50,0x30};
                                    commandStr[3] = (byte) ((QrCode.length()+3)%256);
                                    commandStr[4] = (byte) ((QrCode.length()+3)/256);

                                    mService.write(commandStr);
                                    mService.write(QrCode.getBytes());


                                    byte pcommand[]={0x1d,0x28,0x6b,0x03,0x00,0x31,0x51,48};
                                    mService.write(pcommand);


                                    mService.write(PrinterCommand.POS_Set_PrtAndFeedPaper(35));
                                    mService.write(Command.GS_V_m_n);

                                    String spc = "\n\n\n";
                                    mService.write(spc.getBytes());
                                    Command.ESC_Align[2] = 0x01;
                                    mService.write(Command.ESC_Align);


                                }else{

                                    Log.i(TAG,"Imprimir en GPrinter");


                                    byte[] errorSelector = {0x1d,0x28,0x6b,0x03,0x00,0x31,0x45,0x31};
                                    mService.write(errorSelector);
                                    byte[] moduleSelector = {0x1d,0x28,0x6b,0x03,0x00,0x31,0x43,0x06};
                                    mService.write(moduleSelector);

                                    byte commandStr[]={0x1d,0x28,0x6b,0,0,0x31,0x50,0x30};
                                    commandStr[3] = (byte) ((QrCode.length()+3)%256);
                                    commandStr[4] = (byte) ((QrCode.length()+3)/256);

                                    mService.write(commandStr);
                                    mService.write(QrCode.getBytes());


                                    byte pcommand[]={0x1d,0x28,0x6b,0x03,0x00,0x31,0x51,48};
                                    mService.write(pcommand);


                                    mService.write(PrinterCommand.POS_Set_PrtAndFeedPaper(35));
                                    mService.write(Command.GS_V_m_n);

                                    String spc = "\n\n\n";
                                    mService.write(spc.getBytes());

                                }

                                String finalStr ="Reperesentación impresa del Documeto\n Electrónico\nAutorizado mediante la resolución\n N. 203-2015/SUNAT\nConsulte su documento en:\n\n http://azuriandte.azurian.com.pe/comprobante\n\n¡MUCHAS GRACIAS POR SU PREFERENCIA!";
                                //finalStr= "Reperesentación impresa del Documeto de Venta\n Electrónico\nAutorizado mediante la resolución\n N. 018005000147\nConsulte su documento en:\n\n http://azuriandte.azurian.com.pe/comprobante\n\n¡MUCHAS GRACIAS POR SU PREFERENCIA!";

                                if(tipoDocumentoEmit.equalsIgnoreCase("boleta")){
                                    finalStr= "Reperesentación impresa de la Boleta de Venta \n Electrónica\nAutorizado mediante la resolución\n N. 203-2015/SUNAT\nConsulte su documento en:\n\n http://azuriandte.azurian.com.pe/comprobante\n\n¡MUCHAS GRACIAS POR SU PREFERENCIA!";
                                }

                                if(tipoDocumentoEmit.equalsIgnoreCase("factura")){
                                    finalStr= "Reperesentación impresa de la Factura \n Electrónica\nAutorizado mediante la resolución\n N. 203-2015/SUNAT\nConsulte su documento en:\n\n http://azuriandte.azurian.com.pe/comprobante\n\n¡MUCHAS GRACIAS POR SU PREFERENCIA!";
                                }

                                mService.write(finalStr.getBytes("ISO-8859-1"));
                                mService.write(toPrintNewLine.getBytes());
                                mService.write(toPrintSep.getBytes());
                                mService.write(toPrintNewLine.getBytes());

                                Command.ESC_Align[2] = 0x00;
                                mService.write(Command.ESC_Align);

                                Date date = new Date();
                                String endString = fixedSizeString("Usuario",8,0)+":"+ Dconfig.user+"\n";

                                mService.write(endString.getBytes("UTF-8"));

                                mService.write(toPrintNewLine.getBytes());
                                mService.write(toPrintSep.getBytes());
                                mService.write(toPrintNewLine.getBytes());

                            }catch (Exception e){
                                Log.e(TAG,"ERROR "+e.toString());
                            }

                        }
                    });

                    th.start();







/*
                    Log.i(TAG,infoDocumentoFactura.nombreEmisor);









                    Command.ESC_Align[2] = 0x00;
                    mService.write(Command.ESC_Align);

                    String toPrintRuc = infoDocumentoFactura.rucEmisor+"\n";
                    byte[] buf2h = toPrintRuc.getBytes();
                    mService.write(buf2h);


                    String address = infoDocumentoFactura.emisorStreetName+"\n"+infoDocumentoFactura.emisorCityName+"\n"+infoDocumentoFactura.emisorDistrictName+"\n\n";

                    byte[] buf3 = address.getBytes();
                    mService.write(buf3);

                    Command.ESC_Align[2] = 0x00;
                    mService.write(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x01;
                    mService.write(Command.GS_ExclamationMark);


                    Command.ESC_Align[2] = 0x01;
                    mService.write(Command.ESC_Align);

                    String facturaHeader = "FACTURA "+ codigo +"\n\n";
                    byte[] buf2 = facturaHeader.getBytes();

                    mService.write(buf2);

                    Command.ESC_Align[2] = 0x00;
                    mService.write(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;
                    mService.write(Command.GS_ExclamationMark);

                    mService.write(sepBuf2);

                    String strReceptor = "Senores:\n"+infoDocumentoFactura.nombreReceptor+"\nDireccion:"+infoDocumentoFactura.receptorStreetName+"\n";
                    byte[] buf4 = strReceptor.getBytes();
                    mService.write(buf4);

                    Command.ESC_Align[2] = 0x01;
                    mService.write(Command.ESC_Align);
                    byte[] buf5 = "ITEMS\n\n".getBytes();
                    mService.write(buf5);

                    Command.ESC_Align[2] = 0x00;
                    mService.write(Command.ESC_Align);

                    for(InfoItemFactura item : infoDocumentoFactura.listaItems){

                        StringBuilder sb = new StringBuilder();
                        sb.append("> "+item.descripcion+" \n\t\t");
                        Command.ESC_Align[2] = 0x02;
                        mService.write(Command.ESC_Align);
                        sb.append(item.precio);
                        Command.ESC_Align[2] = 0x00;
                        mService.write(Command.ESC_Align);
                        sb.append("\n_________________________\n");
                        byte[] itemToPrint = sb.toString().getBytes();
                        mService.write(itemToPrint);

                    }

                    mService.write(Command.GS_V_m_n);

                    StringBuilder sbs = new StringBuilder();
                    sbs.append("Total:"+infoDocumentoFactura.total+"/S\n("+infoDocumentoFactura.totalEscrito+")\n");
                    sbs.append("IGV: "+infoDocumentoFactura.taxAmount+"/S\n\n");


                    mService.write(sbs.toString().getBytes());


                    mService.write(Command.GS_V_m_n);

                    mService.write("\n".getBytes());


                    SharedPreferences sharedPreferences = getSharedPreferences("PRINTER_PREFS",MODE_PRIVATE);
                    String idPrinter = "";
                    idPrinter = sharedPreferences.getString("PRINTER_ID","");
                    Log.i(TAG,"SP Printer "+idPrinter);
                    if (idPrinter.equalsIgnoreCase("LESHP")){

                        Log.i(TAG,"Print in LESHP");
                        byte[] qrcode3 = PrinterCommand.getBarCommand(infoDocumentoFactura.qrString, 0, 2, 7);//

                        Command.ESC_Align[2] = 0x01;
                        mService.write(Command.ESC_Align);
                        mService.write(qrcode3);
                        mService.write(Command.ESC_Align);


                        mService.write(PrinterCommand.POS_Set_PrtAndFeedPaper(25));
                        mService.write(Command.GS_V_m_n);


                    }else{

                        Log.i(TAG,"Imprimir en GPrinter");
                        String QrPrueba = infoDocumentoFactura.qrString; //"Eep!|Eeeep!|Eeeeeep!|EeEep";
                        byte[] errorSelector = {0x1d,0x28,0x6b,0x03,0x00,0x31,0x45,0x31};
                        mService.write(errorSelector);
                        byte[] moduleSelector = {0x1d,0x28,0x6b,0x03,0x00,0x31,0x43,0x06};
                        mService.write(moduleSelector);

                        byte commandStr[]={0x1d,0x28,0x6b,0,0,0x31,0x50,0x30};
                        commandStr[3] = (byte) ((QrPrueba.length()+3)%256);
                        commandStr[4] = (byte) ((QrPrueba.length()+3)/256);

                        mService.write(commandStr);
                        mService.write(QrPrueba.getBytes());


                        byte pcommand[]={0x1d,0x28,0x6b,0x03,0x00,0x31,0x51,48};
                        mService.write(pcommand);


                        mService.write(PrinterCommand.POS_Set_PrtAndFeedPaper(35));
                        mService.write(Command.GS_V_m_n);

                        String spc = "\n\n\n\n\n\n\n";
                        mService.write(spc.getBytes());

                    }
                    */


                }catch (Exception e){
                    Log.e(TAG,"ERROR AL imprimir");
                }

            }
        });



    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        Log.i(TAG,"onRequestPermissionsResult");
        switch (requestCode) {
            case 1: {

                /*
                if (!(grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                    Toast.makeText(getApplicationContext(), "Permission denied to access your location.", Toast.LENGTH_SHORT).show();
                }
                */
            }
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        //delayedHide(100);
    }


    public String doCenter(String text, int size) {

        StringBuilder stringBuilder = new StringBuilder();
        int spaces = size - text.length();
        int split = spaces / 2;

        System.out.println("TL: " + text.length() + "  SP: " + spaces + "  SPLT: " + split);
        for (int i = 0; i < split; i++) {
            stringBuilder.append(" ");
        }


        if((spaces/2)%2 != 0){
            stringBuilder.append(" ");
        }


        stringBuilder.append(text);



        for (int i = 0; i < split; i++) {
            stringBuilder.append(" ");
        }
        return stringBuilder.toString();
    }
    private String fixedSizeString(String name,int size, int pos){

        StringBuilder builder = new StringBuilder();

        if(pos == 0){

            if(name.isEmpty()){

                for(int i=0;i<size;i++){
                    builder.append(" ");
                }

                return builder.toString();
            }

            builder.append(name);
            int spacios = size - name.length();
            for(int i = 0; i<spacios;i++){
                builder.append(" ");
            }
            return builder.toString();

        }else{

            if(name.isEmpty()){

                for(int i=0;i<size;i++){
                    builder.append(" ");
                }

                return builder.toString();
            }

            int spacios = size - name.length();
            for(int i = 0; i<spacios;i++){
                builder.append(" ");
            }
            builder.append(name);
            return builder.toString();
        }




    }
    public class InfoItemFactura{
        String cantidad;
        String precio;
        String descripcion;
        String importe;
        String codigoItem;
    }

    public class InfoDocumentoFactura{


        String nombreEmisor;
        String nombreReceptor;
        String rucEmisor;
        String rucReceptor;
        String documentoEmisor;

        String direccionEmisor;
        String direccionReceptor;

        String tipoDocumentoReceptor;
        String documentoReceptor;

        String tipoComprobante;

        String serie;

        String issueDate;
        String currencyCode;

        String strMonto;
        List<InfoItemFactura> listaItems = new ArrayList<>();

        String total;
        String totalEscrito;

        String digest;
        String taxAmount;

        String qrString;

        String igv;
        String totalVenta;
        String fechaEmision;
        String resume;
        String descuento;

        String tipoMoneda;

        String ordenCompra;

    }


    /*
    @Override
    public void onStart() {
        super.onStart();

        Log.i(TAG,"onStart");
        // If Bluetooth is not on, request that it be enabled.
        // setupChat() will then be called during onActivityResult
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(
                    BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            // Otherwise, setup the session
            Log.i(TAG,"didStartActivityForResult");
        } else {
            if (mService == null) {
                //KeyListenerInit();//监听
                Log.i(TAG, "mService is nULL, new BluetoothService");

                mService = new BluetoothService(this, mHandler);

            }
        }

        if(ImpConfig.printAddress.equalsIgnoreCase("")){
            Log.i(TAG,"ImpConfig not setted");
        }else{
            BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(ImpConfig.printAddress);
            mService.connect(device);
            Log.i(TAG,"connect to peripheral");
        }
    }
    */



}
