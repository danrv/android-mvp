package android.azurian.dte.Consulta;

import android.app.DatePickerDialog;
import android.azurian.dte.BlePrinter.BluetoothService;
import android.azurian.dte.BlePrinter.PrinterService;
import android.azurian.dte.R;
import android.azurian.dte.sdk.Command;
import android.azurian.dte.services.ConsultarLoteDoc;
import android.azurian.dte.services.DArrayOfLoteDocumentos;
import android.azurian.dte.services.DConsultarLoteDocRequest;
import android.azurian.dte.services.DConsultarLoteDocResponse;
import android.azurian.dte.services.DDTEServiceSoapBinding;
import android.azurian.dte.services.DIServiceEvents;
import android.azurian.dte.services.DLoteDocumentos;
import android.azurian.dte.services.DOperationResult;
import android.azurian.dte.services.Dconfig;
import android.azurian.dte.services.DocumentHelper;
import android.azurian.dte.services.RestConsltarLoteDoc;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static android.app.AlertDialog.THEME_HOLO_DARK;
import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FormularioFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FormularioFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FormularioFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FormularioFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FormularioFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FormularioFragment newInstance(String param1, String param2) {
        FormularioFragment fragment = new FormularioFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    /*
    * Variables UI
    * */


    Button btnPT;

    Spinner selectTipoDocumento;
    Spinner selectEstadoSunat;
    Button btnFechaInicio;
    Button btnFechaFin;
    Button btnBuscar;
    EditText inTxtSeries;
    EditText inTxtCoInicial;
    EditText inTxtCoFinal;

    String fechaInicio="";
    String fechaTermino="";

    String idDocumento;
    String tipoDocumentoSelected;
    int selectedSunatState = 0;

    ProgressBar progressBar;

    boolean cargo = false;

    final String TAG = getClass().getSimpleName();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_formulario, container, false);



        final PrinterService ps = PrinterService.getInstance(getActivity().getApplicationContext());

        if (ps.isADeviceConnected == false){
            ps.initService();
        }

        final BluetoothService mService = ps.mService;


        //borrar prueba BLE
        byte comm[] = Command.ESC_t;
        /*
        btnPT = v.findViewById(R.id.btnPT);
        btnPT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for(int i =0;i<50;i++){

                    try{

                        byte comm[] = Command.ESC_t;
                        comm[2] = (byte)16;
                        mService.write(comm);

                        String testString = i+ " = Aá Bb EÉeéë \n";
                        byte [] testByteToPrint = testString.getBytes("ISO-8859-1");
                        mService.write(testByteToPrint);

                    }catch (Exception e){

                    }

                }



            }
        });
        */
        progressBar         = v.findViewById(R.id.progesoBusquedaDocumento);
        inTxtSeries         = v.findViewById(R.id.inTxtSeries);
        inTxtCoInicial      = v.findViewById(R.id.inTxtCoInicial);
        inTxtCoFinal        = v.findViewById(R.id.inTxtCoFinal);
        selectEstadoSunat   = v.findViewById(R.id.selectEstadoSunat2);
        selectTipoDocumento = v.findViewById(R.id.selectTipoDocumento);
        btnFechaInicio      = v.findViewById(R.id.btnFechaInicio);
        btnFechaFin         = v.findViewById(R.id.btnFechaFin);
        btnBuscar           = v.findViewById(R.id.btnBuscar);

        idDocumento         = "01";

        final String[] docsSunatStr     = {"NUEVO","APROBADO","APROBADO REPARO","RECHAZADO","DADO BAJA"};
        final int[] docsSunatIds        = {0,1,2,3,4};


        final String[] tipos    = {"FACTURA ELECTRONICA","BOLETA ELECTRONICA","NOTA DE CREDITO","NOTA DE DEBITO"};
        final String[] idDoc    = {"01","03","07","08"};


        selectEstadoSunat.setAdapter(new ArrayAdapter<String>(getContext(),android.R.layout.simple_spinner_item,docsSunatStr));
        selectEstadoSunat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                selectedSunatState = docsSunatIds[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        selectTipoDocumento.setAdapter(new ArrayAdapter<String>(getContext(),android.R.layout.simple_spinner_item,tipos));
        selectTipoDocumento.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id)
            {
                //Toast.makeText(adapterView.getContext(),
                //        (String) adapterView.getItemAtPosition(pos), Toast.LENGTH_SHORT).show();
                idDocumento = idDoc[pos];
                Log.i("ID_DOC",idDocumento);

                tipoDocumentoSelected = tipos[pos];
                if(idDoc[pos].equalsIgnoreCase("01")){
                    inTxtSeries.setText("F001");
                }
                if(idDoc[pos].equalsIgnoreCase("03")){
                    inTxtSeries.setText("B001");
                }
                if(idDoc[pos].equalsIgnoreCase("07")){
                    inTxtSeries.setText("C01");
                }
                if(idDoc[pos].equalsIgnoreCase("08")){
                    inTxtSeries.setText("D01");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {    }
        });



        Date today = new Date();
        Date daysAgo = new DateTime(today).minusDays(10).toDate();

        final Calendar todayCal = Calendar.getInstance();
        todayCal.setTime(today);

        final Calendar mesCal = Calendar.getInstance();
        mesCal.setTime(daysAgo);

/*        Date today = new Date();

        today.setTime(today.getTime());
        todayCal.add(Calendar.DAY_OF_MONTH,-5);
        final Calendar mesCal = Calendar.getInstance();
        mesCal.add(Calendar.MONTH,2);
*/

        //cal.add(Calendar.DATE, -1);
        final int lday = mesCal.get(Calendar.DAY_OF_MONTH);
        final int lmonth = mesCal.get(Calendar.MONTH);
        final int lyear = mesCal.get(Calendar.YEAR);


        btnFechaInicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DatePickerDialog datePickerDialogInicio = new DatePickerDialog(getContext(),THEME_HOLO_DARK, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                        Toast.makeText(getContext(),
                                ""+dayOfMonth+"/"+(++month)+"/"+year, Toast.LENGTH_SHORT).show();
                        btnFechaInicio.setText(""+dayOfMonth+"/"+(month)+"/"+year);
                        fechaInicio = year+"-"+month+"-"+dayOfMonth;
                    }
                }, lyear, lmonth-1, lday);

                datePickerDialogInicio.getDatePicker().setCalendarViewShown(false);
                datePickerDialogInicio.getDatePicker().setSpinnersShown(true);
                datePickerDialogInicio.show();
                try{
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                }catch(Exception e){


                }
            }
        });


        btnFechaFin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                DatePickerDialog datePickerDialogFin = new DatePickerDialog(getContext(),THEME_HOLO_DARK, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                        Toast.makeText(getContext(),
                                ""+dayOfMonth+"/"+(++month)+"/"+year, Toast.LENGTH_SHORT).show();
                        btnFechaFin.setText(""+dayOfMonth+"/"+(month)+"/"+year);
                        fechaTermino = year+"-"+month+"-"+dayOfMonth;
                        //fechaTermino
                    }
                }, todayCal.get(Calendar.YEAR), todayCal.get(Calendar.MONTH), todayCal.get(Calendar.DAY_OF_MONTH));

                datePickerDialogFin.show();
                try{
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                }catch(Exception e){

                    Log.e(TAG,"ERROR! "+e.toString());

                }

            }
        });



        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //DTEDTEServiceSoapBinding service = new DTEDTEServiceSoapBinding();


                //******
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        if(cargo == false) {
                            // intentar otra vez
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    btnBuscar.setVisibility(View.VISIBLE);
                                }
                            });

                        }else{

                            Thread.currentThread().interrupt();
                            Log.i(TAG,"Cargo, detener tarea");
                        }
                    }
                }).start();

                //******
                progressBar.setVisibility(View.VISIBLE);
                btnBuscar.setVisibility(View.INVISIBLE);

                try{
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                }catch(Exception e){

                    Log.e(TAG,"ERROR!!" +e.toString());
                }

                final String serie      = inTxtSeries.getText().toString();
                final String coInicial  = inTxtCoInicial.getText().toString();
                final String coFinal    = inTxtCoFinal.getText().toString();

                if(fechaInicio.isEmpty()){
                    fechaInicio = ""+lyear+"-"+lmonth+"-"+lday;
                }
                if(fechaTermino.isEmpty()){
                    fechaTermino = ""+todayCal.get(Calendar.YEAR)+"-"+(todayCal.get(Calendar.MONTH)+1)+"-"+todayCal.get(Calendar.DAY_OF_MONTH);
                }


                final Thread th = new Thread(new Runnable() {
                    @Override
                    public void run() {


                        try{

                            Long coF = coFinal.isEmpty()? 100000000L : Long.parseLong(coFinal);
                            Long coI = coInicial.isEmpty()? 1L : Long.parseLong(coInicial);

                            JSONObject params = new JSONObject();
                            params.put("apiKey",Dconfig.hash);
                            params.put("correlativoFinal",coF);
                            params.put("correlativoInicial",coI);
                            params.put("estadoSunat",selectedSunatState);
                            params.put("fechaEmisionFinal",fechaTermino);
                            params.put("fechaEmisionInicial",fechaInicio);
                            params.put("rucEmisor",Dconfig.ruc);
                            params.put("serie",serie.toUpperCase());
                            params.put("tipoDocumento",idDocumento);

                            ConsultarLoteDoc cld = new ConsultarLoteDoc();
                            JSONObject resultObj = cld.consultar(params);
                            JSONArray arregloDocumentos = resultObj.getJSONArray("item");

                            final ArrayList<DocumentHelper> listaDocumentos = new ArrayList<>();

                            for(int i=0;i<arregloDocumentos.length();i++){
                                JSONObject jitem = (JSONObject)arregloDocumentos.get(i);
                                DocumentHelper dh = new DocumentHelper();

                                dh.nombreReceptor = jitem.getString("razonSocialReceptor");
                                dh.serie = jitem.getString("serie");
                                dh.correlativo = jitem.getLong("correlativo");
                                dh.linkXML = jitem.getString("linkXML");
                                dh.linkPDF = jitem.getString("linkPDF");
                                dh.valor = jitem.getDouble("totalVenta");
                                dh.rucReceptor = jitem.getString("rucReceptor");
                                dh.correlativoStr = ""+jitem.getLong("correlativo");
                                dh.rucEmisor = jitem.getString("rucEmisor");
                                dh.tipoDocumento = jitem.getString("tipoDocumento");

                                listaDocumentos.add(dh);

                            }
                            if(listaDocumentos.size() == 0){
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        btnBuscar.setVisibility(View.VISIBLE);
                                        progressBar.setVisibility(View.INVISIBLE);
                                        Toast.makeText(getContext(),R.string.doc_no_encontrados,Toast.LENGTH_SHORT).show();
                                    }
                                });

                                return;
                            }
                            cargo = true;
                            Log.i(TAG,"Cargado");

                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {


                                    FragmentManager manager = getFragmentManager();
                                    FragmentTransaction fragmentTransaction = manager.beginTransaction();
                                    Resultados resultados = Resultados.newInstance("",listaDocumentos);
                                    fragmentTransaction.addToBackStack("Formulario");


                                    fragmentTransaction.replace(R.id.fragmentHolder,resultados);
                                    fragmentTransaction.commit();


                                }
                            });


                            /*

                            RestConsltarLoteDoc rcld = new RestConsltarLoteDoc();
                            RestConsltarLoteDoc.ReqParams params = rcld.new ReqParams();


                            Long coF = coFinal.isEmpty()? 100000000L : Long.parseLong(coFinal);
                            Long coI = coInicial.isEmpty()? 1L : Long.parseLong(coInicial);

                            params.setApiKey(Dconfig.hash);
                            params.setCorrelativoInicial(coI);
                            params.setCorrelativoFinal(coF);
                            params.setEstadoSunat(selectedSunatState);
                            params.setFechaEmisionFinal(fechaTermino);
                            params.setFechaEmisionInicial(fechaInicio);
                            params.setRucEmisor(Dconfig.ruc);
                            params.setSerie(serie.toUpperCase());
                            params.setTipoDocumento(idDocumento);

                            final ArrayList<DocumentHelper>listaDocumentos = rcld.consultar(params);

                            if(listaDocumentos == null){

                                Log.w(TAG,"LISTA LLEGO V_ACI");

                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        btnBuscar.setVisibility(View.VISIBLE);
                                        progressBar.setVisibility(View.INVISIBLE);
                                        Toast.makeText(getContext(),"Error",Toast.LENGTH_SHORT).show();
                                    }
                                });

                                return;
                            }

                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {


                                    FragmentManager manager = getFragmentManager();
                                    FragmentTransaction fragmentTransaction = manager.beginTransaction();
                                    Resultados resultados = Resultados.newInstance("",listaDocumentos);
                                    fragmentTransaction.addToBackStack("Formulario");


                                    fragmentTransaction.replace(R.id.fragmentHolder,resultados);
                                    fragmentTransaction.commit();


                                }
                            });

                            */
                        }catch (Exception e){

                            Log.e(TAG,"Error "+e.toString());
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    btnBuscar.setVisibility(View.VISIBLE);
                                    progressBar.setVisibility(View.INVISIBLE);
                                    Toast.makeText(getContext(),R.string.doc_error,Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                        /*
                        try{
                            DDTEServiceSoapBinding service = new DDTEServiceSoapBinding(new DIServiceEvents() {

                                @Override
                                public void Starting(){

                                }
                                @Override
                                public void Completed(DOperationResult result){

                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            progressBar.setVisibility(View.INVISIBLE);
                                            btnBuscar.setVisibility(View.VISIBLE);
                                        }
                                    });
                                    //DArrayOfLoteDocumentos lote = (DArrayOfLoteDocumentos) result.Result;

                                    try{


                                        Log.i(TAG,"result: "+result.Result);
                                        DConsultarLoteDocResponse resp = (DConsultarLoteDocResponse) result.Result;
                                        Log.i(TAG,"mensaje:"+resp.mensaje);

                                        if(resp.mensaje.equalsIgnoreCase("No existen documentos para la consulta")){
                                            Toast.makeText(getContext(),resp.mensaje,Toast.LENGTH_SHORT).show();

                                        }
                                        if(resp.mensaje.equalsIgnoreCase("Documentos obtenidos OK")){
                                            DArrayOfLoteDocumentos lote = resp.item;
                                            Log.i(TAG,"Hay "+lote.size()+ " Documentos");
                                            final ArrayList<DocumentHelper>listaDocumentos = new ArrayList<>();

                                            for (DLoteDocumentos doc : lote){

                                                DocumentHelper dh = new DocumentHelper();
                                                dh.correlativo = doc.correlativo;
                                                dh.correlativoStr =""+doc.correlativo;
                                                dh.estadoDocAzurian = doc.estadoDocAzurian;
                                                dh.estadoDocSunat = doc.estadoDocSunat;
                                                dh.descripcionSunat = doc.descripcionSunat;
                                                dh.linkPDF = doc.linkPDF;
                                                dh.linkXML = doc.linkXML;
                                                dh.rucEmisor = doc.rucEmisor;
                                                dh.serie = doc.serie;
                                                dh.tipoDocumento = doc.tipoDocumento;
                                                dh.tipoDocumentoStr = tipoDocumentoSelected;

                                                listaDocumentos.add(dh);
                                            }

                                            getActivity().runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {


                                                    FragmentManager manager = getFragmentManager();
                                                    FragmentTransaction fragmentTransaction = manager.beginTransaction();
                                                    Resultados resultados = Resultados.newInstance("",listaDocumentos);
                                                    fragmentTransaction.addToBackStack("Formulario");


                                                    fragmentTransaction.replace(R.id.fragmentHolder,resultados);
                                                    fragmentTransaction.commit();


                                                }
                                            });
                                        }

                                    }catch (Exception e){

                                        Log.e(TAG,""+e.toString());
                                        progressBar.setVisibility(View.INVISIBLE);
                                        btnBuscar.setVisibility(View.VISIBLE);
                                        Toast.makeText(getContext(),"Error obtener resultados",Toast.LENGTH_SHORT).show();
                                    }




                                }


                            });
                            DConsultarLoteDocRequest request = new DConsultarLoteDocRequest();
                            request.apiKey = Dconfig.hash; //"2f6a787df6efbb4608817a4079b36bbb";

                            Long coF = coFinal.isEmpty()? 100000000L : Long.parseLong(coFinal);


                            request.correlativoFinal= coF;

                            Long coI = coInicial.isEmpty()? 1L : Long.parseLong(coInicial);
                            request.correlativoInicial=coI;

                            request.estadoSunat=selectedSunatState;
                            request.fechaEmisionFinal=fechaTermino;
                            request.fechaEmisionInicial=fechaInicio;
                            request.rucEmisor= Dconfig.ruc;//"20218339167";
                            request.serie=serie.toUpperCase();
                            request.tipoDocumento=idDocumento;
                            Log.i(TAG,request.toString());
                            service.consultarLoteDocumentosAsync(request);

                        }catch (Exception e){

                            Log.e(TAG,"ERROR: "+e.toString());
                        }
                        */
                    }
                });

                th.start();

            }
        });

        return v;
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
