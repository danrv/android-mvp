package android.azurian.dte.Consulta;

import android.azurian.dte.R;
import android.azurian.dte.services.DocumentHelper;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Resultados.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Resultados#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Resultados extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    static private ArrayList<DocumentHelper>listaDocumentos;
    private OnFragmentInteractionListener mListener;

    public Resultados() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Resultados.
     */
    // TODO: Rename and change types and number of parameters
    public static Resultados newInstance(String param1, ArrayList<DocumentHelper> param2) {
        Resultados fragment = new Resultados();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        listaDocumentos = param2;
        //args.putString(ARG_PARAM2, param2);
        //args.putSerializable();
        //args.put
        //.listaDocumentos = param2;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    ListView listViewDocuments;

    final String TAG = getClass().getSimpleName();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_resultados, container, false);
        listViewDocuments = v.findViewById(R.id.listaDocumentos);


        if(listaDocumentos != null){
            listViewDocuments.setAdapter(new DocumentResultAdapter());
        }

        listViewDocuments.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DocumentHelper dh = listaDocumentos.get(position);
                Log.i(TAG,"idEmpresa: "+dh.rucEmisor);
                Log.i(TAG,"numero: "+dh.correlativo );
                Log.i(TAG,"serie: "+dh.serie);
                Log.i(TAG,"tipoDocumento: "+dh.tipoDocumento);

                try{


                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("idEmpresa",dh.rucEmisor);
                    jsonObject.put("numero",dh.correlativo);
                    jsonObject.put("serie",dh.serie);
                    jsonObject.put("tipoDocumento",dh.tipoDocumento);

                    Log.i(TAG,"JSON-> "+jsonObject.toString());
                    Intent intent = new Intent(getActivity(), WebViewActivity.class);
                    intent.putExtra("url",dh.linkPDF);
                    intent.putExtra("json",jsonObject.toString());
                    intent.putExtra("codigo",dh.serie+"-"+dh.correlativoStr);
                    startActivity(intent);

/*
                    Intent intent = new Intent(getActivity(),PrintTestActivity.class);
                    startActivity(intent);
*/


                }catch (Exception e){
                    Toast.makeText(getContext(),"Error en información",Toast.LENGTH_SHORT).show();
                }


                /*
                Intent intent = new Intent(getActivity(), WebViewActivity.class);
                intent.putExtra("url",dh.linkPDF);
                intent.putExtra("urlXml",dh.linkXML);
                intent.putExtra("codigo",dh.serie+"-"+dh.correlativoStr);
                startActivity(intent);*/
            }
        });



        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    static class DocumentResultViewHolder{
        TextView correlativoTV;
        TextView serieTV;
        TextView rucTV;
    }
    class DocumentResultAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return listaDocumentos.size();
        }

        @Override
        public Object getItem(int position) {
            return listaDocumentos.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        DocumentResultViewHolder viewHolder;
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            DocumentHelper dh = listaDocumentos.get(position);
            if(convertView == null){
                LayoutInflater inflater = getActivity().getLayoutInflater();
                convertView = inflater.inflate(R.layout.row_document,null,false);
                viewHolder = new DocumentResultViewHolder();
                viewHolder.correlativoTV = convertView.findViewById(R.id.correlativo);
                viewHolder.serieTV = convertView.findViewById(R.id.serie);
                viewHolder.rucTV = convertView.findViewById(R.id.ruc);
                convertView.setTag(viewHolder);
            }else{
                viewHolder = (DocumentResultViewHolder) convertView.getTag();
            }

            try{
                DecimalFormat df = new DecimalFormat("#,##0.00");
                viewHolder.rucTV.setText("Valor: S/"+df.format(dh.valor));
            }catch (Exception e){
                Log.e(TAG,"error df "+e.toString());
            }

            viewHolder.correlativoTV.setText("Serie:"+dh.serie+" Correlativo: "+ dh.correlativo);
            viewHolder.serieTV.setText("Receptor: "+dh.nombreReceptor + "\n<"+dh.rucReceptor+">");


            return convertView;
        }
    }
}
